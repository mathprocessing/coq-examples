From Coq Require Import ssreflect ssrfun ssrbool.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Definition f u :=
let: (m, n) := u in m + n.

Check f.

(** Pattern conditional *)

Variable d : Set.

Definition null (s : list d) :=
if s is nil then true else false.


Variable a : d -> bool.

Fixpoint all(s : list d) : bool :=
  if s is cons x s' 
    then a x && all s'
    else true.



(** Parametric polymorphism *)

(* 
Prenex Implicits null.
Definition all_null (s : list T) :=
  all null s. *)


(** Anonymous arguments *)

Inductive list (A : Type) : Type :=
  nil | cons of A & list A.
  
(** Wildcards, Definitions, no parentheses *)

Lemma test: True.
Proof.
pose f x y := x + y.
(* this two lines the same*)
pose x := _ + 1.
pose g n := n + 1.

pose pair x y := (x, y).
Abort.

Lemma test x : f x + f x = f x.
Proof.
set c := {3}(f _).
set a := {1}(f _).
set b := {1}(f _).
Abort.


Lemma test x y z : x + y = z.
Proof.
set carried_add := _ x.

Abort.



Lemma test (g : nat -> nat) : g 2 + g 8 = g 2 + g 2.
Proof.
set x := {+1 3}(g 2).
Abort.

Lemma test x t (Hx : x = 3) : x + t = 4.
Proof.
set z := 3 in Hx.
Abort.




Lemma subnK : forall m n, n <= m -> m - n + n = m.
Proof.
move => m n le_n_m.

move: m le_n_m. 
(* 
move => ... change state (like intros)
move :  ... set to state new value
 *)
Abort.


Ltac mytac H := rewrite H.

Lemma test x y (H1 : x = y) (H2 : y = 3) : x + y = 6.
Proof.
do [mytac H2] in H1 *.
Abort.

Require Import PeanoNat.

Lemma test : True.
Proof.
have : _ * 0 = 0.
have : forall x y, (x, y) = (x, y + 0).
have : 1 = 1 + 0 by exact: eq_refl.
move => H.
have H23 : 3 + 2 = 2 + 3 by rewrite Nat.add_comm.
Abort.

Lemma test a : 3 * a - 1 = a.
Proof.
have -> : forall x, x * a = a.
(* -> means assert and rewrite*)
Abort.

Lemma test (G P : Prop): G.
Proof.
suff have H : P.




































