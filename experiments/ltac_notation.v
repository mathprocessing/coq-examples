Require Import Arith.
From MyExamples Require Import sorry.

Example test : 3 <= 3 := ltac:(exact (Nat.le_refl _)).

Definition swap {A B : Type} : A * B -> B * A :=
  fun p =>
    match p with
    | (a, b) => (b, a) (* what if (b, a) replaced by _ *)
    end.


Definition swap' {A B : Type} : A * B -> B * A :=
  fun p =>
    match p as m with
    | m => (snd m, fst m)
    end.
    
Check ltac:(apply (Nat.le_refl 3); exact (Nat.add_comm 5 4)).

Check Nat.le_refl 3.

Example test' : Nat.le 3 3 := ltac:(apply Nat.le_refl).

Example assoc m n k : (m + n) + k = m + (n + k) := sorry.
Example comm m n : m + n = n + m := sorry.
Example zero_right n : n + 0 = n := sorry.

Example add_comm a b c n : n = 0 -> a + b + c + n = b + a + c.
Proof.
intro h.
rewrite h.
rewrite zero_right.
rewrite (assoc b).
rewrite (comm b).
rewrite (assoc a).
rewrite (comm b c).
rewrite (assoc a).
reflexivity.
Qed.
Example test_rw n : n = 0 -> n + 1 = 1 := fun h => ltac:(rewrite h, <-zero_right; apply eq_refl).


Record Bound : Set := mkBound {
first: nat;
second: nat;
bound_prop: first <= second;
}.

Check {| first:= 5; second := 5; bound_prop := Nat.le_refl _ |} : Bound.
Check mkBound 1 10 ltac:(auto with arith).

Hint Resolve bound_prop : arith.

Example bound_transitive b1 b2 : second b1 <= first b2 -> first b1 <= second b2.
Proof.
intro h.
eapply (le_trans).
apply (bound_prop).
eapply (le_trans).
exact h.
exact (bound_prop b2).
Qed.

Definition b1_var := (mkBound 2 3 ltac:(auto with arith)).
Definition b2_var := (mkBound 3 5 ltac:(auto with arith)).

Example test_bound_transitive : 2 <= 5.
Proof.
assert (h := bound_transitive b1_var b2_var).
simpl in h.
exact (h (ltac:(apply le_refl))).
Qed.

Ltac simp :=
  intros; match goal with
  | |- True => exact I
  | |- ?a = ?a => exact eq_refl
  | |- ?a <-> ?a => apply iff_refl
  | H : ?x |- ?x => assumption
  | H : ?a -> ?b |- ?b => apply H; clear H; simp
  | H : _ <-> _ |- _ => destruct H; simp
  | H1 : ?a -> ?b, H2 : ?b |- _ => clear H1; simp (* not useful hypothesis *)
  | H : True |- _ => clear H; simp
  | H : False |- _ => exfalso; exact H
  
  (* First branch*)
  | H : _ <= _ |- _ <= _ => eapply (le_trans _ _ _ (H)); idtac"appH"; simp
  
  (* Second branch*)
  | |- _ <= _ => eapply (le_trans _ _ _ (bound_prop _)); idtac"appProp"; simp
  
  (* Rewrite branch *)
  | H : _ = _ |- _ => idtac"try rw"; rewrite H; idtac"rwH"; simp
  
  (* Makes infinite loop in example inf_rw_loop *)
  (* | H : _ = _ |- _ => idtac"try rw"; rewrite <-H; idtac"rwH<-"; simp *)
  
  (* Simple cases *)
  | b : Bound |- first ?b <= second ?b => apply (bound_prop b); simp
  | |- ?a <= ?a => apply le_refl
  | _ => fail
  end.



Goal forall b1 b2 : Bound, first b1 <= second b1.
Proof.
Info 0 simp.
Show Proof.
Qed.

Goal forall b1 b2 : Bound, second b1 <= first b2 -> first b1 <= second b2.
Proof.
Info 0 simp.
Show Proof.
Qed.

Goal forall b1 b2 : Bound, second b1 = first b2 -> first b1 <= second b2.
Proof.
Info 0 simp.
Show Proof.
Qed.

Goal forall b1 b2 : Bound, b1 = b2 -> b2 = b1.
Proof.
Info 0 simp.
Show Proof.
Qed.

Goal forall {T : Type} (A B C : T), A = B -> B = C -> A = C.
Proof.
Info 0 simp.
Show Proof.
Qed.

Goal forall b1 b2 b3 : Bound, b1 = b2 -> b2 = b3 -> first b1 = first b3.
Proof.
Info 0 simp.
Show Proof.
Qed.

Example inf_rw_loop: forall b1 b2 b3 : Bound, b1 = b2 -> b2 = b3 -> first b1 <= second b3.
Proof.
Info 0 simp.
Show Proof.
Qed.

Goal forall b1 b2 b3 : Bound, 
second b1 = first b2 -> second b2 = first b3 -> first b1 <= second b3.
Proof.
Info 0 simp.
Show Proof.
Qed.

Goal forall b1 b2 b3 : Bound, 
second b1 <= first b2 -> second b2 <= first b3 -> first b1 <= second b3.
Proof.
Info 0 simp.
Show Proof.
Qed.


Goal forall b1 b2 b3 b4 b5 : Bound, 
second b3 = first b4 -> 
second b1 <= first b2 ->
first b5 = second b4 -> 
second b2 <= first b3 -> 
first b1 <= second b5.
Proof.
Fail Info 0 simp.
intros.
symmetry in H1.
Info 0 simp.
Show Proof.
Qed.

Goal False <-> False. Proof ltac:(simp).
Goal True. Proof ltac:(simp).
Goal forall A : Prop, A -> A. Proof ltac:(simp).
Goal forall A : Prop, A -> (A -> A).
Proof.
Info 0 simp.
Show Proof.
Qed.


