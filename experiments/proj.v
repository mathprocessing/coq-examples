Require Import Unicode.Utf8.
Require Import PeanoNat.


Definition plus_is_one m n :
  m + n = 1 -> m = 0 ∧ n = 1 ∨ m = 1 ∧ n = 0 := sorry.

Example projection1 (A B : Prop) : A ∧ B → A := fun h => proj1 h.
Example projection2 (A B : Prop) : A ∧ B → B := fun h => proj2 h.

Example test2 n m : n + m = 1 -> n < 2.
Proof.
intro h.
eapply plus_is_one in h.
destruct h as [h01|h10].
assert (Hn0 : n = 0) by exact (proj1 h01).
rewrite Hn0.
exact (le_S 1 1 (le_n 1)).
rewrite (proj1 h10).
change (2 <= 2). exact (le_n 2).
Qed.

Ltac have_ineq :=
match goal with
| h1 : le ?a ?b, a : ?t |- _ => idtac "found" a ":" t "," a "≤" b
| _ => idtac "not found"
end.

Ltac have_ineq_2 :=
match goal with
| |- ?a ∨ ?b => idtac "found goal" a b
| _ => idtac "not found"
end.

Example destruct_or (A B C : Prop) : (A → B) → (A → C) → (A → B ∨ C).
Proof.
auto.

Show Proof.
Qed.

(** n ≤ 2 ∨ n = 3 *)

(**
How Nat.le_succ_r proved in standard library?


 *)

Example test4 n : n ≤ 3 → n ≤ 2 ∨ n = 3.
Proof.
intro h.
now apply (Nat.le_succ_r _ ).
(**
now ltac_expr
Run tactic followed by easy. This is a notation for tactic; easy.
 *)
Show Proof.


Qed.

Example test5 n m : n + m = 1 -> n < 2.
Proof.
intro h.
eapply plus_is_one in h.
destruct h as [h01|h10].
assert (Hn0 : n = 0) by exact (proj1 h01).
rewrite Hn0.
auto with arith.
(* exact (le_S 1 1 (le_n 1)). *)
rewrite (proj1 h10).
auto with arith.
(* change (2 <= 2). exact (le_n 2). *)
Qed.