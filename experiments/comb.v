Require Import Unicode.Utf8.
Require Import Arith.
From MyExamples Require Import sorry.

Fixpoint comb (a b : nat) {struct a} :=
match a, b with
| _, 0 => 1
| 0, _ => 0
| S n, S k => comb n (S k) + comb n k
end.

Goal comb 3 0 = 1. Proof eq_refl.
Goal comb 4 2 = 6. Proof eq_refl.
Goal comb 10 0 = 1. Proof eq_refl.
Goal comb 10 1 = 10. Proof eq_refl.
Goal comb 10 2 = 45. Proof eq_refl.
Goal comb 10 3 = 120. Proof eq_refl.

Example comb_zero n : comb n 0 = 1.
Proof.
case n; reflexivity.
Qed.

Example zero_comb n : comb 0 (S n) = 0.
Proof eq_refl.

Example comb_one n : comb n 1 = n.
Proof.
induction n as [|n ih].
reflexivity.
simpl.
rewrite ih.
rewrite comb_zero.
(* Search (?n + 1 = S ?n). *)
Check Nat.add_1_r : ∀ n : nat, n + 1 = S n.
apply Nat.add_1_r.
Qed.

Lemma comb_step n k : 
  comb (S n) (S k) = comb n (S k) + comb n k.
Proof eq_refl.

Lemma mul_next_even n: Nat.even ((n + 1) * n) = true.
Proof.
rewrite Nat.even_mul, (Nat.even_add n 1), Nat.even_1.
case (Nat.even n); simpl; reflexivity.
Qed.

Example div_mul_2_helper n (h : Nat.even n = true) :
  exists (k : nat), n = k * 2.
Proof.
eapply (ex_intro).
rewrite Nat.mul_0_l; trivial.
Admitted.

Example mul_div_2 n: (n * 2) / 2 = n.
Proof. apply (Nat.div_mul n 2). discriminate. Qed.

Example div_mul_2 n (h : Nat.even n = true) : 
  n / 2 * 2 = n.
Proof.
elim (div_mul_2_helper n h); intros k h2.
rewrite h2, mul_div_2; trivial.
Qed.

Example two_neq_zero : 2 = 0 -> False := ltac:(discriminate).

Example comb_two n : comb (S n) 2 = (n + 1) * n / 2.
Proof.
induction n as [|n ih].
reflexivity.
rewrite comb_step.
rewrite comb_one, ih.
rewrite <-((Nat.mul_cancel_l _ _ 2) two_neq_zero).
rewrite Nat.mul_add_distr_l.
rewrite Nat.mul_comm.
rewrite div_mul_2.
rewrite (Nat.mul_comm 2 (_ / _)).
rewrite div_mul_2.
rewrite (Nat.mul_comm 2).
rewrite Nat.add_1_r.
rewrite <-Nat.mul_add_distr_l.
rewrite Nat.mul_comm.
rewrite Nat.mul_cancel_r.
rewrite (Nat.add_comm (S n)).
rewrite (Nat.add_succ_r 1 n).
rewrite Nat.add_succ_r.
rewrite Nat.add_comm; reflexivity.
discriminate.
rewrite <-Nat.add_1_r.
exact (mul_next_even (n + 1)).
exact (mul_next_even n).
Qed.

(* Pascal's triangle *)
Goal comb 0 0 = 1. Proof eq_refl.
Goal comb 0 1 = 0. Proof eq_refl.
Goal comb 0 2 = 0. Proof eq_refl.
Goal comb 0 3 = 0. Proof eq_refl.

Goal comb 1 0 = 1. Proof eq_refl.
Goal comb 1 1 = 1. Proof eq_refl.
Goal comb 1 2 = 0. Proof eq_refl.
Goal comb 1 3 = 0. Proof eq_refl.

Goal comb 2 0 = 1. Proof eq_refl.
Goal comb 2 1 = 2. Proof eq_refl.
Goal comb 2 2 = 1. Proof eq_refl.
Goal comb 2 3 = 0. Proof eq_refl.

(**
n
0   1   2   3   4   5   6   7   8   9
----------------------------------------
1   1   1   1   1   1   1   1   1   1   | 0 k
0   1   2   3   4   5   6   7   8   9   | 1
0   0   1   3   6   10  15  21  28  36  | 2
0   0   0   1   4   10  20  35  56  84  | 3
0   0   0   0   1   5   15  35  70  126 | 4
0   0   0   0   0   1   6   21  56  126 | 5
0   0   0   0   0   0   1   7   28  84  | 6
0   0   0   0   0   0   0   1   8   36  | 7
0   0   0   0   0   0   0   0   1   9   | 8
0   0   0   0   0   0   0   0   0   1   | 9
Column sum
1   2   4   8   16  32  64  128 256 512 = 2^n
Column max
1   1   2   3   6   10  20  35  70  126 = ?
*)

Example comb_diag n: comb n n = 1 := sorry.

Example zero_if_lt n k (h : n < k) : comb n k = 0.
Proof.
assert (h2 : exists m, n + m = k) by exact sorry.
assert (m : nat) by exact sorry.
assert (H : n + m + 1 = k) by exact sorry.
rewrite <- H.
assert (h3 : _) by exact (comb_diag n).
induction m.
case n as [|pn].
simpl; trivial.
replace (S pn + 0) with (S pn).
replace (S pn + 1) with (S (S pn)).
rewrite comb_step.
admit.
Admitted.

Example comb_updiag n: comb (S n) n = S n := sorry.
(* Example comb_sym n k: comb n k = comb n n-k := sorry. *)
Example comb_sym n k: comb (n+k) n = comb (n+k) k := sorry.

Check comb_one : ∀ n : nat, comb n 1 = n.

(** Property testing in Coq o_O *)
Goal comb 0 0 = 1. Proof. apply comb_diag. Undo. exact eq_refl. Qed.
Goal comb 1 0 = 1. Proof. apply comb_updiag. Undo. exact eq_refl. Qed.
Goal comb 4 3 = 4. Proof. apply comb_updiag. Undo. exact eq_refl. Qed.
Goal comb 4 3 = comb 4 1. Proof. exact (comb_sym 3 1). Undo. exact eq_refl. Qed.


Ltac rw_step := 
match goal with
 | _ => rewrite comb_zero; rw_step
 | _ => rewrite zero_comb; rw_step
 | _ => rewrite comb_one; rw_step
 | _ => rewrite comb_two; rw_step
 | _ => rewrite comb_step; rw_step
 | _ => idtac
end.

Example test' n: comb n n = 1.
Proof.
Info 0 rw_step.
reflexivity.

Qed.


Example test : comb 3 2 = 0 + 0 + (0 + 1) + (0 + 1 + 1).
Proof.
rewrite 10? comb_step.
rewrite 10? zero_comb.
rewrite 10? comb_zero.
reflexivity.
Qed.


