Module example1.

Parameter f : nat -> nat.
Axiom f_zero : f 0 = 0.
Axiom f_step : 
forall n : nat, n > 0 -> f n = f (n - 1).

Local Hint Rewrite 
  f_zero
  f_step
  : core.

Example test: f 1 = 1.
  rewrite 5?f_zero, 5?f_step.
Abort.

(*
f (1 - 1 - 1 - 1 - 1 - 1) = 1
2/6
1 - 1 - 1 - 1 - 1 > 0
3/6
1 - 1 - 1 - 1 > 0
4/6
1 - 1 - 1 > 0
5/6
1 - 1 > 0
6/6
1 > 0

if goal is notprovable then we need to 
  going back one or several step

if goal is almost provable we construct abstract planning

if goal is provable it's obvious to prove, but
  we sometimes need to explore graph of states to learn something, or find another solutions of task,
    or find some subtasks(new set of hypothesis that can help to planning in future).
*)

End example1.

Module example2.

Require Import Unicode.Utf8.

Parameter f : nat -> nat.
Axiom f_zero : f 0 = 0.
Axiom f_step : ∀ n, f n > 0 -> f n = f (n + 1).
Axiom f_sum : f 0 + f 1 + f 2 = 3.


Ltac mytac := idtac "hello".

Create HintDb my.
Hint Resolve f_zero f_step f_sum : my.

Lemma f_one_less_4 : f 1 < 4.
Proof. Admitted.

Example test : ∃ z, f 2 = z.
  assert (Hcase : f 1 = 0 \/ f 1 > 0) by admit.
  assert (hsum : _) by exact f_sum.
  rewrite f_zero in hsum.
  simpl in hsum.
  case Hcase; intro h.
  + replace (f 1) with 0 in hsum.
    simpl in hsum.
    exists 3. exact hsum.
  + assert (less_4 : _) by exact f_one_less_4.
    assert (f_one_cases : f 1 = 1 \/ f 1 = 2 \/ f 1 = 3) by admit.
    case f_one_cases; intro h2.
    rewrite h2 in hsum.
    simpl in hsum.
    





  
  auto with nocore my.
Admitted.




End example2.