Require Import Unicode.Utf8.
Require Import Ascii.
Require Import String.
Require Import Arith.
From MyExamples Require Import sorry.

Example comm : ∀ x y, x + y = y + x := sorry.

(*
Examples of code:
induction stack as [|[|]];
induction n as [|n IH].

  intros stack l H; induction l in stack, H |- *; simpl.
    auto using Sorted_merge_stack.

*)
Example add_comm_n_1 n : n + 1 = 1 + n.
Proof.
induction n as [|n IH].
exact eq_refl.
simpl in *.
rewrite <-IH.
exact eq_refl.
Qed.

Fixpoint f (x y : nat) :=
match x with
| 0 => y
| S n => S (f n y)
end.

Fixpoint g (x y : nat) :=
match x with
| 0 => y
| S n => g n (S y)
end.

(**
a1: g 0 y = y
a2: g (S n) y = g n (S y)
*)

Example g_a1 x : g 0 x = x := eq_refl.
Example g_a2 x y : g (S x) y = g x (S y) := eq_refl.

Hint Resolve g_a1 g_a2 add_comm_n_1: arith.

(* throw one from x to y *)
Example i_step k m : g (S k) (S m) = g k (S (S m)) := g_a2 k (S m).
Example back_step x y : g x (S y) = g (S x) y := eq_sym (g_a2 x y).

Example throw_n x y n : g (x + n) y = g x (y + n).
Proof.
induction (n) as [|m ih].
simpl.
admit.
.2




(* induction n.
auto with arith.
rewrite Nat.add_comm.
simpl.
rewrite Nat.add_comm. *)
Qed.

Example g_comm_0 x : g x 0 = g 0 x.
Proof.  
  induction x; trivial.
  Check g_a2 x 0.
  simpl.
  (*
  IHx : g x 0 = g 0 x
  goal: g x 1 = S x
  
  IHx : g (S k) 0 = g 0 (S k)
  goal: g (S k) 1 = S x


  IHx : g k 1 = g 0 (S k)
  goal: g k 2 = S x
  
  *)
Qed.


Example g_zero_x_add_one x :
  g 0 x + 1 = g 1 x.
Proof. simpl; auto with arith. Qed.

Example g_x_zero_helper_2 x :
  g 1 x = g x 1.
Proof.
  
  induction x.
  trivial.
  Check g_a2 0 (S x).
(**
IHx : S (S n) = g (S n) 1
______________________________________(1/1)
S (S x) = g (S x) 1

S x = g (S n) 1


goal: S (g (S n) 1) = g (S x) 1
goal: S (g x 1) = g x 2



S (S x) = g (S x) 1

*)
Qed.

Example g_x_zero_helper x :
  g x 0 + 1 = g x 1.
Proof.
  elim x; trivial; intro n.
  intro ih.
  replace (g (S n) 1) with (g (S (S n)) 0).
  simpl.
  rewrite add_comm_n_1.
  simpl.
  simpl in ih.
  replace (g (S n) 0) with (g n 1).
  rewrite <-ih.
  simpl.
  (**
  g (S n) 0 + 1 = g (S n) 1
  g n 1 + 1 = g (S n) 1
  
  *)
Admitted.

Example g_x_zero x : g x 0 = x.
Proof.
induction x as [|x ih].
trivial. simpl.
replace (g x 1) with (g x 0 + 1).
rewrite ih.
rewrite add_comm_n_1. trivial.
(*
goal: g x 0 + 1 = g x 1
let x = n + 1
g x 0 = g (S n) 0 = g n 1
goal: g n 1 + 1 = g x 1
*)
apply g_x_zero_helper.
Qed.

Example f_x_zero x : f x 0 = x.
Proof.
induction x as [|x ih].
trivial.
simpl.





