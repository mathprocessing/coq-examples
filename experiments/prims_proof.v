Require Import List.
Require Import Coq.Sets.Finite_sets.
Require Import Coq.Sets.Finite_sets_facts.

Check Empty_set.

Check finite_cardinal.
forall (U : Type) (X : Ensemble U),
  Finite U X -> exists n : nat, cardinal U X n

Parameter Vertex : Set.
Record Edge : Set := mkEdge {
  v1 : Vertex;
  v2 : Vertex
}.
(* weighted graph, 0 < weights < +infinity *)
Record Graph := {
  vs : set Vertex;
  adj : Vertex -> Vertex -> Bool;
}.

Parameter e : Edge.
Example testProjectionType1 : (v1 e : Vertex) = _ := eq_refl.
Example testProjectionType2 : (v2 e : Vertex) = _ := eq_refl.

(* Inductive Edge' : Type :=
| edge : Vertex -> Vertex -> Edge'. *)

Parameters v u : Vertex.

Definition e1 := mkEdge v u.

Eval compute in (
  match e1 with
  | {| v1 := s1; v2 := s2 |} => (s1, s2)
  end).
  (* (v, u) : Vertex * Vertex *)

  Parameter deleteVertex : Graph -> Vertex -> Graph.

Definition emptyGraph := nil : Graph.

Definition singleton := nil : Graph.

(*
Minimum spanning tree (MST) properties:
  1. MST connects all graph vertices.
  2. sum of length of all edges in MST is a minimal.
  3. MST is also minimum subgraph that connected all vertices in
     target graph.
*)







Example : mstree ()