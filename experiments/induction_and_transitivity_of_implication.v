From MyExamples Require Import sorry.
Require Import Unicode.Utf8.
Require Import PeanoNat.
(**
a1: x + y + z = 0
s = 0 1 -1 0 (1 -1 0)
s[0] = 0
s[1] = 1
s[2] = -1
prove that `s[n] = [0, 1, -1][n % 3]`
assume we have 
ih: addIndex((s[n], s[n+1], s[n+2]), 0) = 0, 1, -1
then we have
=> addIndex((s[n], s[n+1], s[n+2]), 3) = 0, 1, -1
then we must prove base case for some n.
Let's prove base case for n = 0:
(s[0], s[1], s[2]) = 0, 1, -1 by definition

induction principle:
ih: P(n)
goal: P(n+1)
ind_principle: ih and base ==> set_can_be_filled
ih: forall n in [a, b]. P(n) -> P(n+1), base: P(a) ==> can_be_filled: forall n in [a, b+1]. P(n)

00000001111111111100000
       a        b
Let b = +inf

ih: forall n. (n >= a) -> (P(n) -> P(n+1)), base: P(a) ==> forall n. (n >= a) -> P(n)

Generalize (n >= a) to some Set:

ih: forall n. n in Set -> (P(n) -> P(n+1)), base: P(min(Set)) ==> forall n. n in S -> P(n)

Generalize (+1) successor operation to some function `f`:

ih: forall n : Set. P(n) -> P(n+1), base: P(a) ==> forall n. n in S -> P(n)

Принцип `set_can_be_filled_with_property_from_base_case` должен выводиться из:
* base: P(a)
* step: forall n in Set. P(n) -> P(f(n))
* f целиком заполняет множество начиная с элемента базы `a`:
  apply implication_transitive: forall P Q R. (P -> Q) -> (Q -> R) -> (P -> R)
  - Стоит обратить на понятие: _транзитивное замыкание_ бинарного отношения R на множестве X есть наименьшее _транзитивное отношение_ T на X, включающее R
  + `imp_set` are formed from range[a, b]
  + maybe `fold imp in imp_set ==> `


Например для графа G:
Где `step: P(node) -> P(neighbours(node))`
запонение всей компоненты связности происходит начиная с любой вершины, входящей в компоненту.
`have_path: forall u v. exists path(u, v)` - из любой вершины можно попасть в любую другую вершину за конечное число шагов (транзитивных переходов).
* для множества натуральных чисел это будет выглядет так:
  have_path: forall n m : nat. exists path_in_graph(G, n, m) where G = minimal_transitive_rel(successor)
*)

Example helper_1 n : (n ≠ 0) = (n > 0) := sorry.
Example contrapose (A B : Prop) : (¬B → ¬A) → (A → B) := sorry.
Example gt_self n : n > n → False := sorry.
Example cancel_left m n : m + n = m → n = 0 := sorry.

(* s is some function on natiral numbers *)
Parameter s : nat -> nat.
(*Axioms for s*)
Example s_base : s 0 = 1 := sorry.
Example s_step : ∀ n, s n + s (n + 1) = 1 := sorry.

(* Simple examples *)
Example s_1 : s 1 = 0.
Proof.
pose (Hstep := s_step 0).
rewrite s_base in Hstep.
eapply (cancel_left 1 (s 1) Hstep).
Qed.

Example step_zero n m : n = 0 → n + m = 1 → m = 1 := sorry.
Example step_one n m : n = 1 → n + m = 1 → m = 0 := sorry.

Create HintDb my.
Hint Resolve step_zero step_one s_base s_1 s_step: my.

Example s_2 : s 2 = 1.
Proof.
debug auto 9 with my.
eapply step_zero.
exact s_1.
apply s_step.
Show Proof. (*(step_zero (s 1) (s 2) s_1 (s_step 1))*)
Qed.




(* Lemmas *)








