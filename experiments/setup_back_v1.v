Require Import Arith.
Require Import Lia.
Require Import Unicode.Utf8.
Require Import List.
Import ListNotations.
Require Import Ascii.
Require Import String.

Axiom sorry : forall {A : Type}, A.

Ltac back depth :=
  lazymatch (eval compute in depth) with
  | 0 => idtac "depth = 0"
  | _ =>
    match goal with
      | |- True => trivial
      | |- (le _ _) => 
        let n := (eval compute in (depth - 1)) in
        (simple apply le_n; back n) ||
        (simple apply le_S; back n)
      | _ => assumption || fail "No such assumption"
    end
  end.
  
Example test_back : le 5 100.
Proof.
back 100.
Show Proof.
Qed.

Section section1.

Variables (x y z : nat).

Example test: (x ≤ 0 ∨ x ≥ 1) -> x <= x * x := sorry.

(* Create HintDb my. *)
Hint Resolve test : arith.

Goal x ≤ x * x.
Proof.
Info 0 back.
debug auto 3 with arith.

Qed.

Goal x + (2 * y + z) = (x + y) + z + y.
Proof.
ring.
Defined.


End section1.

Ltac back2 d :=
  match d with
  | 0 => fail "depth = 0"
  | ?x => idtac x
  end.

Ltac back3 n :=
  lazymatch n with
  | 0 => fail "0"
  | _ => idtac n
  end.
