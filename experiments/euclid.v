Require Import Unicode.Utf8.
Require Import PeanoNat.
Require Import Compare_dec.
Require Import Wf_nat.

Local Open Scope nat_scope.
Implicit Types a b n q r : nat.

Inductive diveucl a b : Set :=
  divex : ∀ q r, b > r → a = q * b + r → diveucl a b.

Check {q : nat | ∃ x : nat, x = q}.

Goal {q : nat | ∃ x : nat, x = q}.
Proof.


Abort.

Lemma eucl_dev : ∀ n, n > 0 → ∀ m : nat, diveucl m n.
Proof.
  intros n H m; induction m as (m,H0) using gt_wf_rec.
  destruct (le_gt_dec n m) as [Hlebn|Hgtbn].
  destruct (H0 (m - n)) as (q,r,Hge0,Heq); eauto with arith.
  apply divex with (S q) r; trivial.
  simpl; rewrite <- plus_assoc, <- Heq; auto with arith.
  apply divex with 0 m; simpl; trivial.
Defined.

Lemma quotient :
  forall n,
    n > 0 ->
    forall m:nat, {q : nat |  exists r : nat, m = q * n + r /\ n > r}.
Proof.
  intros n H m; induction m as (m,H0) using gt_wf_rec.
  destruct (le_gt_dec n m) as [Hlebn|Hgtbn].
  destruct (H0 (m - n)) as (q & Hq); auto with arith; exists (S q).
  destruct Hq as (r & Heq & Hgt); exists r; split; trivial.
  simpl; rewrite <- plus_assoc, <- Heq; auto with arith.
  exists 0; exists m; simpl; auto with arith.
Defined.