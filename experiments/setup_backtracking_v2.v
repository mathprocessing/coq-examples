Require Import Arith.
Require Import Lia.
Require Import Unicode.Utf8.
Require Import List.
Import ListNotations.
Require Import Ascii.
Require Import String.


Local Open Scope string_scope.

Example Hello := "Hello".
Example World := "World".
Eval compute in Hello ++ String Space "" ++ World ++ "!".

Axiom sorry : forall {A : Type}, A.

Ltac back depth := 
  lazymatch eval compute in depth with
  | 0 => fail "depth = 0"
  | _ =>
    match goal with
      | |- True => trivial
      | |- (le _ _) => 
        let n := (eval compute in (depth - 1)) in
        (simple apply le_n; back n) ||
        (simple apply le_S; back n)
      | _ => assumption || fail "No such assumption"
    end
  end.
  
Ltac add_tac x y := 
  match eval compute in x with
  | 0 => idtac "x is 0: x + y =" y
  | _ => let n := eval compute in (x + y) in
    idtac x "+" y "=" n
  end.
  
Ltac dec x := 
  match eval compute in x with
  | 0 => idtac "done"
  | _ => let n := eval compute in (x - 1) in
    idtac "n ="n; dec n
  end.

Ltac show_binary x s := 
  lazymatch eval compute in x with
  | 0 => idtac "done"
  | _ => let n := eval compute in (x / 2) in
    lazymatch eval compute in (Nat.modulo x 2) with
    | 0 => idtac " bin 0"; show_binary n s
    | _ => idtac " bin 1"; show_binary n s
    end
  end.
  
(* return binary representation of a number as string *)
Ltac to_binary x s :=
  lazymatch eval compute in x with
  | 0 => idtac "done"
  | _ => let n := eval compute in (x / 2) in
    lazymatch eval compute in (Nat.modulo x 2) with
    | 0 => idtac " bin 0"; show_binary n s
    | _ => idtac " bin 1"; show_binary n s
    end
  end.

Fixpoint append_char (s : string) (ch : ascii) :=
match s with
| "" => String ch ""
| String x xs => String x (append_char xs ch)
end.

Check (0, 1) : nat * nat.
Check ("hello", "W"%char) : string * ascii.

Definition eq_chars (c1 c2 : ascii) := eqb (String c1 "") (String c2 "").

Fixpoint split_by_char (s acc : string) (ch : ascii) :=
match s with
| "" => (acc, s) (* don't know order of pair *)
| String x xs => 
  match eq_chars x ch with
  | true => (acc, s)
  | false => split_by_char (xs) (append_char acc x) ch
  (* `x` because char con't mutate s or acc *)
  end 
end.

Eval compute in split_by_char "hello world" "" " ".

Eval compute in append_char "hello" "7".

Check String "H" "ello".
Example test_back : le 5 100.
Proof.
to_binary 10 "".
Show Proof.
Qed.

Section section1.

Variables (x y z : nat).

Example test: (x ≤ 0 ∨ x ≥ 1) -> x <= x * x := sorry.

(* Create HintDb my. *)
Hint Resolve test : arith.

Goal x ≤ x * x.
Proof.
Info 0 back.
debug auto 3 with arith.

Qed.

Goal x + (2 * y + z) = (x + y) + z + y.
Proof.
ring.
Defined.


End section1.
