Require Import Unicode.Utf8.
Require Import Arith.
Axiom sorry : forall {A : Type}, A.
Example comm n m : n + m = m + n := sorry.

Lemma comm2 n m : n + m = m + n.
Proof comm n m.

(**
fun ha hb => _
fun ha hb => conj _ _
fun ha hb => conj ha _
fun ha hb => conj ha hb
*)
Fact conj_2 (A B : Prop): A -> B -> (A /\ B).
Proof fun ha hb => conj ha hb.

(** Use auto-reducing expressions:
1 < 2 ≡ 2 ≤ 2 *)
Fact lt_1_2 : 1 < 2.
Proof (le_n 2).


Fact lt_test : 5 < 11. (** OK *)
Proof. Info 0 auto with arith. Qed.

Fact lt_test_2 : 5 < 12. (** Fails *)
Proof. assert_fails solve [ auto with arith ]. Abort.
