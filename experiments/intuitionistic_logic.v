(** Intuitionistic Logic
http://us.metamath.org/ileuni/notnot1.html *)

Section Axioms.

Axiom sorry : forall {A : Type}, A.

Example duplicate_goal : forall P : Prop, P -> P -> P :=
  fun _ _ hp => hp.

Definition decidable (P : Prop) := P \/ ~P.

Axiom mp : forall {P Q : Prop} (hp : P) (hq : P -> Q), Q. (* modus ponens *)
Axiom con2i : forall {A B : Prop} (h : A -> ~B), B -> ~A. (* Contraposition inference *)
Axiom id' : forall {A : Prop},  A -> A.
Example notnotl (A : Prop) : A -> ~~A.
Proof. (* exact (@con2i (~A) A (@id' (~A))). *)
  assert (h1 : ~A -> ~A) by exact (id').
  assert (h2 : A -> ~~A) by exact (@con2i (~A) A h1).
  exact h2.
Qed.

Example sylbi {P Q R : Prop} : (P <-> Q) -> (Q -> R) -> (P -> R) := 
  fun (hpq : P <-> Q) hqr hp => 
    let hPimpQ : P -> Q := proj1 (iff_and hpq : (P -> Q) /\ _)
    in hqr (hPimpQ (hp : P) : Q) : R.
Example pm2_53 (A : Prop) : (~A \/ A) -> (~~A -> A) := sorry.

Example orcom (A B : Prop) : (A \/ B) <-> (B \/ A) := sorry.

Example orcom_exact (A : Prop)
  : (A \/ ~A) <-> (~A \/ A).
Proof.
  refine (orcom A (~A)).
Qed.


(*
orcom -----|
  |        |
  |        orcom_exact
  |        |
  |--->|<--|
       |
    (some \/ ~some) <-> (~some \/ some)
*)

(* 
bitri: 
  1. decidable A <-> (A \/ ~A) => X <-> Y   | arg1
  2. (A \/ ~A) <-> (~A \/ A)   => Y <-> Z   | arg2
  3. decidable A <-> (~A \/ A) => X <-> Z   | result
=> bitri : X <-> Y -> Y <-> Z -> X <-> Z
*)
Example bitri {X Y Z : Prop} :
  X <-> Y -> Y <-> Z -> X <-> Z := sorry.

Example notnot2dc (A : Prop) : decidable A -> (~~A -> A).
Proof.
  assert (h3 : decidable A <-> (~ A \/ A)).
    eapply (bitri).
    (*definition of decidable applied automatically*)
    apply iff_refl. 

    
  assert (h4 : (~ A \/ A) -> (~~A -> A)) by exact (pm2_53).
  refine (sylbi _ _).
  exact h3.
  exact h4.
Qed.

Example notnot2dc_coq (A : Prop) : decidable A -> (~~A -> A).
Proof.
  intros hdec hnn.
  unfold decidable in hdec.
  case hdec.
  exact id'.
  intros hn.
  unfold not in hnn, hn.
  assert (hfalse : _) by exact (hnn hn).
  case hfalse.
Qed.


End Axioms.

(** Proofs by Coq *)

Example test1 (A : Prop) : A -> A :=
  fun H => H.

Example test2 (A : Prop) : A -> ~~A :=
  fun h1 h2 => h2 h1. (* flip function *)

Lemma not_provable_not_not_a_imp_a : forall A : Prop, ~~A -> A.
Proof. auto. Abort.