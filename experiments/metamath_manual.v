Module Example1.
Parameter term : Type.
(* Parameter wff : Type. well-founded formula *)
(** we use Prop instead *)

(* Parameter t r s : term. *)
Parameter zero : term.

(* Parameter P Q : Prop. *)

Parameter add : term -> term -> term.
Parameter eq : term -> term -> Prop.
(* Parameter imp : Prop -> Prop -> Prop. *)

Local Notation "0" := zero.
Local Notation "a + b" := (add a b).
Reserved Notation "x == y" (at level 75, no associativity).
Notation "a == b" := (eq a b).

Axiom A1 : forall t r s : term, t == r -> (t == s -> r == s).
Axiom A2 : forall t : term, (t + 0) == t.
Axiom mp : forall {P Q : Prop} (hp : P) (hq : P -> Q), Q. (* modus ponens *)

Theorem refl : forall t : term, t == t.
Proof.
  intro t.
  assert (step1 : t + 0 == t)                         by exact (A2 t).
  assert (step2 : t + 0 == t -> t + 0 == t -> t == t) by exact (A1 (t + 0) t t).
  assert (step3 : t + 0 == t -> t == t)               by exact (mp step1 step2).
  assert (step4 : t == t)                             by exact (mp step1 step3).
  exact step4.
Qed. (* fun t : term => mp (A2 t) (mp (A2 t) (A1 (t + 0) t t)) *)

Example duplicate_goal : forall P : Prop, P -> P -> P := fun _ _ hp => hp.

Theorem refl_without_modus_ponens : forall t : term, t == t.
Proof.
  intros.
  Check A1 (t + 0) t t (A2 t). (* step3 : t + 0 == t -> t == t *)
  (* eapply (A1 _ ?x ?x). (* Error: Unknown existential variable. *) *)
  apply duplicate_goal.
  refine (  (A1 (t + 0) t t (A2 t)) (A2 t)  ). (* reverse of proof_2 *)
  (* proof_2 = (mp (A2 t) (A1 (t + 0) t t (A2 t))) *)
  (* mp ?x (fun H -> H) = ?x => we don't need id function *)
  refine (  A1 (t + 0) t t (A2 t) (A2 t)  ). (* reverse of proof_1 *)
  (* proof_1 = mp (A2 t) (mp (A2 t) (A1 (t + 0) t t)) *)
Qed. (* fun t : term => A1 (t + 0) t t (A2 t) (A2 t) *)

(*
Try to emulate this in Coq:
1. ?              $? wff $2
2. ?              $? wff t = t
3. ?              $? |- $2          <= temporary variable (represent symbol sequence)
4. ?              $? |- $2 -> t = t
5. 1, 2, 3, 4 mp  $a |- t = t
*)
 
Example refl_proof_1 : forall t, t == t := 
  (fun t : term =>
    mp
      (A2 t)
      (mp
         (A2 t)
         (A1 (t+0) t t)
      )
  ).

Example refl_proof_2 : forall t, t == t := 
  (fun t : term =>
    mp
      (mp
         (A2 t)
         (A1 (t+0) t t (A2 t))
      )
      (fun H => H)
  ).

Theorem refl_proof_1_with_tactics : forall t, t == t.
Proof.
  intros.
  (* MM-PA> assign 1 mp 
  MM-PA> show new_proof/lemmon
  3 ?      $? |- $2
  4 ?      $? |- $2 -> t = t
  5 3,4 mp $a |- t = t                                     *)
  refine (mp _ _).
  (* MM-PA> assign 4 mp 
     3     min=?  $? |- $2
     6       min=?  $? |- $4
     7       maj=?  $? |- $4 -> ($2 -> t = t)              *)
  all: swap 1 2.
  refine (mp _ _).
  (* MM-PA> assign 3 a2
    7       min=? $? |- $4
    8       maj=? $? |- $4 -> ( $5 + 0 ) = $5 -> t = t     *)
  apply A2.
  apply (A1 _ t t).
  apply (A2 t).
Qed. (* fun t : term => mp (A2 t) (mp (A2 t) (A1 (t + 0) t t)) *)

Theorem refl_proof_2_with_tactics : forall t, t == t.
Proof.
  intros.
  refine (mp _ _).
  refine (mp _ _).
  apply (A2 t).
  apply (A1 _ t t).
  apply (A2 t).
  apply (fun H => H).
  Show Proof.
Qed. (* fun t : term => mp (mp (A2 t) (A1 (t + 0) t t (A2 t))) (fun H => H) *)

Theorem refl_experiment_find_another_proofs : forall t, t == t.
Proof.
  intros.
  refine (mp _ _).
  refine (mp _ _).
  eapply (mp _).
  apply A1.
  apply A2.
  apply A1.
  apply A2.
  apply id.
  Unshelve.
  Show Proof.
Abort. (* mp (mp (mp ?hp (A1 (t + 0 + 0) (t + 0) t (A2 (t + 0)))) (A1 (t + 0) t t (A2 t)))
             (id (A:=t == t)) *)

(* Error with unfold "==": eq is opacue *) (* try cbv delta in *. but this not helps. *)

Check 0.     (* : term *)
Check 0 + 0. (* : term *)
Check eq 0 (0 + 0). (* : Prop *)

Check 0 = 0 -> 0 = 0.
Fail Check 0 = 0 -> 0.
(* Set Printing Notations. *)
(* Unset Printing Notations. *)
End Example1.
(** example of set.mm propositions *)
Module Example2.
Section vars.

(* from set.mm file:
  $v ph $.  $( Greek phi $)
  $v ps $.  $( Greek psi $)
  $v ch $.  $( Greek chi $)
  $v th $.  $( Greek theta $)
  $v ta $.  $( Greek tau $)
  $v et $.  $( Greek eta $)
  $v ze $.  $( Greek zeta $)
  $v si $.  $( Greek sigma $)
  $v rh $.  $( Greek rho $)
  $v mu $.  $( Greek mu $)
  $v la $.  $( Greek lambda $)
  $v ka $.  $( Greek kappa $)
*)

Variables ph ps ch th: Prop.

(** Axioms *)
(* Rule of Modus Ponens *)
Example ax_mp: ph -> ((ph -> ps) -> ps) :=
  fun (H1 : ph) (H2 : ph -> ps) => H2 H1.

(* Axiom Simp. *)
Example ax_1: ph -> (ps -> ph) :=
  fun (H : ph) _ => H.

(* Axiom Frege. *)
Example ax_2:
  (ph -> (ps -> ch)) -> ((ph -> ps) -> (ph -> ch)) :=
    fun H1 H2 H3 => (H1 H3) (H2 H3).

(* Axiom Transp *)
Example ax_3:
  (~ph -> ~ps) -> (ph -> ps).
intros H1 H2.
unfold "~" in H1.
Admitted.

(** Theorems *)

Example id': ph -> ph.
intro H1.
assert (H2 : _) by exact (ax_1 H1).
assert (H3 : 
  (_ -> ps -> ch) -> _ -> _)
  by exact (fun h1 h2 => h1 H1 (h2 H1)).
assert (H3' : 
  (ph -> ps -> ch) -> (ph -> ps) -> ch).
  

Check H1.       (* : ph               *)
Check ax_1 H1.  (* : ps -> ph         *)
Check ax_mp H1. (* : (ph -> ps) -> ps *)

































End vars.
End Example2.

Module Example3.
Example test: forall x:bool, orb x true = true.
intro x.

simpl.
auto with bool. (* not useful*)
destruct x; auto.
Qed.
Example test2: forall x:bool, orb x true = true.
Fail refine (fun x : bool => 
    if x return (_ = true) then eq_refl else eq_refl).
(* The term "if x then eq_refl else eq_refl" has type "true = true" while it is expected to have type
 "(x || true)%bool = true". *)
refine (fun x : bool => 
  if x return ((x || true)%bool = true) then eq_refl else eq_refl).
Qed.
End Example3.



