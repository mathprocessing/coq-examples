Require Import Unicode.Utf8.
Require Import Reals.
Require Import Ring.
Require Import Arith.
Axiom sorry : forall {A : Type}, A.

(** Для того чтобы проверять ваши идеи доказательства, термы, функции, леммы используйте метод `Check` как можно чаще.
В теории без интерактивного режима(т.е. используя только лишь `Check`) можно проверить доказательства для любых теорем, но возможно, если л-терм будет длинным, то понабится много времени.
*)

(** Доказательство это лямбда функция, процесс доказательства это как путешествие по лабиринту:
Тип лямбда-терма - это место где сейчас находимся.
Сам лямбда-терм - это инструкция как в это место прийти из стартового состояния (из начала лабиринта).
*)

(* По умолчанию используются натуральные числа. *)
Check 1 = 1 : Prop.
Check 1%nat = 1%nat : Prop.
Check 1%Z = 1%Z : Prop.
Check 1%Z = 1%Z : Prop.

(* Задача получите не тип `Prop`, а тип `1 = 1`*)
(* Решение: *)
Check eq_refl 1 : 1 = 1.
(**
Тип лямбда-терма:  1 = 1
Лямбда-терм:       eq_refl 1
*)
(* ------------------------------------------------------------------------------------ *)
(* как в Coq заданы натуральные числа nat?                                              *)
(* ------------------------------------------------------------------------------------ *)

Check nat : Set.

(** Не путайте заглавную английскую букву `O` с цифрой `0` *)
Example zero_nat : 0 = O := eq_refl.
Example one_nat : 1 = S O := eq_refl.
Example two_nat : 2 = S (S O) := eq_refl.
Example three_nat : 3 = S (S (S O)) := eq_refl.
(** S - это функция successor
Посмотрим какой она имеет тип: *)
Check S : nat -> nat.
(** successor принимает аргумент натуральное число (n : nat) и возвращает натуральное число,
следующее за ним: S 0 = 1, S 1 = 2, ... S n = n + 1 хм... давайте докажем это *)


(** Попробуем найти эту лемму с помощью команды `Search`
https://coq.inria.fr/refman/proof-engine/vernacular-commands.html *)
Search (S ?n = Nat.add _ _).
(**
В поле messages видим вывод команды `Search`:
plus_n_Sm: ∀ n m : nat, S (n + m) = n + S m
ZL0: 2 = 1 + 1
Тоже неплохо, но продолжим поиск
*)
Search (Nat.add _ _ = S ?n).
(**
Отлично.
Nat.add_1_r: ∀ n : nat, n + 1 = S n
То что нужно.
Проверим доступна ли лемма:
*)

Check Nat.add_1_r : ∀ n : nat, n + 1 = S n.

(** Думаю, её нужно развернуть, для этого поищем что-то связанное с операцией `=` или eq *)
Search (?a = ?b -> ?b = ?a).

(** Нашли eq_sym: ∀ (A : Type) (x y : A), x = y → y = x
Применим: *)
Fail Check eq_sym (Nat.add_1_r). (* Не работает *)
Check eq_sym (Nat.add_1_r 5). (* Работает *)

(** функция eq_sym ожидает аргумент без квантора forall, подумаем как это исправить:
Иcпользуем лямбда функции, синтаксис `fun` *)
Check fun n : nat => eq_sym (Nat.add_1_r n).

(** или можно использовать placeholder `_`*)
Check eq_sym (Nat.add_1_r _). (* Удивительно, но работает *)

(** Совет: Активно используйте `_` в связке с exact, eapply, refine (eapply лучше чем apply) *)

Example add_one_is_succ : ∀ n : nat, S n = n + 1 := fun _ => eq_sym (Nat.add_1_r _).
(** Нам не понадобился интерактивный режим (режим с тактиками) чтобы это доказать *)

(** Интересно, а можно было доказать предыдущее утверждение не используя самый правый `_`? *)
(** Пусть библиотечная функция Nat.add_1_r была бы такой: *)
Example my_add_1_r {n : nat} : n + 1 = S n := sorry.
(** Наша функция отличается от библиотечной только наличием кривых скобок `{n : nat}`*)

(** Попытаемся снова доказать add_one_is_succ *)
Example add_one_is_succ' : ∀ n : nat, S n = n + 1 := fun _ => eq_sym (@my_add_1_r _).
Example add_one_is_succ'' : ∀ n : nat, S n = n + 1 := fun _ => eq_sym (my_add_1_r).
(* Получилось: было eq_sym (Nat.add_1_r _) стало `eq_sym (my_add_1_r)`*)
(** Аргументы функции могут явными (explicit) и неявными (implicit)
explicit arguments задаются круглыми скобками `(n : nat)`
implicit arguments задаются кривыми скобками `{n : nat}`
*)

(* ------------------------------------------------------------------------------------ *)
(* как в Coq заданы целые числа Z?                                                      *)
(* ------------------------------------------------------------------------------------ *)

Check Z : Set.

Example zero_int :  0%Z = Z0                := eq_refl. (* in binary 0 *)
Example one_int :   1%Z = Zpos  xH          := eq_refl. (* in binary 1 *)
Example two_int :   2%Z = Zpos (xO xH)      := eq_refl. (* in binary 10 *)
Example three_int : 3%Z = Zpos (xI xH)      := eq_refl. (* in binary 11 *)
Example four_int :  4%Z = Zpos (xO (xO xH)) := eq_refl. (* in binary 100 *)
Example five_int :  5%Z = Zpos (xI (xO xH)) := eq_refl. (* in binary 101 *)
Example six_int :   6%Z = Zpos (xO (xI xH)) := eq_refl. (* in binary 110 *)
Example seven_int : 7%Z = Zpos (xI (xI xH)) := eq_refl. (* in binary 111 *)

Check xO : positive -> positive. (* функция добавляет бит 0 *)
Check xI : positive -> positive. (* функция добавляет бит 1 *)
Check xH : positive.             (* константа единица *)

(* Используйте команду `Search (Pos.le).` чтобы посмотреть все свойства `<=` для типа positive *)

(** Тип positive не имеет нуля т.е. верно утверждение ниже *)
Example pos_gt_zero : ∀ n : positive, Pos.le 1 n := Pos.le_1_l.

(* ------------------------------------------------------------------------------------ *)
(* как в Coq заданы действительные числа R?                                             *)
(* ------------------------------------------------------------------------------------ *)

Example zero_real' : 0%R = IZR Z0 := eq_refl.

(** Если подключить область видимости комендой `Local Open Scope`, то можно некоторые обозначения опустить: арифметические операции, вместо 2%R писать 2 *)
Local Open Scope R_scope.

Example zero_real : 0 = IZR Z0 := eq_refl.
Example one_real : 1 = IZR (Zpos xH) := eq_refl.
(** Равенства ниже показывают что IZRпросто отображает целые числа (множество Z) в действителььные
(множество R) *)
Example two_real : 2 = IZR (Zpos (xO xH)) := eq_refl.
Example forty_two_real : 42 = IZR (Zpos (xO (xI (xO (xI (xO xH)))))) := eq_refl.
(** Отрицательные числа нужно оборачивать в скобки если не открыть `R_scope`*)
Example minus_forty_two_real : (-42)%R = IZR (Zneg (xO (xI (xO (xI (xO xH)))))) := eq_refl.
Example minus_forty_two_real' : -42 = IZR (Zneg (xO (xI (xO (xI (xO xH)))))) := eq_refl.

(** 42 = 101010 в двоичной системе счисления *)

(** Zneg добавляет минус к числу, Zpos добавляет плюс и превращает положительное число в целое *)
Check Zneg : positive -> Z.
Check Zpos : positive -> Z.

Eval compute in -5 + 5.



Check plus_minus 0 3 : 0 - 3 + 3 = 0.
Check plus_minus 0 3 : Rplus (Rminus (IZR Z0) (IZR (Zpos (xI xH)))) (IZR (Zpos (xI xH))) = (IZR Z0).

(** Эти леммы нам понадобятся позже как "кирпичики" и нам пока не нужно думать как их доказать. *)
Lemma plus_comm m n : Rplus m n = Rplus n m. Proof. Admitted.
Lemma plus_minus m n : Rplus (Rminus m n) n = m. Proof. Admitted.

(** `Admitted` используется если Вы пока ещё не понимаете как должно выглядеть доказательство леммы, но хотите её использовать (ссылаться на неё как будто она уже доказана) в доказательствах других
лемм. *)

(** как решить не используя ring? *)
Example example1_2 (a b : R) : a - (b + 1) = (a - b) - 1.
Proof.
replace (a - (b + 1)) with (a + (-(b + 1))).
(** думаем как использовать лемму mul_neg_one *)
Check mul_neg_one b 1 : - (b + 1) = - b - 1.
(** надо развернуть выражение: используем своство симметричности операции `=` *)
Check eq_sym(mul_neg_one b 1) : - b - 1 = - (b + 1).

(* 1. Переписываем часть выражения используя replace *)
replace (- (b + 1)) with (- b - 1) by exact (eq_sym(mul_neg_one b 1)).
Undo. (* Отменяем предыдущий шаг *)
(* 2. Используя rewrite *)
rewrite (mul_neg_one b 1).


Qed.





Example plus_minus_right m n : Rplus (Rminus m n) n = m := sorry.

Example example1 (a b t : R) : a - (b + t) = (a - b) - t.
Proof.

Admitted.






Example helper1 a b t: Rminus a (Rplus b t) = Rminus (Rminus a b) t.
Proof.
(* отключаем Display all basic low-level*)
(* Наша задача разбить операцию доказательства на более мелкие операции *)

Qed.


Hint Resolve mul_neg_one : arith.
Hint Rewrite mul_neg_one : arith.





(** Когда записываем доказательство в Coq важно чтобы оно уже было решено на бумаге
либо в комментариях :)

План простой используем тактику rewrite.
Можно использовать replace with для наглядности.

Задачи можно разделять: лучше сначала составить список лемм, которые будут использоваться в доказательстве и, когда доказательство будет готово, искать их в библиотеке.

a - (b + 1)) = ((a - b) - 1) use forall m, n: -(m + n) = (- m - n)
a + (-b - 1)) = ((a - b) - 1) используем ассоциативность сложения add_assoc
a + -b - 1) = ((a - b) - 1) 
*)


(* Когда не понятно, или слишком все одинаково выбираем в меню View -> Display all basic low-level contents
Теперь наша цель выглядит так
@eq R (Rminus a (Rplus b (IZR (Zpos xH)))) (Rminus (Rminus a b) (IZR (Zpos xH)))

Смотрим различия (лишние скобки удаляем):
Левая часть равенства
Rminus a (Rplus b (IZR (Zpos xH))
Правая часть равенства
Rminus (Rminus a b) (IZR (Zpos xH))

(IZR (Zpos xH)) одинаково, заменяем его на t
Получаем:
Rminus a (Rplus b t) = Rminus (Rminus a b) t

Думаем над задачаей:
minus a (plus b t) = minus (minus a b) t


*)
