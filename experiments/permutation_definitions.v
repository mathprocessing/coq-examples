Require Import List Setoid Compare_dec Morphisms FinFun PeanoNat.
(* Set Implicit Arguments. *)
Import ListNotations.

Module First_example.
Section Perms.
Variable A : Type.

(* Permutation definition based on transpositions for induction with fixed length *)
Inductive Perm : list A -> list A -> Prop :=
| perm_refl : forall l, Perm l l
| perm_swap :  forall x y l1 l2, Perm (l1 ++ y :: x :: l2) (l1 ++ x :: y :: l2)
| perm_trans l l' l'' : Perm l l' -> Perm l' l'' -> Perm l l''.

Local Hint Constructors Perm : core.

Example Perm_sym : forall l l' : list A,
  Perm l l' -> Perm l' l.
Proof.
  intros l l' Hperm.
  induction Hperm; auto.
  apply perm_trans with (l':=l').
  all: assumption.
Qed.

End Perms.

End First_example.


Module Second_example.
Section Perms.

Variable A : Type.

Inductive Perm : list A -> list A -> Prop :=
| perm_refl : forall l, Perm l l
| perm_swap :  forall x y l1 l2, Perm (l1 ++ y :: x :: l2) (l1 ++ x :: y :: l2).

Local Hint Constructors Perm : core.

Example Perm_sym : forall l l' : list A,
  Perm l l' -> Perm l' l.
Proof.
  intros l l' Hperm.
  induction Hperm; auto.
Qed. (* What's going on? Why we don't need transitivity? *)
(*If we have more axioms then we have more problems? Need to proof some additional cases. WTF??? *)

(*I think that normally in 'paper and pencil' mathematics:
If we have more axioms than ... we don't need to
change our proofs! *)

Lemma list_cases : forall (l:list A), l = nil \/ exists (z:A) (zs:list A), l = z :: zs.
Proof. intros. destruct l. left. reflexivity. right. exists a, l. reflexivity. Qed.

Example Perm_nil : forall (l : list A),
  Perm [] l -> l = [].
Proof.
  intros l hFalse.
  remember (@nil A) as m in hFalse.
  induction hFalse. 
  * exact Heqm.
  * exfalso. apply eq_sym in Heqm.
    case (list_cases l1); intro h.
    + subst l1.
      apply (app_cons_not_nil) in Heqm.   (*Search ([] <> _). *)
      contradiction.
    
    + destruct h as [z h]. destruct h as [zs h].
      subst l1.
      exact ((app_cons_not_nil (z::zs) (x :: l2) y) Heqm).
Qed.

Example Perm_nil_cons : forall (l : list A) (x : A),
  ~ Perm [] (x::l).
Proof.
  intros l x hFalse.
  assert (H : x :: l = []) 
    by exact (Perm_nil (x :: l) hFalse).
  discriminate.
Qed.
  
Example Perm_transitivity : forall xs ys zs : list A,
  Perm xs ys -> Perm ys zs -> Perm xs zs.
Proof.
  intros xs ys zs h_xy h_yz.
  induction xs, zs with .
  apply perm_refl.
  
  apply Perm_nil in h_xy.
  subst ys.
  apply Perm_nil_cons in h_yz.
  contradiction.
  
  apply Perm_sym in h_yz.
  apply Perm_nil in h_yz.
  subst ys.
  apply Perm_sym in h_xy.
  apply Perm_nil_cons in h_xy.
  contradiction.
  
  
Abort.
  

End Perms.
End Second_example.

