Example test : 1 = 1 := eq_refl.

Inductive Edge :=
| edge : nat -> nat -> Edge.

Inductive EdgeList :=
| nil : EdgeList
| cons : Edge -> EdgeList -> EdgeList.

Inductive Graph :=
edges : EdgeList -> nat -> Graph.

Check edge 2 3.
Check nil.
Check cons (edge 1 2) nil.
Check edges (cons (edge 1 2) nil) 3.

(* Set Implicit Arguments.
Implicit Type (A : Type). *)

Section GraphProps.
Variable A : Type.
Parameter isEmpty : Graph -> Prop.

Axiom isEmpty_def : forall (g : Graph) (es : EdgeList), g = edges es 0 -> isEmpty g.

Axiom my_eq_ind : forall {A : Type} (x : A) (P : A -> Prop),
    P x -> forall y : A, x = y -> P y.

Lemma trick : isEmpty (edges nil) -> False.
  intros.
  apply isEmpty_def in H.

  (*
  eq_ind
	 : forall (A : Type) (x : A) (P : A -> Prop),
       P x -> forall y : A, x = y -> P y

  x = (edges nil)
  y = empty
  P = (empty := False, edges _ := True)
  |- P x <=> |- True <= I : True
  *)
  pose (func := (fun e : @Graph A => 
  match e with
    | empty => False
    | edges _ => True
  end)).
  (* func разбивает множество конструкторов на два непересекающихся множества *)

  refine (my_eq_ind (edges nil) func _ empty _).
  unfold func. (* func (edges nil) <=> True [by unfold]*)
  exact I. (* I <=> true_intro in Lean *)
  exact H.


  eapply (eq_ind (edges nil) 
  (fun e : Graph => match e with
  | empty => False
  | edges _ => True
  end) I empty).

  Check @eq_ind.
  exact (
    eq_ind (edges nil) _ _ _ _
  
  ).
  (* discriminate.
  Show Proof. *)
  let H1 : False :=
  eq_ind (edges nil)
  (fun e : Graph => match e with
                      | empty => False
                      | edges _ => True
                      end) I empty H in
  False_ind False H1)
Qed.



End GraphProps.

