Parameter not : Prop -> Prop.
Parameter xor : Prop -> Prop -> Prop.
Parameter or : Prop -> Prop -> Prop.
Parameter and : Prop -> Prop -> Prop.
Axiom sorry : forall {A : Type}, A.

Section definitions.
Variables (x y z : Prop).

Axiom or_true : or True x = True.
Axiom or_false : or False x = x.

Axiom and_true : and True x = x.
Axiom and_false : and False x = False.

Axiom not_true : not True = False.
Axiom not_false : not False = True.

Axiom xor_def : xor x y = or (and (not x) y) (and x (not y)).

Axiom prop_case : x \/ ~x.

End definitions.

Axiom true_eq : forall A: Prop, A -> (A = True).
Axiom false_eq : forall A: Prop, ~A -> (A = False).
Axiom true_eq' : forall A: Prop, A = (A = True).
Axiom false_eq' : forall A: Prop, ~A = (A = False).

Lemma or_refl {x : Prop} : or x x = x.
Proof.
  case (prop_case x); intro h.
  apply true_eq in h.
  subst x.
  apply or_true.
  apply false_eq in h.
  subst x.
  apply or_false.
Qed.

Ltac logic_solve := 
  match goal with
  | h : ~?x |- _ = _ =>
        (apply false_eq in h || apply true_eq in h); subst x; idtac "not case"
  | h : ?x |- _ = _ =>
        (apply false_eq in h || apply true_eq in h); subst x; idtac "base case"
  | _ => fail 
  end.

Lemma or_refl' (x : Prop) : or x x = x.
Proof.
  case (prop_case x); intro h; logic_solve; apply or_true + apply or_false.
Qed.

Example true_eq_rw : forall A: Prop, A <-> (A = True).
Proof. constructor. exact (true_eq A). intros; subst A; trivial. Qed.
Example false_eq_rw : forall A: Prop, ~A <-> (A = False).
Proof. constructor. exact (false_eq A). intros; subst A; exact (fun H => H). Qed.

Example refl (f : Prop -> Prop -> Prop) (x : Prop) := f x x = x.

Example test_refl (x : Prop) : refl or x := or_refl.

Example and_refl (x : Prop) : refl and x.
Proof.
  unfold refl.
  case (prop_case x); intro h.
  all : logic_solve.
  all : apply and_true + apply and_false.
  (* rewrite (true_eq' (and x x)). *)
Qed.

Example bconj (a b : Prop) : a -> b -> and a b := sorry.

(* 
How to testing that [proof] is exactly what we want?
Answer: ...
How to testing that [goal] is exactly what we want?
use construction: assert (h : wanted_goal) by exact sorry; exact h. *)

Ltac simp :=
  intros; match goal with
  | |- True => exact I
  | |- ~False => exact (fun H => H) 
  | |- ?a = ?a => exact eq_refl
  | |- ?a <-> ?a => apply iff_refl
  | |- ?a = True => apply true_eq
  | |- ?a = False => apply false_eq; intro
  | |- True = ?a => apply eq_sym; simp
  | |- False = ?a => apply eq_sym; simp
  | |- _ <-> _ => constructor; simp
  | |- _ /\ _ => apply conj; simp
  | |- and _ _ => apply bconj; simp
  | H : ?x |- ?x => assumption
  | H : ?a -> ?b |- ?b => apply H; clear H; simp (* what if we have two hypothesis? *)
  | H : _ <-> _ |- _ => destruct H; simp
  | H1 : ?a -> ?b, H2 : ?b |- _ => clear H1; simp (* not useful hypothesis *)
  | H : True |- _ => clear H; simp
  | H : False |- _ => exfalso; exact H
  | H1 : ?a, H2 : ?a -> _ |- _ => 
    let h := fresh "h" in assert (h : _) by exact (H2 H1); clear H2; simp
  | _ => idtac
  end.

Example test_simp (x : Prop): x -> x.
Proof. Info 0 simp. Qed.

Example test_simp2 (A B : Prop): (A -> B) -> (A -> True -> True -> True -> B).
Proof. Info 0 simp. Qed.

Example test_simp3 (A B : Prop): True -> True -> True -> False -> A.
Proof. Info 0 simp. Qed.

Example test_simp4 (A B : Prop): A -> B -> B /\ A.
Proof. Info 0 simp. Qed.

Example test_simp5 (A B : Prop): (A <-> B) -> (B -> A).
Proof. Info 0 simp. Qed.

Example test_simp6 (A B : Prop): (A -> B) -> (B -> A) -> (B <-> A).
Proof. Info 0 simp. Qed.

Example test_simp7 (A B : Prop): A <-> A. (*:= iff_refl A *)
Proof. Info 0 simp. Qed.

Example test_simp8 (A B C : Prop) : A -> True -> (A -> B) ->  (A -> B -> C) -> C.
Proof. Info 0 simp. Qed.

Example test_infinite_loop_admitted
  (A B C : Prop) : (C -> (B -> A)) -> (A -> B) ->  ((B -> A) -> C) -> C.
Proof. (* intros. (* |- C *) apply H1. (* |- B -> A *) apply H. (* |- C *) *)
  Info 0 simp.
  apply H.
  all: swap 1 2.
  apply H1.
  assert (hc : C) by exact sorry; exact hc. (* test that goal is C *)
Qed.

Example example_optimal (A B C : Prop) : (C -> (A -> B)) -> (A -> B) ->  ((B -> A) -> C) -> C.
Proof.   
(* example of optimal actions *)
intros. clear H. (* H : C -> A -> B already in H0 : A -> B *)
apply H1. intros. clear H0. (* H0 : A -> B already in H : B *)
Abort.

Print Ltac simp.
Print Ltac Signatures.
 (* time_constr tac might be useful for working with infinite loops *)

