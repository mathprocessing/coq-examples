Require Import Unicode.Utf8.
Require Import PeanoNat.
Require Import Ascii.
Require Import Bool BinPos BinNat.

Check Space.
Check ascii_of_pos 4.
Check " "%char.

Check "h"%char.

Goal ascii_of_pos 49 = "1"%char. Proof eq_refl.
Goal ascii_of_pos 50 = "2"%char. Proof eq_refl.