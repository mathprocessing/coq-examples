Require Import Unicode.Utf8.
(* https://coq.inria.fr/distrib/current/refman/practical-tools/coqide.html#managing-files-and-buffers-basic-editing *)

(**
see default bindings in github coq/ide/coqide/default_bindings_src.ml
https://github.com/coq/coq/blob/42e9c71b8f0b27c7720c22cd57517af77a5f8e90/ide/coqide/default_bindings_src.ml

Type \not and press Shift+Space and you see ¬
*)

Example test_forall : ∀ x : nat, x = 0 -> 0 = x.
  intro.
Abort.

Example test_exists : ∃ x : nat, x = 100.
  (* exists 100. *)
  apply (ex_intro _ 100).
  exact eq_refl.
  (* 
  ex_intro (∃ x : nat, x = 100) 100 eq_refl
  *)
Abort.


Fail Example test_numbers: 
  ∀ x₁x₂: nat, x₁= x₂→ x₂= x₁.
(* The reference x₂ was not found
in the current environment. *)

Example test_or_and : 
  ∀ A B : Prop, A ∧ B → A ∨ B.
Proof.
  intros A B [ha hb].
  left. exact ha.
Qed.

Example duplicate_goal_lemma: ∀ A : Prop, A ∧ A → A.
Proof. intros A h. exact (proj1 h). Qed.
Ltac dup := apply duplicate_goal_lemma; split.

Example test_not : ∀ A C : Prop, A → ¬ A → C.
Proof.
  dup.
  + intros A C ha hnota.
    exfalso.
    apply hnota.
    exact ha.
  + exact absurd.
Qed.

(**
shortlist of bindings:
\for = ∀
\ex = ∃
\-> = →
\not = ¬
\and = ∧
\or = ∨
\sup = ^
\dot = ˙
\top = ⊤
\bot = ⊥
\le = ≤
\ge = ≥
\ne = ≠
\equiv = ≡
\harr = ↔
\esim = ≂
\bsim = ∽
\emptyv = ∅
\subset = ⊂
\supset = ⊃
\cap = ∩
\cup = ∪
\in = ∈
\notin = ∉

subscript:
\_9 = ₉
\_beta = ᵦ

greek letters:
\alpha = α
\beta = β
\gamma = γ
\delta = δ
\epsilon = ∊
\theta = θ
\vartheta = ϑ
\lambda = λ
\pi = π
\rho = ρ
\sigma = σ
\omega = ω
\Phi = Φ
\Psi = Ψ
\Omega = Ω

 *)
 
 
 
 
 
 
 
 
 
 
