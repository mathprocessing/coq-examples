(* https://github.com/tchajed/strong-induction/blob/master/StrongInduction.v *)

(** Here we prove the principle of strong induction, induction on the natural
numbers where the inductive hypothesis includes all smaller natural numbers. *)

Require Import PeanoNat.

Section StrongInduction.

  Variable P:nat -> Prop.

  (** The stronger inductive hypothesis given in strong induction. The standard
  [nat ] induction principle provides only n = pred m, with [P 0] required
  separately. *)
  Hypothesis IH : forall m, (forall n, n < m -> P n) -> P m.

  Lemma P0 : P 0.
  Proof.
    apply IH; intros.
    exfalso; inversion H.
  Qed.

  Hint Resolve P0 : core.

  Lemma pred_increasing : forall n m,
      n <= m ->
      Nat.pred n <= Nat.pred m.
  Proof.
    induction n; cbn; intros.
    apply le_0_n.
    induction H; subst; cbn; eauto.
    destruct m; eauto.
  Qed.

  Hint Resolve le_S_n : core.

  (** * Strengthen the induction hypothesis. *)

  Local Lemma strong_induction_all : forall n,
      (forall m, m <= n -> P m).
  Proof.
    induction n; intros;
      match goal with
      | [ H: _ <= _ |- _ ] =>
        inversion H
      end; eauto.
  Qed.


  Local Lemma strong_induction_all' : forall n,
      (forall m, m <= n -> P m).
  Proof.
    induction n; intros.
    inversion H.
    apply (IH 0).
    intros n n_less_zero.
    apply Nat.nlt_0_r in n_less_zero; contradiction.
    
    inversion H. eauto.
    inversion H1. eauto.
    inversion H2. subst m m0. clear H2.
(*       match goal with
      | [ H: _ <= _ |- _ ] =>
        inversion H
      end; eauto. *)
  Abort.
  
  Check fun n : nat => strong_induction_all n n.
  (** forall n : nat, n <= n -> P n *)
  
  Example strong_induction_short_proof : forall n, P n :=
    fun n : nat => strong_induction_all n n (le_n n).

  Theorem strong_induction : forall n, P n.
  Proof.
    intro n.
    eapply strong_induction_all.
    exact (le_n n).
    Show Proof.
(*  fun n : nat =>
    IH n
   (fun (n0 : nat) (_ : n0 < n) =>
    IH n0
      (fun (n1 : nat) (_ : n1 < n0) =>
       IH n1 (fun (n2 : nat) (_ : n2 < n1) => strong_induction_all n2 n2 (le_n n2))))
 *)
  Qed.
  Theorem strong_induction' : forall n, P n.
    intros.
    apply (IH n).
    intros n0 h.
    apply (IH n0).
    intros n1 h2.
  Abort.

End StrongInduction.