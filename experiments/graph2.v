Require Import Unicode.Utf8.
Require Import List.
Import ListNotations.
Require Import PeanoNat.
Axiom sorry : forall {A : Type}, A.

Section GraphProps.
Variable (V : Type).
Implicit Type (V : Type).

(* Set Implicit Arguments.
Implicit Type (A : Type). *)

Parameter A B C D E F : V.

Inductive Edge :=
| edge : nat → nat → Edge.

Definition EdgeList := list Edge.
Definition VertexList := list V.





Parameter _in : Edge → EdgeList → Prop.
Axiom _in_nil (e : Edge) : _in e nil → False.
Axiom _in_cons : ∀ e xs, _in e xs ↔ _in.


Record Graph := mkGraph {
  vs : VertexList;
  es : EdgeList;
  good_edges : ∀ (a b : nat), _in (edge a b) es -> a < length vs /\ b < length vs;
}.

Example test1 : length [edge 0 1; edge 1 2] = 2 := eq_refl.
Check edge 0 1.
Check [edge 0 1; edge 1 2] : EdgeList.
Check [A; B; C] : VertexList.
Check mkGraph [A; B] [edge 0 1].
Check mkGraph [A; B] [edge 0 1].

End GraphProps.

