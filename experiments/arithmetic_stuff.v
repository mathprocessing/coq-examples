Require Import Unicode.Utf8.
Require Import Reals.
Require Import Lia.
Require Import Ring.
Require Import Arith.
Axiom sorry : forall {A : Type}, A.

Local Open Scope R_scope.

Example example1 (a b t : R) : (a - (b + t)) = ((a - b) - t).
Proof. ring.

Show Proof.
 Qed.

Lemma mul_neg_one m n : -(m + n) = (- m - n).
Proof.
(* пример как использовать assert
assert (add_comm : forall a b, a + b = b + a) by exact sorry. *)



Qed.

Example helper1 a b t: Rminus a (Rplus b t) = Rminus (Rminus a b) t.
Proof.
(* отключаем Display all basic low-level*)
(* Наша задача разбить операцию доказательства на более мелкие операции *)

Qed.


Hint Resolve mul_neg_one : arith.
Hint Rewrite mul_neg_one : arith.


(** как решить не используя ring? *)
Example example1_2 (a b : R) : (a - (b + 1)) = ((a - b) - 1).
Proof.
(* Когда не понятно, или слишком все одинаково выбираем в меню View -> Display all basic low-level contents
Теперь наша цель выглядит так
@eq R (Rminus a (Rplus b (IZR (Zpos xH)))) (Rminus (Rminus a b) (IZR (Zpos xH)))

Смотрим различия (лишние скобки удаляем):
Левая часть равенства
Rminus a (Rplus b (IZR (Zpos xH))
Правая часть равенства
Rminus (Rminus a b) (IZR (Zpos xH))

(IZR (Zpos xH)) одинаково, заменяем его на t
Получаем:
Rminus a (Rplus b t) = Rminus (Rminus a b) t
*)


replace (a - (b + 1)) with (a + (-(b + 1))).

(** думаем как использовать лемму mul_neg_one *)
Check mul_neg_one b 1 : - (b + 1) = - b - 1.
(** надо развернуть выражение: используем своство симметричности операции `=` *)
Check eq_sym(mul_neg_one b 1) : - b - 1 = - (b + 1).

(* 1. Переписываем часть выражения используя replace *)
replace (- (b + 1)) with (- b - 1) by exact (eq_sym(mul_neg_one b 1)).
Undo. (* Отменяем предыдущий шаг *)
(* 2. Используя rewrite *)
rewrite (mul_neg_one b 1).




autorewrite with arith.
auto with arith.



Qed.


(** Когда записываем доказательство в Coq важно чтобы оно уже было решено на бумаге
либо в комментариях :)

План простой используем тактику rewrite.
Можно использовать replace with для наглядности.

Задачи можно разделять: лучше сначала составить список лемм, которые будут использоваться в доказательстве и, когда доказательство будет готово, искать их в библиотеке.

a - (b + 1)) = ((a - b) - 1) use forall m, n: -(m + n) = (- m - n)
a + (-b - 1)) = ((a - b) - 1) используем ассоциативность сложения add_assoc
a + -b - 1) = ((a - b) - 1) 
*)
