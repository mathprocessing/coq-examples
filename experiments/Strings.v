Require Import Unicode.Utf8.
Require Import Ascii.
Require Import String.
Require Import Arith.
From MyExamples Require Import sorry.

Local Open Scope string_scope.

(*
(** Implementation of string as list of ascii characters *)

Inductive string : Set :=
  | EmptyString : string
  | String : ascii -> string -> string.
*)

Example Hello := "Hello".
Example World := "World".
Eval compute in Hello ++ String Space "" ++ World ++ "!".

Example concat1 : ∀ x y : string,
  x = "1" → y = "2" → x ++ y = "12".
Proof.
  intros x y hx hy.
  rewrite hx, hy.
  reflexivity.
Defined.

Check index_correct4 : ∀ (n : nat) (s : string),
  index n "" s = None → length s < n.

Check index : nat → string → string → option nat.

Goal index 0 "" "ab" = Some 0. Proof eq_refl.
Goal index 1 "" "ab" = Some 1. Proof eq_refl.
Goal index 2 "" "ab" = Some 2. Proof eq_refl.
Goal index 3 "" "ab" = None. Proof eq_refl.

Check le_0_n.
Example lt_0_Sn : ∀ n, 0 < S n. Proof. Admitted.


Example sub_0_r n : n - 0 = n :=
match n with
 | 0 => eq_refl
 | S n => eq_refl
end.




Example index_correct4_2 : ∀ (n : nat) (s : string),
  index n "" s = None → length s < n.
Proof.
  intros n s; generalize n; clear n;
  elim s; simpl; auto.
  intros n; case n; simpl; auto with arith.
  intros; discriminate.
  intros; apply lt_0_Sn.
Qed.


Example concat_index : ∀ x y : string,
  x ++ x = "12".
Proof.
  intros x y hx hy.
  rewrite hx, hy.
  reflexivity.
Defined.


