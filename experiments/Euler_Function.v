(**
Euler function

irreducible fractions:
If we like multiplicativity we select second variant
f  1 =  {0, 1}: {{}, 1/1}
Это может к идее особых точек функций и типологии дискретный могообразий
f  2 =  1: 1/2
f  3 =  2: 1/3 2/3
f  4 =  2: 1/4 3/4
f  5 =  4: 1/5 .. 4/5
f  6 =  2: 1/6 5/6
f  7 =  6: 1/7 .. 6/7
f  8 =  4: 1/8 3/8 5/8 7/8
f  9 =  6: 1/9 2/9 4/9 5/9 7/9 8/9
f 10 =  4: 1/10 3/10 7/10 9/10
f 11 = 10: 1/11 .. 10/11
f 12 =  4: 1/12 5/12 7/12 11/12
f 13 = 12: 1/13 .. 12/13
f 14 =  6: 1/14 3/14 5/14 9/14 11/14 13/14
f 15 =  8: 1/15 2/15 4/15 7/15 8/15 11/15 13/15 14/15
f 16 =  8: 1/16 3/16 5/16 7/16 9/16 11/16 13/16 15/16

euler_of_prime: ∀ p : prime, f p = p - 1 := euler_of_pow p 1 (partial case of euler_of_pow)
euler_of_pow: ∀ (p : prime) (n > 0) : f (p ^ n) = (p - 1) * p ^ (n - 1)
multiplicative: ∀ m n, gcd m n = 1 -> f (m * n) = f m * f n
*)