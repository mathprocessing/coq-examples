Require Import List Setoid Compare_dec Morphisms FinFun PeanoNat.
Import ListNotations. (* Set Implicit Arguments. *)

Variable T : Type.
Parameter Permutation : list T -> list T -> Prop.
Axiom perm_nil : Permutation [] [].
Axiom perm_skip : forall (x : T) (l l' : list T), 
  Permutation l l' -> Permutation (x::l) (x::l').
Axiom perm_swap : forall (x y : T) (l : list T),
  Permutation (y :: x :: l) (x :: y :: l).
Axiom perm_trans : forall l l' l'' : list T,
  Permutation l l' -> Permutation l' l'' -> Permutation l l''.
  
Example single (x : T): Permutation [x] [x] := perm_skip x [] [] perm_nil.

Example two (x y : T): Permutation [x; y] [y; x] := perm_swap y x [].

Axiom list_cases : forall {A : Type} (l : list A),
  l = nil \/ (exists (x : A) (l' : list A), l = x :: l').

Example perm_refl_fail (l : list T): Permutation l l.
Proof.
  case (list_cases l); intro h.
  subst l. apply perm_nil.
  destruct h as [x h].
  destruct h as [xs h].
  subst l.
  apply perm_skip.
  Show Proof.
Abort.

Example perm_refl (l : list T): Permutation l l.
Proof.
  elim l.
  + apply perm_nil.
  + intros x l' h.
    exact (perm_skip x l' l' h).
Qed.

Example perm_nil_single_false (x : T) : Permutation [] [x].
Proof.
  eapply perm_trans.
  eapply perm_swap.
  intro h.
  (** 
  let proof of h begin with perm_nil => contradiction
  with perm_skip => false by t1@(one of elements is [])
  with perm_swap => false by t2@(need two elements x and y but t1)
  with perm_trans => false by (perm_trans perm_nil <=> id /\ perm_trans _,
    filling placeholder:
      with perm_skip by t1
      with perm_swap by t2
      with perm_trans by (perm_trans perm_trans <=> perm_trans)
  )
  
  
   *)


Parameter reverse : list T -> list T.
Axiom reverse_nil : reverse [] = [].
Axiom reverse_step : forall (x:T)(xs:list T), reverse (x :: xs) = reverse xs ++ [x].

Fixpoint init (l : list T) : list T :=
match l with
| [] => []
| [x] => []
| x::xs => x :: init xs
end.

Axiom reverse_step2 : forall (x:T)(xs:list T), reverse (x :: xs) = last (x :: xs) :: reverse (init (x :: xs)).

(**
reverse [1,2,3] = last [1,2,3] :: reverse (init [1,2,3])
reverse [1,2,3] = 3 :: reverse [1, 2]
reverse [1,2,3] = 3 :: [2, 1] = [3, 2, 1] *)

Inductive Permutation : list T -> list T -> Prop :=
| perm_nil: Permutation [] []
| perm_skip x l l' : Permutation l l' -> Permutation (x::l) (x::l')
| perm_swap x y l : Permutation (y::x::l) (x::y::l)
| perm_trans l l' l'' :
    Permutation l l' -> Permutation l' l'' -> Permutation l l''.


Check perm_trans.

Inductive Vertex :=
| A : Vertex
| B : Vertex
| C : Vertex.

Parameter Graph : Type.
Parameter empty : Graph.
Parameter addVertex : Vertex -> Graph -> Graph.
Parameter In : Vertex -> Graph -> Prop.

Parameter addEdge : fun (v1 v2 : Vertex) (g : Graph) => 
  In v1 g /\ In v2 g -> True.

Check addEdge A B.


Check addVertex B (addVertex A empty).


Definition InGraph : Vertex -> Graph -> Prop
| v g := 
| 

Inductive Graph :=
| empty : Graph
| addVertex : Vertex -> Graph -> Graph
| addEdge v1 v2 g : InGraph v1 g /\ InGraph v2 g -> Graph.

(**
addVertex A : Graph -- Graph with one vertex A
(addVertex A)

 *)
