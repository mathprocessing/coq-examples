Require Import Unicode.Utf8.
From Coq Require Import ssreflect ssrfun ssrbool.
Set Implicit Arguments.
Require Import Reals.
Require Import Ensembles.
Require Import Lia.
Require Import Lra.
Axiom sorry : forall {A : Type}, A.

(* https://www.youtube.com/watch?v=UjcGyISekpE&list=PLuh62Q4Sv7BXkeKW4J_2WQBlYhKs_k-pj&index=4 *)

(* 
Probability Theory stands on:
1. Set Theory (empty, union, whole)
2. Rational numbers.
  because P = m/n, where m,n : nat
  m, n ≥ 0, n ≠ 0
*)

(** 
Definitions:
Sample space is a set that contains all other sets. (might be wrong)
Event is a subset of the sample space.

Experiment 1:
  Flip a coin twice, record results.
Sample space: {HH, HT, TH, TT}, 
we have 4 possible outcomes for this experiment

Question for experment 1: (Event)
  What's the probabilty that at least one of these tosses included a head?
  Answer: this Event has 3 outcomes, but all possible outcomes is 4 =>
    P = 3 / 4

List of all possible events of this experiment:
  [∅, {HH}, {HT}, {TH}, {TT},
  {HH, HT}, {HH, TH}, {HH, TT}, {HT, TH},
  {HT, TT}, {TH, TT},  
  {HH, HT, TH}, {HH, HT, TT}, {HH, TH, TT}, {HT, TH, TT},
  {HH, HT, TH, TT}], length = 16.

P has tobe at least as much as {HH} or {HT}
P({HH, HT}) ≥ P({HH})
P({HH, HT}) ≥ P({HT})

forall A,B:Set, A ⊆ B → P(A) ≤ P(B)

P(∅) = 0
P(A | A in S) = x, x in [0 .. 1]
P(S) = 1

Lemmas from set theory:
s1. ¬A ∪ A = S
s2. ¬A ∩ A = ∅


Axioms of probability:
   
a1. P(A) >= 0.
a2. P(S) = 1, where S = ¬ ∅.
a3. if A ∩ B = ∅ then
     P(A ∪ B) = P(A) + P(B).

Definition of disjoint events:
∀ A,B : Event, 
  A ∩ B = ∅ <=> disjoint(A, B)
  
stronger a3:
  if A1, A2, ..., An are disjoint
  then P(fold union [A1, A2, .., An]) = (maybe wrong but you get the idea)
    P(A1) + P(A2) + .. + P(An)
    
Theorems:
t1. ∀ A : Set, P(¬A) = 1 - P(A) <= 
    ¬A ∩ A = ∅ from s2,
    P(¬A ∪ A) = P(¬A) + P(A) from a3,
    ¬A ∪ A = S from s1,
    P(S) = 1 exact a2
    
t2. P(∅) = 0 <=
    

t3. P(A ∪ B) = P(A) + P(B) - P(A ∩ B) <=
IDEA 1:
  if A ∩ B = ∅ then
    P(A ∩ B) = 0 from t2,
    then our goal is:
    |- P(A ∪ B) = P(A) + P(B) from a3
    Done.
  elif A ∩ B != ∅ then
    A != ∅
    B != ∅
    if P(A ∩ B) = P(A) * P(B) can be applied (not wrong)
      |- P(A ∪ B) = P(A) + P(B) - P(A) * P(B) from ∀ A B, P(A ∩ B) = P(A) * P(B)
      |- P(A ∪ B) = P(A) - P(A) * P(B) + P(B) by ring
      |- P(A ∪ B) = (1 - P(B)) * P(A) + P(B)  by ring
      |- P(A ∪ B) = P(¬ B) * P(A) + P(B)      from t1
      ...
      Note: IDEA 1 demonstrate some breath first search attempt to find a proof.
      Now we have two variants in general:
        1. Delete all proofs that contains errors.
        2. Save "wrong proofs" like above and add it as test case to something.
      We assume that humans doing something near optimal from this assumption
      we can't select first variant.
      
      I.e. we need to save this "error proof" to test some prover in future,
      that prover must construct this "error proof". And we can filter out better models of provers.
IDEA 2:
\cap = ∩
\cup = ∪
\not = ¬

  A ∪ B = (A ∩ B) ∪ (A ∩ ¬ B) ∪ (¬ A ∩ B) by bruteforce_all_set_combinations
  A ∪ B = A ∪ (¬ A ∩ B)
  P(A ∪ B) = P(A ∪ (¬ A ∩ B)) by congruence or f_equal
  
  main fact that enables using a3 is: A ∩ (¬ A ∩ B) = ∅
  P(A ∪ (¬ A ∩ B)) = P(A) + P(¬ A ∩ B) from a3
  now goal is:
  |- P(¬ A ∩ B) = P(B) - P(A ∩ B)
  We can prove this as lemma.
  
  Lemma: ∀ A B : Set, P(B ∩ ¬ A) = P(B) - P(A ∩ B)
  Proof.
  |- P(B) = P(B ∩ ¬ A) + P(B ∩ A)
  (B ∩ ¬ A) ∩ (B ∩ A) = ∅ by set_theory
  we can apply a3 in reverse:
  P((B ∩ ¬ A) ∪ (B ∩ A)) = P(B ∩ ¬ A) + P(B ∩ A) from <- a3
  
  |- P(B) = P((B ∩ ¬ A) ∪ (B ∩ A)) by f_equal
  |- B = (B ∩ ¬ A) ∪ (B ∩ A)
  Done by set_theory.

--self analysis ON--
all_disjoint = 
branch1: (A1 ∩ A2 ∩ .. ∩ An = ∅)
branch2: forall i j, (Ai ∩ Aj = ∅)

Now I don't know which branch is true,
but suppose some solver can achienve this state,
what it can do next?
Answer: make two branches and wait for new knowledge
and maybe do some other X stuff.


General rule of development (this name is good?):
  If you get stuck don't open Google,
  just keep calm and write whole state
  (something that possible) of your mind on paper,
  then in future you can use it as test case for 
  some dev projects, libs, theories, etc.
  
  Or you can just view of what an error happens,
  this helps to analyze your behaviour.
  
  To solve task X we need to synthesize some interface of
  objects and then use it.
  What if we in some state can create only part of
  that interface?
  Answer: we get stuck.
--self analysis OFF--

t4. If A1, A2, .. An are disjoint,

 then P(A1 ∪ A2 ∪ .. ∪ An) = sum(i=1, i=N, P(Ai))

TODO: add more theorems
*)

Section section1.
Variable (T : Type).
Parameter P : Ensemble T → R.

Check Empty_set T.

Local Open Scope R_scope.

Definition set {T : Type} := Ensemble T.
Definition union {T : Type} := Union T.
Definition intersection {T : Type} := Intersection T.
Definition empty_set {T : Type} := Empty_set T.
Definition full_set {T : Type} := Full_set T.
Definition complement {T : Type} := Complement T.

Infix "∪" := union (at level 25). (* level of union must be > level of intersection *)
Infix "∩" := intersection (at level 20).
Notation "¬ x" := (complement x).
Notation "∅" := (empty_set) (at level 10).
Notation "¬∅" := (full_set) (at level 14).

(* Lemmas from set theory:
s1. ¬A ∪ A = S
s2. ¬A ∩ A = ∅

Axioms of probability:
a1. P(A) >= 0.
a2. P(S) = 1, where S = ¬ ∅.
a3. if A ∩ B = ∅ then
     P(A ∪ B) = P(A) + P(B).  *)


(** ------------------ Axioms BEGIN --------------------- *)
(* Replaced `Example` with `Let` because we can see axioms when we write/check proofs
if we use `Let`. *)
Let a1 : ∀ (A : @set T), 0 <= P A := sorry.
Let a2 : P (¬∅) = 1 := sorry.
Let a3 : ∀ (A B : @set T), 
  A ∩ B = ∅ → P (A ∪ B) = P A + P B := sorry.

Example s_union_not : ∀ (A : @set T), A ∪ (¬ A) = ¬∅ := sorry.
Example s_inter_not : ∀ (A : @set T), A ∩ (¬ A) = ∅ := sorry.
Example s_inter_empty : ∀ (A : @set T), A ∩ ∅ = ∅ := sorry.
Example s_union_empty : ∀ (A : @set T), A ∪ ∅ = A := sorry.
Example s_inter_full : ∀ (A : @set T), A ∩ ¬∅ = A := sorry.
Example s_union_full : ∀ (A : @set T), A ∪ ¬∅ = ¬∅ := sorry.
Example s_inter_self : ∀ (A : @set T), A ∩ A = A := sorry.
Example s_union_self : ∀ (A : @set T), A ∪ A = A := sorry.

Example s_inter_comm : ∀ (A B : @set T), A ∩ B = B ∩ A := sorry.
Example s_union_comm : ∀ (A B : @set T), A ∪ B = B ∪ A := sorry.

Example s_inter_assoc : ∀ (A B C : @set T), A ∩ B ∩ C = A ∩ (B ∩ C) := sorry.
Example s_union_assoc : ∀ (A B C : @set T), A ∪ B ∪ C = A ∪ (B ∪ C) := sorry.

(* Theorem mul_add_distr_r : forall n m p, (n + m) * p == n * p + m * p. *)
Example s_inter_union_distr_l : ∀ (A B C : @set T), A ∩ (B ∪ C) = A ∩ B ∪ A ∩ C := sorry.
Example s_union_inter_distr_l : ∀ (A B C : @set T), A ∪ (B ∩ C) = (A ∪ B) ∩ (A ∪ C) := sorry.

(* Check that brackets are correct *)
Check s_union_empty : ∀ A : set, union A empty_set = A.
Check s_union_comm : ∀ (A B : set), (A ∪ B) = (B ∪ A).
Check ∀ (A B : @set T), (A ∪ B) = ((¬A) ∩ B).
(* Still have brackets  A ∪ B = (¬ A) ∩ B, TODO: fix this*)

Check s_union_assoc : ∀ A B C : set, (A ∪ B) ∪ C = A ∪ (B ∪ C).
Check s_inter_assoc : ∀ A B C : set, (A ∩ B) ∩ C = A ∩ (B ∩ C).
Check s_inter_union_distr_l : ∀ A B C : set, A ∩ (B ∪ C) = A ∩ B ∪ A ∩ C.
Check s_union_inter_distr_l : ∀ A B C : set, A ∪ B ∩ C = (A ∪ B) ∩ (A ∪ C).

(* the joke with notation *)
Example joke: (¬ @empty_set T) = full_set := sorry.

(** ------------------ Axioms END --------------------- *)

Theorem t1 (A : @set T) : P (¬A) = 1 - P A.
Proof.
  have h1 : A ∩ (¬ A) = ∅ := s_inter_not A.
  have h2 : A ∩ (¬ A) = ∅ → P (A ∪ (¬ A)) = P A + P (¬ A) := @a3 A (¬ A).
  have h3 : A ∪ (¬ A) = ¬∅ := s_union_not A.
  apply h2 in h1.
  rewrite h3 in h1.
  rewrite a2 in h1.
  rewrite h1.
  ring.
Qed.

Theorem t2 (A : @set T) : P A <= 1.
Proof.
  have h1 : P A = 1 - P (¬ A) := sorry.
  rewrite h1.
  have h2 := a1 (¬A).
  lra. (* Linear real arithmetic *)
  (** See https://coq.github.io/doc/V8.11.0/refman/addendum/micromega.html *)
Qed.

(*
1 = 1 - P (∅) → 1 + P (∅) = 1
1 + P (∅) = 1 → P (∅) = 0
*)
Lemma move_to_left (n m k : R) : n = m - k ↔ n + k = m.
Proof. apply conj; move => h. + rewrite h; ring. + rewrite -h; ring. Qed.

(* h : m + n = n
      m = n + - n    *)
Lemma move_to_right (x y z : R) : x + y = z → x = z + - y.
Proof. move => h. rewrite -h. ring. Qed.

Lemma cancel_left (n m : R) : n + m = n ↔ m = 0.
Proof.
  constructor; move => h.
  rewrite <- (Rplus_opp_r n).
  rewrite <- Rplus_comm in h.
  exact: move_to_right m n _ h.
  rewrite h.
  exact: Rplus_0_r n.
Qed.

(* 
Theorem t3 : P ∅ = 0.
Give 
Syntax error: '.' expected after [gallina] (in [vernac_aux]).
*)
Theorem t3 : P (∅) = 0.
Proof.
  have H := t1 (∅).
  rewrite joke in H.
  rewrite a2 in H.
  lra.
  Undo. (* If you don't want to use lra: *)
  apply move_to_left in H.
  apply cancel_left in H.
  exact H.
Qed.

(* Test for s_inter_union_distr_l to make sure that it written correctly,
we can move that test, or delete it *)
Goal ∀ A B : @set T, (A ∩ B) ∪ (A ∩ ¬ B) = A ∩ (B ∪ ¬ B).
Proof.
  move=>A B.
  by rewrite -s_inter_union_distr_l.
Qed.

Lemma all_combinations {A B : @set T}: A ∪ B = (A ∩ B) ∪ (A ∩ ¬ B) ∪ ((¬ A) ∩ B).
Proof sorry.

(* Test #2 *)
Lemma L1 (A B : @set T) : P(A) = P(A ∩ ¬ B) + P(A ∩ B). Abort.

(* Maybe we need something like set to bool coersion to prove this set_thoery_n lemmas as 
boolean formulas *)
Lemma set_theory_1 (A B : @set T) : A ∪ B = A ∪ ((¬ A) ∩ B).
Proof.
  rewrite all_combinations.
  f_equal.
  rewrite -s_inter_union_distr_l.
  rewrite s_union_not.
  apply s_inter_full.
Qed.

(* For each lemma connection to outer code is only `set of lemmas`, we can make it explicit
by just typing it for each lemma, and in future this work can be automated *)
(*
Additional:
Also if proof changed ==> changed interface of lemma! I.e. we have really set of interfaces for
each formula of lemma i.e. set of sets of lemmas (don't scare by it, just say set of interfaces
, or set of outer views).
*)
Lemma set_theory_2 (A B : @set T) : A ∩ ((¬ A) ∩ B) = ∅.
Proof.
  (* Interface: {s_inter_not, s_inter_comm, s_inter_assoc, s_inter_empty}*)
  rewrite -s_inter_assoc.
  rewrite s_inter_not.
  rewrite s_inter_comm.
  apply s_inter_empty.
Qed.

Lemma set_theory_3 (A B : @set T) : ((¬ A) ∩ B) ∩ (A ∩ B) = ∅.
Proof.
  (* Interface: {s_inter_not, s_inter_comm, s_inter_assoc, s_inter_empty} <=> I of set_theory_2 *)
  rewrite (@s_inter_comm (¬ A)).
  rewrite -s_inter_assoc.
  replace ((B ∩ (¬ A)) ∩ A) with (B ∩ ((¬ A) ∩ A)).
  rewrite (@s_inter_comm (¬ A)).
  rewrite s_inter_not.
  by rewrite s_inter_empty s_inter_comm s_inter_empty.
  by rewrite s_inter_assoc.
Qed.

(*For each lemma expression just exists map:
B = ((¬ A) ∩ B) ∪ (A ∩ B) => I {s_union_not, ... }
((¬ A) ∩ B) ∩ (A ∩ B) = ∅ => I { ... }

And then we can add come constraints on properties of interfaces and thinking about them as about sets:
* |I| = n
  we can use just cardinality of set
* I1 in I2
  (include some interfaces in others)
* and so on and so forth ...
*)
Lemma set_theory_4 (A B : @set T) : B = ((¬ A) ∩ B) ∪ (A ∩ B).
Proof.
  (* Interface: {s_union_not, s_inter_comm, s_union_comm, s_inter_full, s_inter_union_distr_l} *)
  replace ((¬ A) ∩ B ∪ A ∩ B) with (B ∩ (¬ A) ∪ B ∩ A).
  rewrite -s_inter_union_distr_l.
  rewrite s_union_comm.
  rewrite s_union_not.
  by rewrite s_inter_full.
  f_equal; apply s_inter_comm.
Qed.

Theorem t4 (A B : @set T) : P(A ∪ B) = P(A) + P(B) - P(A ∩ B).
Proof.
  have h_s1 : A ∪ B = A ∪ ((¬ A) ∩ B) by apply set_theory_1.
  have h_s2 : A ∩ ((¬ A) ∩ B) = ∅ by apply set_theory_2.
  have h_s3 : ((¬ A) ∩ B) ∩ (A ∩ B) = ∅ by apply set_theory_3.
  have h_s4 : B = ((¬ A) ∩ B) ∪ (A ∩ B) by apply set_theory_4.
  rewrite h_s1; clear h_s1.
  rewrite (a3 h_s2); clear h_s2.
  have H : P B = P ((¬ A) ∩ B) + P (A ∩ B).
  + rewrite -(a3 h_s3).
    f_equal.
    exact: h_s4.
  + rewrite H.
    ring.
Qed.

(* End section1. TODO: think about sections... *)

(* TODO: 
* Use coq library i.e. Ensemble.v
* Check that we can call that library without errors
* Prove something for Singletons, Couples, Triples... (easy stuff but interesting)
* Prove s_inside_def and other lemmas using that library to make all logic stuff
fully checkable from low level to high level things.
* Then realize that we proved probability theory by Coq and using Coq library
*)
Parameter inside : Ensemble T → Ensemble T → Prop.
Notation "a ⊆ b" := (inside a b) (at level 30).
Check ∀ A B : set, inside (A ∪ B) (B ∩ B).

Let s_inside_def : ∀ {A B : @set T}, 
  A ⊆ B ↔ A ∩ B = A := sorry.

Lemma s_inside_self (A : @set T) : A ⊆ A.
Proof (@s_inside_def A A).2 (s_inter_self A). 

Lemma s_inside_empty {A : @set T} : ∅ ⊆ A.
Proof.
  apply s_inside_def.
  rewrite s_inter_comm.
  exact (s_inter_empty A).
Qed.

Lemma s_inside_full (A : @set T) : A ⊆ ¬∅.
Proof (s_inside_def).2 (s_inter_full A).

Lemma s_inside_eq {A B : @set T} : 
  A ⊆ B → B ⊆ A → A = B.
Proof.
  move => Hab Hba.
  apply s_inside_def in Hab, Hba.
  rewrite s_inter_comm in Hba.
  by transitivity (A ∩ B).
Qed.

Lemma s_inside_iff_eq {A B : @set T} : 
  (A ⊆ B ∧ B ⊆ A) ↔ A = B.
Proof.
split.
+ move=> [Hab Hba].
  by apply s_inside_eq.
+ move=> Heqab.
  rewrite Heqab.
  Undo. Undo.
  move=> ->. (* Just one step, instead of two! *)
  exact: conj (s_inside_self B) (s_inside_self B).
Qed.

Lemma s_inside_inter {A B C : @set T} :
A ⊆ B → A ∩ C ⊆ B.
Proof.
  move => Hab.
  apply s_inside_def in Hab.
  apply s_inside_def.
  rewrite -{2}Hab.
  rewrite s_inter_assoc.
  rewrite (s_inter_comm C _).
  by rewrite s_inter_assoc.
Qed.

Lemma s_inside_trans {A B C : @set T} : 
  A ⊆ B → B ⊆ C → A ⊆ C.
Proof.
  move=> Hab Hbc.
  apply s_inside_def in Hab.
  rewrite -Hab.
  rewrite s_inter_comm.
  exact (s_inside_inter Hbc).
Qed.

Goal ∀ (A B : set), A ⊆ ∅ → A ⊆ B.
Proof.
  (* in Lean exist tactic: assume, and we can write `assume H : A ⊆ ∅` 
  How to do things like that in Coq? *)
  move => A B H. 
  eapply s_inside_trans.
  apply H.
  exact s_inside_empty.
Qed.

(* proof 1*)
Goal ∀ (A : set), A ⊆ ∅ → A = ∅.
Proof.
  (*Interface: {s_inside_eq, s_inside_empty} *)
  move => A H.
  apply s_inside_eq.
  + exact H.
  + exact s_inside_empty.
Qed.
(* proof 2*)
Goal ∀ (A : set), A ⊆ ∅ → A = ∅.
Proof.
  (*Interface: {s_inside_def, s_inside_empty} *)
  move => A H.
  apply s_inside_def in H.
  rewrite -H.
  exact: s_inter_empty.
Qed.

Lemma key_step {A B : @set T} (h : A ∩ B = A): 
  A ∪ B = B.
Proof.
  (*Interface: {all_combinations, ...}
  How to prove that interface of this lemma must contain all_combinations?
   *)
  (* We can use set_theory_1 lemma *)
  rewrite set_theory_1.
  rewrite -{1}h.
  rewrite s_inter_comm.
  rewrite (s_inter_comm _ B).
  rewrite -s_inter_union_distr_l.
  rewrite s_union_not.
  apply s_inter_full.
Qed.

Theorem t5 (A B : @set T) : A ⊆ B → P A <= P B.
Proof.
move => H.
apply s_inside_def in H.
(* Why we need assumption A ⊆ B?
What if we can prove this theorem without it?
Answer: No, we can't prove without assumption A ⊆ B because
we must use it in proving key step: A ∪ B = B
*)
have H2 : P B = P A + P (B ∩ (¬A)).
{
  rewrite -(a3 _).
  {
    rewrite s_inter_comm.
    rewrite s_inter_assoc.
    rewrite (s_inter_comm _ A).
    rewrite s_inter_not.
    apply s_inter_empty.
  }
  f_equal.
  (*H : A ∩ B = A
    |- B = A ∪ B ∩ (¬ A)
    equivalent to
    |- B = A ∪ (B - A)
  *)

  rewrite s_union_inter_distr_l.
  rewrite s_union_not.
  rewrite s_inter_full.
  apply eq_sym.
  (* Now is key step: |- A ∩ B = A → A ∪ B = B *)
  apply: key_step H.
}
rewrite H2.
have H3 : 0 <= P (B ∩ (¬ A)) := a1 (B ∩ (¬ A)).
lra.
Qed.

(**
# Discrete, finite sample space.

Example: Roll A 6-sided die, record number.
S = {1,2,3,4,5,6}
A = "Die rool is even" = even ∩ S = {...,-2,0,2,...} ∩ S = {2,4,6}
P(A) = P({2,4,6})
  H_union: {2} ∪ {4} ∪ {6} = {2,4,6}
  H_inter: {2} ∩ {4} ∩ {6} = ∅
  H_result: P({2,4,6}) = P({2}) + P({4}) + P({6}) from H_union, H_inter


If the die is FAIR, each outcome is equally likely.
  FAIR means 
  ax1: S = {1,2,3,4,5,6}
  ax2: ∀ n, m in S, P({n}) = P({m})
  lem1: ∀ n in S, P({n}) = 1 / |S| = 1 / 6
  lem1: P({1}) = P({2}) = ... = P({6}) = 1/6

th1: P(A) = "# oucomes in A" / "# total oucomes in S" = 3 / 6 = 1 / 2

Example:
Roall a D6 until the current # matches the previous #.
Let x be that roll.
S = {2,3,4, ... }  (infinite sample space)
P(x = 1) = 0
P(x = 2) = 6 / 36 from th1
P(x = 3) = 
  Roll 3 = Roll 2 but Rool2 ≠ Rool 1
  
#OVER-ANALYSIS ON
  "But" word formally means Rool2 = Rool 1
   surprisingly not that our intuition imaging: Rool2 ≠ Rool 1
   Interecting how this word can be formalized?
   We must first formalize definition of intuition?
#OVER-ANALYSIS OFF

= ("30 non-doubles" / "36 total outcomes") *  (* Rool 2 *)
  * ("1 good outcome" / "6 outcomes")         (* Rool 3 *)
= 5 / 36


In general:
 What is an probabilty of event when x = n.
 P (X = n) = P(Rool 2 ≠ Roll 1) * P(Rool 3 ≠ Roll 2) ...
   * P(Rool n ≠ Roll (n-1))
 = 5/6 * 5/6 * ... * 1/6
   --------------------- = (5/6)^(n-2) * (1/6)
   2   *   3 ...   * n

Do these add up to 1?
We can prove this:
  
*)


Check sum_eq.

Check sum_f 0 5 (fun n => 1).

Check sum_cte : ∀ (x : R) (N : nat),
  sum_f_R0 (fun=> x) N = x * INR (S N).
  
Check INR 6.



(*
Rge_antisym: ∀ r1 r2 : R, r1 >= r2 → r2 >= r1 → r1 = r2
Rge_dec: ∀ r1 r2 : R, {r1 >= r2} + {¬ r1 >= r2}
RList.RList_P1: ∀ (l : list R) (a : R), RList.ordered_Rlist l → RList.ordered_Rlist (RList.insert l a)
sqrt_Rsqr_abs: ∀ x : R, sqrt x² = Rabs x
Rlt_dec: ∀ r1 r2 : R, {r1 < r2} + {¬ r1 < r2}
Majxy_cv_R0: ∀ x y : R, Un_cv (Majxy x y) 0

Rle_0_sqr: ∀ r : R, 0 <= r²
pos_INR: ∀ n : nat, 0 <= INR n

Rle_0_1: 0 <= 1
*)


Check Rplus_0_l.
Check Rmult_assoc.
End section1.

(**
  A ∪ B = (A ∩ B) ∪ (A ∩ ¬ B) ∪ (¬ A ∩ B) by bruteforce_all_set_combinations
  A ∪ B = A ∪ (¬ A ∩ B)
  P(A ∪ B) = P(A ∪ (¬ A ∩ B)) by congruence or f_equal
  
  main fact that enables using a3 is: A ∩ (¬ A ∩ B) = ∅
  P(A ∪ (¬ A ∩ B)) = P(A) + P(¬ A ∩ B) from a3
  now goal is:
  |- P(¬ A ∩ B) = P(B) - P(A ∩ B)
  We can prove this as lemma.
  
  Lemma: ∀ A B : Set, P(B ∩ ¬ A) = P(B) - P(A ∩ B)
  Proof.
  |- P(B) = P(B ∩ ¬ A) + P(B ∩ A)
  (B ∩ ¬ A) ∩ (B ∩ A) = ∅ by set_theory
  we can apply a3 in reverse:
  P((B ∩ ¬ A) ∪ (B ∩ A)) = P(B ∩ ¬ A) + P(B ∩ A) from <- a3
  
  |- P(B) = P((B ∩ ¬ A) ∪ (B ∩ A)) by f_equal
  |- B = (B ∩ ¬ A) ∪ (B ∩ A)
  Done by set_theory.
*)

(*
Lemma key_step_ {A B : @set T} (h : A ∩ B = A): 
  A ∪ B = B.
Proof.
have h2 : A ∪ A ∩ B = A ∪ A := sorry.
have h3 : A ∪ A ∩ B = A := sorry.
rewrite s_union_inter_distr_l in h3.
rewrite s_union_self in h3.
rewrite -{3}h in h3.
Undo. Undo. Undo.
rewrite -h.
rewrite s_union_comm.
rewrite s_union_inter_distr_l.
rewrite s_union_self.
rewrite s_inter_comm.
rewrite s_inter_union_distr_l.
rewrite s_inter_self.
rewrite s_inter_comm.
(* Fail: we returned to same state |- B ∪ A ∩ B = B *)
Restart.
rewrite -h.
rewrite s_union_comm.
rewrite s_union_inter_distr_l.
rewrite s_union_self.
rewrite -h.
rewrite s_union_inter_distr_l.
rewrite s_union_self.
rewrite (s_inter_comm (_ B A) _).
rewrite s_inter_union_distr_l.
rewrite s_inter_self.
rewrite s_inter_comm.
(*
B ∩ (B ∪ B ∩ A) = B
*)
rewrite s_inter_union_distr_l.
rewrite s_inter_self.
Restart.
rewrite all_combinations.
  rewrite h.
  rewrite (s_union_inter_distr_l A).
  rewrite s_union_self.
Restart.
  rewrite all_combinations.
  rewrite -s_inter_union_distr_l.
  rewrite s_union_not.
  rewrite s_inter_full.
  (* rewrite s_union_inter_distr_l.
  rewrite s_union_not. 
  We returned to beginning state => Fail *)
  rewrite -{1}h.
  rewrite s_inter_comm.
  rewrite (s_inter_comm _ B).
  rewrite -s_inter_union_distr_l.
  rewrite s_union_not.
  by rewrite s_inter_full.
Qed.
*)

(** 
Parameter real_zero : Real. 
Local Notation "0" := real_zero.
*)