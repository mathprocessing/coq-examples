Section Some.
Local Open Scope nat_scope.

Parameter R : nat -> nat -> Prop.
Axiom R_refl : forall x, R x x.
Axiom R_sym : forall x y, R x y -> R y x.
Axiom R_trans : forall x y z, R x y -> R y z -> R x z.

Theorem test : forall x, R x 0 <-> R 0 x.
Proof.
  intros.
  constructor. (* apply conj. *)
  apply R_sym.
  apply R_sym.
Qed.

Print test.

Lemma R_sym_eq : forall {x y}, R x y <-> R y x.
Proof.
  intros.
  exact (conj (R_sym x y) (R_sym y x)).
Qed.

Definition test' : forall x, R x 0 <-> R 0 x := fun x : nat => R_sym_eq.

(* Set Implicit Arguments. *)

(* Implicit Type (x y z : bool). *)

Parameter or : bool -> bool -> bool.
Axiom or_true : forall x, or true x = true.
Axiom or_false : forall x, or false x = x.
Lemma or_comm : forall x y, or x y = or y x.
  intros. 
  destruct x.
  1: rewrite or_true.
  2: rewrite or_false.
  all: destruct y.
  all: (rewrite or_true + rewrite or_false); trivial.
Qed.
Lemma or_refl : forall x, or x x = x.
  intros.
  destruct x.
  all: (rewrite or_false || rewrite or_true); trivial.
Qed.

Parameter not' : bool -> bool.
Axiom not'_true : not' true = false.
Axiom not'_false : not' false = true.

Parameter not : bool -> bool.
Parameter xor : bool -> bool -> bool.
Axiom iff_equal_eq : iff = eq.

Axiom not_def : forall x, not x <> x.
Lemma helper : forall x, (not true = x -> (x = false)).
  intros x h.
  destruct x.
  pose (h2 := not_def true).
  contradiction.
  trivial.
Qed.

Lemma helper2 : forall (A : Prop), (A -> true = false) -> (A -> False).
Proof. intros. refine (_ (H H0)). discriminate. Qed.

Lemma helper3 : forall x, x = true \/ x = false.
  intros x.
  apply (bool_ind (fun x => x = true \/ x = false)).
  (* how to backtrack like in Lean prover? 
  For example: left <|> right <|> trivial *)
  (* all: ((try left + (try right))); trivial. NOT WORKS*)
  1: left. 2: right. all: reflexivity.
Qed.

Lemma helper4 : forall A B : Prop, (A -> False) -> (A \/ B) -> B.
Proof. intros. intuition. Qed.

(* Lemma recursion : forall x y, (x = true -> not x = y -> y = false) *)

Lemma not_true : not true = false.
  (* pose (h := helper false). (not true = false -> True) <=> True  => bad branch *)
  assert (h : not true = true -> true = false) by exact (helper true).
  assert (h2 : not true = true -> False) by exact (helper2 _ h). clear h.
  assert (h3 : not true = true \/ not true = false) by apply helper3.
  destruct h3 as [h_true | h_false].
    exfalso; exact (h2 h_true).
    exact h_false.
Qed.

Lemma not_false : not false = true.
  assert (h : _) by exact (helper3 (not false)).
  destruct h as [h_true | h_false].
  exact h_true.
  (* make contradiction *) (* h_false: not false = false *)
  exfalso; apply ((not_def false) h_false).
  Show Proof.
Qed.

(** simplest example of meta-proof (proof about manipulating information of another proof)
1. palindromic proof. (see basic group theory lemmas for simple examples)
*)


Create HintDb logic.
Hint Resolve helper3 not_def : logic.
Hint Rewrite helper3 : logic.
Lemma not_def_true : not true <> true. Proof. auto with logic. Qed.
Hint Resolve not_def_true : logic.
Lemma not_def_true' : not true = true -> False. Proof. auto with logic. Qed.

Hint Rewrite or_true or_false : logic.
(* DANGER! : do not use or_comm, because this makes infinite rewriting loop *)

Example test_rewrite : or false (or true true) = or false true.
Proof.
  erewrite 1!or_true, 5?or_false, 5?or_comm. (* works without infinite loop !!! *)
  (* 1! ~ exact 1 iteration, 5? ~ <= 5 iterations *)
  (* autorewrite with logic. *)
  reflexivity.
  (* Show Proof. *)
Qed.

Hint Resolve or_true or_false : logic.


Ltac mytac := 
  match goal with
  | h1 : _, h2 : _ |- (_ = _) => idtac "equality with hypothesis" h1 h2
  | |- (_ = _) => idtac "equality"
  | _ => idtac "other thing"
  end.

Ltac basic :=
match goal with
    | |- True => trivial
    | _ : False |- _ => contradiction
    | _ : ?A |- ?A => assumption
end.
(* Ltac simplify :=
repeat (intros;
    match goal with
        | |- _ \/ _ => (left; my_tauto) || (right; my_tauto)
        | |- _ => assumption
        | h : _ -> ?g |- ?g => apply h
        | H : ~ _ |- _ => red in H
        | H : _ /\ _ |- _ =>
            elim H; do 2 intro; clear H
        | H : _ \/ _ |- _ =>
            (elim H; intro; clear H) || destruct H
        | H : ?A /\ ?B -> ?C |- _ =>
            cut (A -> B -> C);
                [ intro | intros; apply H; split; assumption ]
        | H: ?A \/ ?B -> ?C |- _ =>
            cut (B -> C);
                [ cut (A -> C);
                    [ intros; clear H
                    | intro; apply H; left; assumption ]
                | intro; apply H; right; assumption ]
        | H0 : ?A -> ?B, H1 : ?A |- _ =>
            cut B; [ intro; clear H0 | apply H0; assumption ]
        | |- _ /\ _ => repeat split; assumption
        | |- ~ _ => red
        | |- _ <-> _ => constructor
        | |- ?a = ?a => reflexivity
    end).

Example test1: forall A : Prop, ~(A = A) -> False.
Proof. Info 0 simplify. Qed.

Example test2 : forall A B : Prop, A /\ B <-> B /\ A.
Proof. Info 0 simplify. Qed.

Example test3 : forall A B C : Prop, A /\ B /\ C <-> B /\ C /\ A.
Proof. Info 0 simplify. Qed.

Example test4 : forall A B C : Prop, A /\ B /\ C -> B \/ C.
Proof. Info 0 simplify. Qed. *)

Axiom and_refl : forall A : Prop, A -> A /\ A.

(* test backtracking (depth first search) *)
Ltac back := 
  match goal with
    | |- True => trivial
    | _ => contradiction
    | |- _ \/ _ => (left; back) || (right; back)
    | |- _ /\ _ => (apply and_refl; back)
    | _ => assumption || fail "No such assumption"
  end.

Example test_back1 : forall A B C P : Prop, B -> (A /\ A) \/ B \/ True.
  intros.
  Info 0 back. (* right. left. assumption. *)
Qed.

Example test_back2 : forall A B C P : Prop, False -> (A /\ A) \/ B \/ True.
  intros.
  Info 0 back. (* contradiction. *)
Qed.

Example test_back3 : forall A B C P : Prop, A -> (A /\ A) \/ B \/ True.
  intros.
  Info 0 back.(* left. apply and_refl. assumption. *)
Qed.

Example test_back4 : forall A B C P : Prop, (A /\ A) \/ B \/ True.
  intros.
  Info 0 back. (* right. right. trivial. *)
Qed.
Example test5 : forall A B C : Prop, A \/ B <-> B \/ A.
Proof.
  intros.
  constructor.
  intros.
  destruct H.
  right. assumption.
  left. assumption.
  intros.
  destruct H.
  (* (left + right); assumption.
  (left + right); assumption. *)
  Info 0 simplify.
Qed.

Ltac rwa := try assumption;
  match goal with
  | |- (?a = ?b) =>
    match a with
    | (or ?c ?d) => idtac "matched or" c d; tryif 
    | _        => idtac "nothing"
    end
  | _ => idtac "other thing"
  end.

Lemma or_comm' : forall x y, or x y = or y x.
Proof.
intros.
rwa.
Defined.



Lemma not_false' : not false = true.
  intuition.
Qed.


 
  (* not true = true \/ not true = false <=> A \/ B *)
  (* (A -> False) -> (A \/ B) -> B *)
  (* Check (helper4 _ h3). *)


  pose (h_false := eq_refl false).


  







Lemma not_true' : not true = false.
  (* pose (H := not_def true).
  apply (bool_ind _). *)

  (* pose (h := helper true).
  2: {
    replace eq with iff by exact iff_equal_eq.
  } *)

Abort.
  



  
  
  


Check or_refl true.

End Some.