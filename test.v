Require Import List.
Import ListNotations.

From MyExamples Require Import Short.

Check Refl.f.

(* From MyExamples Require Import Short.
 *)
Require Import MyExamples.Short.

Eval compute in [1;2;3].
Eval compute in 1 :: 2 :: [].


Eval compute in In 5 [6].

(**
T1: P -> P \/ False
T2: goal: P \/ False <-> P =>
    goal: P <-> P             by rewrite T1

try to abstract this method:
T3: (C -> A) -> (C <-> B) -> (B <-> A) 
[this is a undetermined statement!] (not provable)
*)

Lemma T3 (A B C : Prop):
  (C -> A) -> (C <-> B) -> (B <-> A).
Proof.
(* (0 -> 0) -> (0 <-> 0) -> (0 <-> 0) = 1 -> (1 -> 1) = 1 -> 1 = 1
(0 -> 0) -> (0 <-> 1) -> (1 <-> 0) = 1 -> (0 -> 0) = 1 -> 1 = 1
(0 -> 1) -> (0 <-> 0) -> (0 <-> 1) = 1 -> (1 -> 0) = 1 -> 0 = 0 => not provable
(0 -> 1) -> (0 <-> 1) -> (1 <-> 1) = 1 -> (0 -> 1) = 1 -> 1 = 1
(1 -> 0) -> (1 <-> 0) -> (0 <-> 0) = 0 -> (0 -> 1) = 0 -> 1 = 1
(1 -> 0) -> (1 <-> 1) -> (1 <-> 0) = 0 -> (1 -> 0) = 0 -> 0 = 1
(1 -> 1) -> (1 <-> 0) -> (0 <-> 1) = 1 -> (0 -> 0) = 1 -> 1 = 1
(1 -> 1) -> (1 <-> 1) -> (1 <-> 1) = 1 -> (1 -> 1) = 1 -> 1 = 1 *)
Abort.

Lemma cancel_false : forall P, P \/ False <-> P.
Proof.
intros P.
apply iff_to_and.
split. 
- intros h.
  refine (or_ind _ _ h).
  + exact id.
  + intro f. exfalso. exact f.
- intros hp.
  exact (or_introl hp).
Qed.

(** 
(fun (P : Prop) (H : P \/ False) =>
 or_ind (fun H0 : P => H0)
   (fun H0 : False => False_ind P H0) H)
*)

Goal ~ In 5 [6].
Proof.
intro h.
unfold In in h.
apply -> cancel_false in h.
Show Proof.
discriminate.
Show Proof.
Qed.

Goal In 5 [6] = False.
unfold In.
replace (6 = 5) with False.
Set Printing All.
rewrite (cancel_false False).
Check or_introl.
