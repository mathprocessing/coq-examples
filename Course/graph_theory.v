From mathcomp Require Import ssreflect ssrfun ssrbool eqtype ssrnat.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import Bool.
Require Import List.
Import ListNotations.
From MyExamples Require Import sorry.

Example or_refl x: orb x x = x.
Proof.
case x; trivial.
Qed.

(* using `case: (bool_cases x) => H; rewrite H.` *)
Example bool_cases x : (x = true) \/ (x = false) := sorry.

Example test_imp x y: y ==> (x ==> y).
Proof.
case: (bool_cases x) => H; rewrite H.
all: simpl.
case y; easy.
rewrite implb_true_r.
exact isT.
Qed.

Example de_morgan x y: negb (orb x y) = andb (negb x) (negb y).
Proof.
by case x.
Restart.
case y.
rewrite andb_false_r orb_true_r //.
rewrite andb_true_r orb_false_r //.
Qed.

Lemma eqb_true_l x: eqb true x = x. Proof. by case x. Qed.
Lemma eqb_true_r x: eqb x true = x. Proof. by case x. Qed.
Lemma eqb_false_l x: eqb false x = ~~ x. Proof. by case x. Qed.
Lemma eqb_false_r x: eqb x false = ~~ x. Proof. by case x. Qed.

Lemma negb_true: ~~true = false. Proof erefl.
Lemma negb_false: ~~false = true. Proof erefl.

Check de_morgan.

(* ABAAA *)
Check [true; false; true; true; true].

Fixpoint any (l : list bool) :=
match l with
| [] => false
| x::xs => orb x (any xs)
end.

Example any_nil : any nil = false := erefl.
Example any_true xs : any (true :: xs) = true := erefl.
Example any_false xs : any (false :: xs) = any xs := erefl.
Example any_orb x xs : any (x :: xs) = x || any xs := erefl.

Example any_not x xs : negb (any (x::xs)) ==> ~~ x.
Proof.
rewrite implb_contrapositive.
rewrite any_orb.
case x; first easy.
rewrite implb_false_l.
exact isT.
Qed.

Fixpoint count (e : bool) (l : list bool) :=
match l with
| [] => 0
| x::xs =>
  match (eqb x e) with
  | true => S (count e xs)
  | false => count e xs
  end
end.

Example count_nil x : count x nil = 0 := erefl.
Example count_eq x xs : count x (x :: xs) = S (count x xs).
Proof. simpl; by rewrite eqb_reflx. Qed.

Example count_neq x xs : count x (~~x :: xs) = count x xs.
Proof. simpl; by rewrite eqb_negb1. Qed.

Example any_zero_trues (xs : list bool) : negb (any xs) ==> (count true xs == 0).
Proof.
elim: xs.
trivial.
move=>a l ih.
rewrite any_orb.
case a; first trivial.
rewrite orb_false_l.
simpl.
exact ih.
Qed.

Goal 1 = 1.
Proof.
assert (h := any_zero_trues [true]).
rewrite any_true in h.
rewrite negb_true in h.
rewrite implb_false_l in h.
Abort.

Goal 1 = 1.
Proof.
assert (h := any_zero_trues [false]).
rewrite any_false in h.
rewrite any_nil in h.
rewrite negb_false in h.
rewrite implb_true_l in h.
rewrite -negb_false in h.
rewrite count_neq in h.
rewrite count_nil in h.
Abort.

Section Graphs.

(* https://stackoverflow.com/questions/42871971/how-to-define-finite-set-of-n-elements-in-coq *)
Inductive Graph :=
graph : (nat -> nat -> bool) -> nat -> Graph.

Definition non_connected_graph := graph (fun _ _ => false).
Definition full_graph := graph (fun _ _ => true).
Definition full_loopless_graph := graph (fun v1 v2 => v1 != v2).
Definition star_graph := graph (fun v1 v2 => (v1 < 1) && (v1 != v2)).

Definition count_nodes g := match g with graph _ n => n end.

Definition connected (v1 v2 : nat) (g : Graph) : bool :=
match g with graph f n => if (v1 < n) && (v2 < n) then f v1 v2 else false end.

(* https://stackoverflow.com/questions/26755507/coq-notation-for-multi-type-list *)
Local Notation "a --> b" := (connected a b) (at level 1).

Example connected_of_non_connected v1 v2 n:
  v1 --> v2 (non_connected_graph n) == false.
Proof. simpl; by rewrite if_same. Qed.

Example connected_of_non_connected' v1 v2 n:
  v1 --> v2 (non_connected_graph n) = false.
Proof. Admitted.

(* we save andb_diag for learning purposes *)
Check andb_diag : forall b : bool, b && b = b.
Example connected_of_full_loopless_graph v n:
  v --> v (full_loopless_graph n) == false.
Proof. simpl; by rewrite eq_refl negb_true if_same. Qed.

Goal forall n, 0 --> 0 (non_connected_graph n) == false.
Proof. move=> n. by rewrite (connected_of_non_connected 0 0 n). Qed.
Goal 2 --> 0 (full_graph 1)          = false.  Proof erefl.
Goal 1 --> 1 (full_loopless_graph 2) == false. Proof connected_of_full_loopless_graph 1 2.
Goal 0 --> 1 (full_loopless_graph 2) = true.   Proof erefl.

Fixpoint count_edges_from_aux upper_v v g :=
match upper_v with
| 0 => 0
| (S current_v) => 
  let m := count_edges_from_aux current_v v g in
  if v --> current_v g then S m else m
end.

Example count_edges_from_aux_zero v g : count_edges_from_aux 0 v g = 0 := erefl.
Example count_edges_from_aux_step current_v v g :
  let m := count_edges_from_aux current_v v g in
  count_edges_from_aux (S current_v) v g = if v --> current_v g then S m else m := erefl.

Definition count_edges_from v g :=
match g with graph _ n => count_edges_from_aux n v g end.

Fixpoint count_edges_aux up_v g :=
match up_v with
| 0 => 0
| (S cur_v) => count_edges_from cur_v g + count_edges_aux cur_v g
end.

Example count_edges_aux_zero g : count_edges_aux 0 g = 0 := erefl.
Example count_edges_aux_step cur_v g :
  count_edges_aux (S cur_v) g = count_edges_from cur_v g + count_edges_aux cur_v g := erefl.

Definition count_edges g :=
match g with graph _ n => count_edges_aux n g end.

Example count_edges_def {f} n (g := graph f n) : count_edges g = count_edges_aux n g := erefl.

Goal Nat.div2 (count_edges (full_loopless_graph 0)) = 0. Proof erefl.
Goal Nat.div2 (count_edges (full_loopless_graph 1)) = 0. Proof erefl.
Goal Nat.div2 (count_edges (full_loopless_graph 2)) = 1. Proof erefl.
Goal Nat.div2 (count_edges (full_loopless_graph 3)) = 3. Proof erefl.
Goal Nat.div2 (count_edges (full_loopless_graph 4)) = 6. Proof erefl.
Goal Nat.div2 (count_edges (full_loopless_graph 5)) = 10. Proof erefl.

Example edges_of_zero_nodes (g : Graph) (h : count_nodes g = 0) : count_edges g = 0.
Proof. case g in *; rewrite /count_nodes in h; by rewrite h /count_edges. Qed.

Check @edges_of_zero_nodes (graph (fun _ _ => true) 0).

Compute count_edges (non_connected_graph 1).

Example non_connected_graph_edges n : count_edges (non_connected_graph n) = 0.
Proof.
rewrite count_edges_def.
elim: n; trivial.
move => n ih1.
rewrite count_edges_aux_step.
rewrite /count_edges_from.
rewrite count_edges_from_aux_step.
rewrite -/non_connected_graph.
rewrite connected_of_non_connected'.
elim n; trivial.
move => n0 ih2.
rewrite count_edges_from_aux_step.
rewrite count_edges_aux_step.
rewrite connected_of_non_connected'.
Admitted.

(*rewrite /non_connected_graph.
rewrite /count_edges.
case: n.
by rewrite count_edges_aux_zero.
move => n.
rewrite count_edges_aux_step.
simpl.
rewrite if_same.
case: n.
by rewrite count_edges_from_aux_zero.
move => n.
simpl.
rewrite if_same.*)



rewrite /count_edges_aux.
Admitted.

Example star_graph_edges n : count_edges (star_graph (S n)) = n := sorry.
Example full_graph_edges n : count_edges (full_graph n) = n * n := sorry.
Example full_loopless_graph_edges n :
  count_edges (full_loopless_graph (S n)) = S n * n := sorry.




Check full_loopless_graph_edges 0.
Compute (count_edges (star_graph 5)).

Definition count_edges g :=
match g with
| graph f n => 
end.

Goal count_edges (non_connected_graph 5) = 0. Proof erefl.
Goal count_edges (full_loopless_graph 3) = 3. Proof erefl.

Check graph 0 v_equal.



















