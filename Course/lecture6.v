From mathcomp Require Import ssreflect ssrfun ssrbool eqtype ssrnat.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Lemma addnC :
  commutative addn.
Proof.
move=> x y.
elim: x. (*`:` clears x from context *)
- rewrite add0n.
  elim: y=> // y IHy.
  rewrite addSn.
  rewrite -IHy.
  done.
Restart.
elim => [| x IHx] y;
first by rewrite addn0.
rewrite addSn IHx -addSnnS.
done.
Undo.
Undo.
by rewrite addSn IHx -addSnnS.
Qed.

Fixpoint factorial_helper n acc :=
  if n is n'.+1 then
    factorial_helper n' (n * acc)
  else
    acc.

(** The iterative implementation of the factorial
function: *)
Definition factorial_iter (n : nat) : nat :=
  factorial_helper n 1.

Goal factorial_iter 0 = 1. Proof erefl.
Goal factorial_iter 3 = 6. Proof erefl.

(** Let's prove our iterative implementation of
factorial is correct. *)

Lemma factorial_iter_correct n :
  factorial_iter n = n`!.
Proof.
rewrite /factorial_iter.
elim: n.
simpl.
by [].
move=> n ih.
rewrite /factorial_helper.
Restart.
































