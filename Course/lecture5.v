From mathcomp Require Import ssreflect ssrfun ssrbool eqtype ssrnat div.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Definition A_implies_A' :
  forall (A : Prop), A -> A :=
  fun A proof_A => proof_A.

Definition A_implies_A'' :
  forall (A : Prop), A -> A :=
  fun A => id.

(* The Lemma vernacular essentially means Definition.
But in this case, instead of providing the body of the definition after the := syntax we finish the statement with a dot (.).
*)
Lemma A_implies_A :
  forall (A : Prop), A -> A.
Proof.
intros A proof_A.
exact proof_A.
Restart.
move => A.
move => proof_A.
exact: proof_A.
Qed.

Lemma A_implies_A_2 :
  forall (A : Prop), A -> A.
Proof.
move=> A.
Show Proof.
move=> proof_A.
Show Proof.
exact: proof_A.
Show Proof.
Unset Printing Notations.
Show Proof.
Set Printing Notations.
Qed.

Lemma modus_ponens (A B : Prop) :
  A -> (A -> B) -> B.
Proof.
move=> ha.
apply. (* backward reasoning *)
(* earlier one can't see apply with 0 arguments *)
exact: ha.
Qed.

Definition and1 (A B : Prop) :
  A /\ B -> A.
Proof.
case.         (* take apart conjunction *)
move=> a _.   (* move two assumptions in one go *)
exact: a.
Restart.
case.
exact: id.
Qed.

Definition andC (A B : Prop) :
  A /\ B -> B /\ A.
Proof.
case.
move => a b.
split.
 - exact b.
exact: a.
Qed.

Lemma or_and_distr A B C :
  (A \/ B) /\ C -> A /\ C \/ B /\ C.
Proof.
case; case.
left.
split; assumption.
right.
split; assumption.
Qed.

Lemma or_and_distr1 A B C :
  (A \/ B) /\ C -> A /\ C \/ B /\ C.
Proof.
case.
case=> [a|b] c.
+ by left.
by right.
Qed.

Lemma or_and_distr2 A B C :
  (A \/ B) /\ C -> A /\ C \/ B /\ C.
Proof. by case=> [[a|b] c]; [left | right]. Qed.

Lemma HilbertSaxiom A B C :
  (A -> B -> C) -> (A -> B) -> A -> C.
Proof.
move=> abc ab a.
move: abc.
apply.
- by [].
move: ab.
apply.
done.
Restart.
move=> abc ab a.
apply: abc.
- by [].
by apply: ab.
Restart.
move=> abc ab a.
(* special action // meaning "try solving the new trivial subgoal and do not change a subgoal if it's not trivial". *)
apply: abc=> //.
by apply: ab.
Qed.

Section RewriteTactic.

Variable A : Type.
Implicit Types x y z : A.

Lemma esym x y :
  x = y -> y = x.
Proof.
move=> x_eq_y.
rewrite x_eq_y.
trivial.
Undo.
by [].
Qed.

Lemma eq_sym_shorter x y :
  x = y -> y = x.
Proof.
(* move=> eq; rewrite eq same as "move => ->" *)
move => ->.
done.
Qed.

Lemma eq_trans x y z :
  x = y -> y = z -> x = z.
Proof.
move => ->.
done.
Restart.
move => -> ->.
done.
Qed.

End RewriteTactic.

Lemma addnA :
  associative addn.
Proof.
rewrite /associative. (* unfold definition *)
move=> x y z.
(* But the above solution if not idiomatic, the
experienced user knows what `associative` looks
like, and the `=>` tactical can look inside of
definitions *)
Restart.
Show.
move=> x y z.
(* Proving this lemma needs induction which we can
ask Coq to use with `elim` tactic. We'd like to do
induction on the variable `x`. But, as usual,
`elim` operates on the top of the goal stack, so
we need to put back `x` into the goal: *)
elim x. (* induction over `x` in this case *)
Show Proof.
Check nat_ind.
(* Notice that `elim` uses the `nat_ind` induction
principle under the hood. *)
- (* the base case *)
  (* `0 + (y + z)` reduces into `y + z` and
     `0 + y` reduces to `y`, hence
     the goal is equivalent to `y + z = y + z`,
     i.e. it can be solved trivially. *)
exact erefl.
(* inductive step *)
(* [IH] stands for "induction hypothesis" *)
move=> n. (* but we wnat to induction on x.
Let's Restart proof. *)
Restart.
move=> x y z.
move: x.
elim.
+ exact erefl.
move => x IHx.
Fail rewrite IHx.
(* To use `IHx` we need to change the goal to
   contain `x + (y + z)`. To achieve this we need
   to use a lemma which lets us simplify `x.+1 +
   y` into `(x + y).+1`. Let's use the `Search`
   mechanism to find such a lemma: *)
Search (_.+1 + _).
(* This query returns a list of lemmas and a
brief examination shows `addSn` is the lemma we
are looking for *)
rewrite addSn IHx.
done.
Restart.
(* The proof can be simplified into: *)
by move=> x y z; elim: x=> // x IHx; rewrite addSn IHx.
Qed.





















