From mathcomp Require Import ssreflect ssrfun ssrbool eqtype ssrnat.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import Unicode.Utf8.
Require Import Bool.

Check PeanoNat.Nat.neq_0.
Example neq_0 : ¬ (∀ n : nat, n = 0).
Proof.
intro h.
exact (n_Sn 0 (esym(h 1))).
Undo.
exact (absurd _ (esym(h 1) : 0 = 1) (n_Sn 0 : 0 ≠ 1)).
Qed.