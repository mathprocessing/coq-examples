From mathcomp Require Import ssreflect ssrfun ssrbool ssrnat eqtype.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import Arith.

Definition succ_inj (n m : nat) :
  n.+1 = m.+1 -> n = m := 
  fun Sn_Sm : n.+1 = m.+1 =>
    match Sn_Sm in (_ = Sm)
    return (n = Sm.-1)
  with
  | erefl => erefl n
  end.

Definition or_introl_inj (A B : Prop) (p1 p2 : A) :
  or_introl p1 = or_introl p2 :> (A \/ B) ->
   p1 = p2
:=
  fun eq =>
    match eq in (_ = oil2)
      return (p1 =
         if oil2 is or_introl p2' then p2' else p2)
   with
   | erefl => erefl p1
   end.

Check or_introl_inj.

Definition congr1 (A B : Type) :
  forall (f : A -> B) (x y : A),
    x = y -> f x = f y
:=
  fun f x y (Hxy : x = y) =>
    match Hxy : x = y in (_ = b)
      return (f x = f b)
    with
    | erefl => erefl (f x)
    end.

Definition addn0 :
  forall n : nat, n + 0 = n
:=
  fix rec (n : nat) : n + 0 = n :=
    match n return (n + 0 = n) with
    | O => erefl 0
    | S n' => congr1 S (rec n')
    end.

Definition add0n :
  forall n : nat, 0 + n = n
:=
  fun n : nat => erefl n.

Definition nat_ind :
  forall (P : nat -> Prop),
    P 0 ->
    (forall n : nat, P n -> P n.+1) ->
    forall n : nat, P n
:=
  fun P p0 step =>
    fix rec (n : nat) :=
    match n with
    | O    => p0
    | S n' => step n' (rec n')
    end.
    
(** In type theory induction is just recursion! *)

Definition addn0' :
  forall n : nat, n + 0 = n
:=
  @nat_ind 
   (fun n => n + 0 = n)
   erefl
  (fun k ih => congr1 S ih).


Check ~ exists n : nat, n = n.

Check ex_intro (fun n : nat => n = n) 5 (erefl 5).

Example test : exists (n : nat), n + 1 = 1.
Proof.
refine (ex_intro 
(fun n : nat => n + 1 = 1) _ _).
Unshelve.
all: swap 1 -1.
exact 0.
(* 2: exact 0. *)
exact (erefl 1).
Qed.

Example test_swap (A B : Prop) (ha: A) (hb: B):
  A -> B -> A /\ B.
Proof.
split.
all: swap 1 2.
exact hb.
exact ha.
Show Match list.
Show Proof.
Qed.

Definition sum : list nat -> nat :=
fix s xs := match xs with
| nil => 0
| cons x xs' => x + s xs'
end.

Goal sum nil = 0. Proof erefl.
Goal sum (cons 2 nil) = 2. Proof erefl.
Goal sum (cons 2 (cons 3 nil)) = 5. Proof erefl.

Example test_list (x : nat) (xs : list nat) :
sum (cons x nil) = x.
Proof Nat.add_0_r x.




