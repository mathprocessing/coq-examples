From mathcomp Require Import ssreflect ssrfun ssrbool eqtype ssrnat.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import Unicode.Utf8.
Require Import Bool.
Require Import List.
Require Import Arith.
Import ListNotations.

Axiom sorry : forall {A : Type}, A.

Parameter f: nat -> nat.
Axiom a1 : f 5 = 6.

Lemma ex1 : f 5 + f 5 = 12.
Proof.
rewrite a1 //.
Qed.




Example eq_is_iff (a b : Prop) : (a = b) = (a <-> b) := sorry.

Example nat_step n m : (n = m) = (n.+1 = m.+1).
Proof.
rewrite eq_is_iff.
apply conj; intro h; first rewrite h; trivial.
Check congr1 S (erefl 0) : 1 = 1.
Check f_equal S (erefl 0) : 1 = 1.
revert h.
exact (eq_add_S n m).
Qed.

(* using `case: (bool_cases x) => H; rewrite H.` *)
Example bool_cases x : (x = true) \/ (x = false) := sorry.
Lemma eqb_true_l x: eqb true x = x. Proof. by case x. Qed.
Lemma eqb_true_r x: eqb x true = x. Proof. by case x. Qed.
Lemma eqb_false_l x: eqb false x = ~~ x. Proof. by case x. Qed.
Lemma eqb_false_r x: eqb x false = ~~ x. Proof. by case x. Qed.

Lemma negb_true: ~~true = false. Proof erefl.
Lemma negb_false: ~~false = true. Proof erefl.


(** https://github.com/coq/coq/blob/master/theories/Bool/Bool.v *)
(** Most of the lemmas in this file are trivial by case analysis *)

Ltac destr_bool :=
 intros; destruct_all bool; simpl in *; trivial; try discriminate.

(**
1. Prove that don't exist solution
2. Find shortest proof.

* * *     1 0 0
* * * --> 1 1 0
* * *     1 0 0

Where `-->` is a iteration of game of life rule.
 *)

Check 
[
   0; 0; 0
 ; 0; 0; 0
 ; 0; 0; 0
].

(**
a function - one step before b function
b function - b 0 0 = 1, b 0 1 = 0, ...
*)

(** Firstly let's solve more simple problem with 
Rule = xor_lst [(f x y); (f x y+1); (f x y-1); (f x-1 y); f x+1 y)]
*)

Definition F := false.
Definition T := true.

Fixpoint bfold (init: bool) (bool_func : bool → bool → bool) (l : list bool) :=
match l with
| [] => init
| x::xs => bool_func x (bfold init bool_func xs)
end.

Definition xor_lst := bfold F xorb.
Definition or_lst := bfold F orb.
Definition and_lst := bfold T andb.

Hint Immediate xorb_false_l : core.
Hint Immediate xorb_false_r : core.

Example xor_lst_step x xs : xor_lst (x::xs) = if x then ~~(xor_lst xs) else (xor_lst xs).
Proof.
assert (k : bool) by exact sorry.
replace (~~ xor_lst xs) with k by exact sorry.
rewrite -(xorb_false_l (xor_lst xs)).
replace k with (~~ xor_lst xs) by exact sorry.
trivial.
Qed.

Example or_lst_step x xs : or_lst (x::xs) = if x then T else or_lst xs := erefl.
Example and_lst_step x xs : and_lst (x::xs) = if x then and_lst xs else F := erefl.
Example and_lst_true xs : and_lst (true :: xs) = and_lst xs := erefl.
Example and_lst_false xs : and_lst (false :: xs) = false := erefl.

Goal xor_lst [F; T; T] = F. Proof erefl.
Goal or_lst [F; F] = F. Proof erefl.
Goal and_lst [T; T] = T. Proof erefl.

Parameter A B C : nat → nat → bool.
Check xor_lst [A 2 3].
Check forall x : nat, xor_lst [A 2 x].

Axiom C_xor : ∀ x y : nat,
  xor_lst [C (x+1) (y+1); C (x+1) (y+2); C (x+1) y; C x (y+1); C (x+2) (y+1)].
Check C_xor 0 0.

Fixpoint sum (l : list bool) :=
match l with
| [] => 0
| true::xs => S (sum xs)
| false::xs => sum xs
end.

Example eq_to_eqb (a b : bool) : (a == b) = eqb a b := sorry.

Example length_step {Q : Type} (x : Q) xs : length (x :: xs) = S (length xs) := erefl.
Example sum_nil : sum [] = 0 := erefl.
Example sum_step x xs : sum (x::xs) = if x then S (sum xs) else sum xs := erefl.
Example sum_step_2 x xs : 
(x ==> (sum (x::xs) == S (sum xs))) && (~~x ==> (sum (x::xs) == sum xs)).
Proof.
case: (bool_cases x) => H; rewrite H.
all: simpl. by rewrite andb_true_r. by [].
Qed.

Example sum_one_is_and p (h: sum [p] = 1): and_lst [p].
Proof.
rewrite /and_lst /bfold andb_true_r.
revert h; case p; intro h; first trivial.
rewrite sum_step sum_nil in h. discriminate.
Qed.

Example le_succ m n : m ≤ n → S m ≤ S n := sorry.

Example sum_le_len xs : sum xs ≤ length xs.
Proof.
induction xs as [|x xs ih].
simpl; trivial; simpl.
case x.
exact (le_succ ih).
exact (Nat.le_trans _ _ _ ih (Nat.le_succ_diag_r (length xs) : length xs ≤ (length xs).+1)).
Qed.

Example sum_two_is_and p q (h: sum [p; q] = 2): and_lst [p; q].
Proof.
rewrite and_lst_step.
revert h; case p; intro h.
rewrite sum_step in h.
rewrite -nat_step in h.
exact (sum_one_is_and h).
rewrite sum_step in h.
assert (H := sum_le_len [q]).
rewrite h in H; simpl in H.
exact (absurd _ H (Nat.nle_succ_diag_l 1)).
Qed.

Example sum_n_is_and (xs: list bool) (h: sum xs = length xs): and_lst xs.
Proof.
induction xs as [|x xs ih].
by rewrite /and_lst /bfold.
revert h ih.
case x; intros h ih.
all: rewrite and_lst_step.
all: rewrite sum_step length_step in h.
exact (ih (eq_add_S _ _ h)).
assert (H := sum_le_len xs).
rewrite h in H.
assert (notH := Nat.nle_succ_diag_l (length xs)).
exact (absurd _ H notH).
Show Proof.
Qed.

Example true_eq_refl {Q : Type} (n : Q) : True = (n = n) := sorry.

Goal sum [] = length ([] : list bool) → and_lst [].
Proof.
exact (@sum_n_is_and []).
Undo.
simpl.
rewrite /and_lst /bfold.
exact (fun _ => is_true_true).
Qed.

(** 
Prove that orb x y = orb y x
if x = true
  `orb true y = orb y true` can be proved using orb_true_l and orb_true_r.
elif x = false
  `orb false y = orb y false` can be proved using orb_false_l and orb_false_r.
*)

Example orb_comm' x y : x || y = y || x.
Proof.
refine (if x then _ else _).
rewrite orb_true_l orb_true_r //.
rewrite orb_false_l orb_false_r //.
Qed.



















