Require Import PeanoNat.
Require Export Bool.
Require Import Setoid.
Require Import Lia.
From Coq Require Import ssreflect ssrfun ssrbool.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Section Ex1.
Variable A : Type.
Variable R : A -> A -> Prop.

Definition reflexive (r : A -> A -> Prop) : Prop := forall (x: A), r x x.
Definition loopless (r : A -> A -> Prop) : Prop := forall (x: A), ~ r x x.
Definition symmetric (r : A -> A -> Prop) : Prop := forall (x y: A), r x y <-> r y x.

Definition transitive3 (r : A -> A -> Prop) : Prop :=
forall (x y z: A), r x y -> r y z -> r x z.

Definition transitive4 (r : A -> A -> Prop) : Prop :=
forall (x y z w: A), r x y -> r y z -> r z w -> r x w.

Definition transitive5 (r : A -> A -> Prop) : Prop := 
forall (x y z w v: A), r x y -> r y z -> r z w -> r w v -> r x v.

Lemma t3_imp_t4: transitive3 R -> transitive4 R.
Proof.
unfold transitive3, transitive4.
eauto.
Undo.
move=> Ht3 x y z w Hxy Hyz Hzw.
eapply Ht3.
eapply Hxy. (* R x ? unifies only with Hxy i.e. in that state only one possible apply *)
eapply Ht3. 
eapply Hyz. (* R y ? unifies only with Hyz *)
exact Hzw.
Qed.

Lemma t3_imp_t5: transitive3 R -> transitive5 R.
Proof.
unfold transitive3, transitive5.
eauto.
Qed.

(* t4 -> t5 can't be proved, interesting what a premise(additional hypothesis) we need to 
be able to prove this? Answer: reflexive R.
Can we make premise weaker? Like exists some one reflexive element R v v or something like that? *)
Lemma t4_imp_t5: transitive4 R -> transitive5 R.
Proof.
unfold transitive4, transitive5.
intuition.
eapply H.
eapply H0.
eapply H1.
eapply H.
eapply H2.
eapply H3.
(* we need to prove "R v v" i.e. we need "reflexive R" as precondition *)
Abort.

Lemma refl_t4_imp_t5: reflexive R -> transitive4 R -> transitive5 R.
Proof.
unfold reflexive, transitive4, transitive5.
eauto.
Qed.


Definition connected_only_by_intermediate x y := ~ R x y /\ (exists t, R x t /\ R t y).


Parameter (connected : A -> A -> Prop).

Axiom connected_def: forall x y, (R x y \/ exists t, x <> t /\ R x t /\ connected t y) -> connected x y.
Axiom connected_def_2: forall x y, (x = y \/ R x y \/ exists t, connected x t /\ connected t y) -> connected x y.
Axiom connected_decidable: forall x y, connected x y \/ ~ connected x y.

(* Lemma conn_ : connected .
Proof.

Qed. *)

Lemma linked_imp_connected x y : R x y -> connected x y.
Proof.
move => Hrxy.
eapply (@connected_def x y).
left.
exact: Hrxy.
Qed.

Lemma linked_imp_connected_2 x y : R x y -> connected x y.
Proof.
move => Hrxy.
eapply (@connected_def_2 x y).
right; left.
exact: Hrxy.
Qed.

Lemma trans_of_connected : transitive3 connected.
Proof.
unfold transitive3.
move => x y z Hxy Hyz.
have H := connected_def_2.
apply H.
right; right.
exists y.
exact: conj Hxy Hyz.
Qed.

Lemma l1 (P : A -> Prop): (forall x : A, ~ P x) <-> (~ exists x : A, P x).
Proof.
constructor; intro H.
intro Hex.
case Hex.
intros x Hp.
have Hp_false := H x.
contradiction.
move => x Hpx.
case: H.
exists x.
exact Hpx.
Qed.


Lemma l2 (P : A -> Prop): (forall x : A, P x) <-> (~ exists x : A, ~ P x).
Proof.
constructor.
+ by firstorder.
+ Fail by firstorder.
Abort.

Lemma all_2_ex (P  Q: A -> Prop) : (forall x, P x -> Q x) -> (exists t, P t) -> (exists t, Q t).
Proof. firstorder.  (* move => hPimpQ [] x hp; exists x. apply: hPimpQ hp. *) Qed.

Lemma by_intermediate_is_connected x y: connected_only_by_intermediate x y -> connected x y.
Proof.
move=> [_ HexRxtty].
apply connected_def_2; right; right.
eapply all_2_ex.
2: apply HexRxtty.
move=> t [Hxt Hty].
split.
all: apply linked_imp_connected; assumption.
Qed.

(**
connected on R is equivalent to transitive_closure(R)

Lemma 1.
Given two paths: 
PathX: x1 - x2 - x3 ...
PathY: y1 - y2 - y3 ...

if exists (Vx, Vy) such that Vx in PathX /\ Vy in PathY
  then forall x in PathX, y in PathY, connected x y.


Lemma 2.
~ conn x y -> ~ R x y
connected x y <-> conn_component_of x = conn_component_of y

Lemma 3. What if exists some central node `center`?
exists c, conn x c -> R x c ==> ?
  Goal 1: conn x c <-> R x c
  Goal 2: all nodes in graph are connected to each other: forall x y, connected x y
  
Lemma existsing_shortest_path:
  if connected x y then exist shortest_path from x to y (length edge x y = 1)

*)

Lemma new_lemma : .
Proof.

Qed.

(* Definition connected := UNKNOWN, system must calculate it only from information in that file *)

(* Def can be expressed using some subset "LS" of lemmas for exampe gcd function:

```dafny
function gcd(x: nat, y: nat): nat
{
  if x == y then // Used lemma gcd n n = 1
    1
  else if x > y then // Used lemma gcd x y = gcd y x
    gcd(y, x)
  else
    gcd(x, y-x) // Used lemma x < y -> gcd x y = gcd x (y - x)
  
}
```

"LS" - lemma set set
LS have properties:
* LS enouth to calculate object deterministically, i.e. for example if we use only one lemma
  gcd n n = 1 then gcd 1 2  can have set of values Vs = {0, 1, 2, ...} but target is |Vs| = 1


How about beginning of creating defintion?
WE have in mind some links between objects, and relations, lemmas mainly not general or too general to be true.

Task: how to use that objects maximally?
*)


(**
Usually we write tests for some code then write definition for operation
but in Coq, we firstly might write not only tests.. we can write lemmas!
It's huge advantage of that process.

How to solve it's a like equation:
If we have some set of lemmas that use object C (for example connected relation) how to automatically bruteforce all possible correct definition for that C?

If even we don't know definition for some object C, this not means that we know anything.
We can create:
* lemmas
  Each lemma is step in some "space" of actions
* constraints on lemmas (in coq it's possible, but really hard, it's like implement coq in coq)
* 
*)



Lemma test : reflexive R -> reflexive R.
Proof.


Qed.

(**
# Graphs isomorphic test:
* count(Edges A) = count(Edges B)
* multiset(deg A) = multiset(deg B)
  where deg A = [v1: deg=5, v2: deg=7, ...] is a list of vertex degrees
* ...


=== GRAPHS BEGIN ===
# Simple graph definition:
* no loops: R x x = False
* edge = two directional link: R x y -> R y x
* connected v1 v2 if exists some path from e1 to e2
  Path_exists v1 v2 = if exists v3 that v1 -- v3 and Path_exists v3 v2
    from transitivity of connected: forall v1 v2 v3, 
      connected v1 v2 -> connected v2 v3 -> connected v1 v3
      <=>
      exists_path v1 v2 -> exists_path v2 v3 -> exists_path v1 v3



=== GRAPHS END ===


# Saving property values (invariants):
* (saves 100%) Identity save all properties
* (saves <100%) Permutation of vertices save |V|, |E|, if before isomorphic(A, B) ==> after isomorphic(A, B)
* ...
* ... some function saves some subset of properties ...
* exists or not such arbitrary function that saves 0 % of properties?
  Note: "minimal" property P is a property that return boolean value and
  exists some graph fG that P(fG) = false
  /\ exists some graph tG that P(tG) = true


# Search for theorems
* Search using concrete emamples. 
  How to implement: implement type Relation and operations on it
* Search using "inductive states" (links between some general constructions...)
  How to implement: implement some type of inductive step Ind_Relation and operations on it
  + It corresponds to arbitrary schemes in notebook, where humans writes some really simple views of problems
  Scheme is just map or gluing of some states(set of subtasks, subgoals)
  Scheme can be equation like fib(n) = fib(n-1) + fib(n-2)
  But in general we don't know some "silver bullet" to solve any problem uncrementally
  

*)



