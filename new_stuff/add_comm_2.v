Lemma add_comm m n: m + n = n + m.
Proof.
elim m.
simpl.
rewrite <- plus_n_O.
trivial.
intros k ih.
simpl.
rewrite ih.
trivial.
Qed.

Lemma add_assoc m n k : (m + n) + k = m + (n + k).
Proof.
elim m; trivial.
intros u ih.
f_equal.
exact ih.
Qed.

Lemma ex (P Q: Prop): P -> Q -> P /\ Q.
Proof.
intros hp hq.
split.
Show Proof.
Show Existentials.
Qed.



Lemma add_assoc' m n k : (m + n) + k = m + (n + k).
Proof.
case ().
f_equal.
exact ih.
Qed.




