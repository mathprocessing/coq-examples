Require Import ZArith.
Require Export Bool.
Require Import Setoid.
Require Import Lia.
From Coq Require Import ssreflect ssrfun ssrbool.
Unset Printing Implicit Defensive.

(**
Z.pow_0_l':         forall     a : Z, a <> 0 -> 0 ^ a = 0
Z.pow_1_l:          forall     a : Z, 0 <= a -> 1 ^ a = 1

Z.pow_0_r:          forall     n : Z, n ^ 0 = 1
Z.pow_1_r:          forall     a : Z, a ^ 1 = a
Z.pow_2_r:          forall     a : Z, a ^ 2 = a * a

Z.pow_succ_r:       forall   n m : Z, 0 <= m -> n ^ Z.succ m = n * n ^ m
Z.pow_neg_r:        forall   n m : Z,  m < 0 -> n ^ m = 0

Z.pow_eq_0:         forall   a b : Z, 0 <= b -> a ^ b = 0 -> a = 0
Z.pow_nonzero:      forall   a b : Z, a <> 0 ->    0 <= b -> a ^ b <> 0

Z.pow_twice_r:      forall   a b : Z, a ^ (2 * b) = a ^ b * a ^ b 

Z.pow_opp_even:     forall   a b : Z, Z.Even b -> (- a) ^ b =   a ^ b
Z.pow_opp_odd:      forall   a b : Z,  Z.Odd b -> (- a) ^ b = - a ^ b

Z.pow_even_abs:     forall   a b : Z, Z.Even b -> a ^ b = Z.abs a ^ b
Z.pow_odd_abs_sgn:  forall   a b : Z,  Z.Odd b -> a ^ b = Z.sgn a * Z.abs a ^ b

Zpower_nat_Zpower:  forall   z n : Z, 0 <= n -> z ^ n = Zpower_nat z (Z.abs_nat n)

Z.pow_mul_l:        forall a b c : Z, (a * b) ^ c = a ^ c * b ^ c
*)
Check Z.pow_0_l' 42.
Open Scope nat.

Section Ex.
Variable (f: Z -> Z).
Check Z.of_nat 7.

Lemma pow_0_l_nat (a: nat): a > 0 -> 0 ^ a = 0.
Proof.
Admitted.

End Ex.

Open Scope Z_scope.

Lemma pow_0_l a: a <> 0 -> 0 ^ a = 0.
Proof.
elim a. lia.
all: move => p _.
have Hpos : 0 < Z.pos p := Pos2Z.pos_is_pos p.
Search positive.
Check @pow_0_l_nat p.


Admitted.

