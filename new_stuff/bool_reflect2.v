Require Export Bool.
Require Import Setoid.
Require Import Lia.
Require Import BinInt BinNat PeanoNat.
Require Import ssreflect ssrbool.
Require Import Hammer.Tactics.Reconstr.

Open Scope Z_scope.

Section Example1.

Lemma e1 a b: a = 1 -> b = a -> a + b = b + a.
Proof.
docrush.
Qed.


Lemma e2 a b: a = b -> a <-> true -> b <-> true.
Proof.
docrush.
Qed.


Lemma e3 a b: a <-> b -> b <-> a.
Proof.
docrush.
Qed.

Lemma e4 a: 0 < a \/ a <= 0.
Proof.
Fail now docrush.
lia.
Qed.

Lemma e5 a b: a + b * 2 = b + a + b.
Proof.
Fail now docrush.
Fail now qrrscrush.
Fail now qrrsimple.
lia.
Qed.

Lemma e6 a b : eqb a b -> (a || b <-> a).
Proof.
now docrush.
Undo.
now qrrscrush.
Undo.
Fail qrreasy.
now qrrsimple.
Qed.

Lemma e7 : exists a, a = true.
Proof.
now qrrsimple.
Qed.

Lemma e8 : exists a b, (a && b) <> b.
Proof.
Fail qrrsimple.
exists false, true.
simpl.
discriminate.
Qed.


Lemma e8' : exists a b, (a && b) <> b.
Proof.
by [exists false, true].
Qed.

Definition abs (n : Z) :=
match n with
| Z.neg m => Z.pos m
| m => m
end.


Lemma e9 a: Z.abs(a) = 0 -> a = 0.
Proof.
lia.
Undo.
qrrsimple.
Qed.


Lemma abs_max n: Z.abs n = Z.max n (- n).
Proof.
(* case n; first by [].
all: move=> p.
all: simpl. *)
have H : n < 0 \/ 0 <= n by lia.
case: H => h.
replace (Z.max n (-n)) with (-n).
  apply (Z.abs_neq_iff n).
  have hle : n < 0 -> n <= 0 by lia.
  exact: hle h.
have H : n <= -n by lia.
rewrite Z.max_r; first exact: H.
reflexivity.
rewrite (Z.abs_eq n).
exact h.
rewrite Z.max_l.
apply: Z.le_trans.
2: exact h.
have H : -n <= 0 by lia.
exact H.
reflexivity.
(* 
Just `lia.`
means we can make hacks and our
proof don't have same depth in all cases, 
that's why only possible application of lia with have tactic:
`have H : statement by lia.`

Or possible to implement some tactic like
simp using only [le_trivial, bool_trivial]
And this tactic ensures that depth also != 0
i.e. 
Proof.
Fail simp using only [le_trivial, bool_trivial]
Abort.
 *)
Qed.

(**
|- |n| = max(n, -n)

|- n < 0 -> |n| = max(n, -n) /\
   n >= 0 -> |n| = max(n, -n)
   
|- n < 0 -> -n = max(n, -n) /\
   n >= 0 -> n = max(n, -n) /\
   
n < 0 => -n > 0 => n < 0 < -n => n < -n
=> max_le
|- n < 0 -> -n = max(n, -n) /\
   n >= 0 -> n = max(n, -n) /\
*)

Check Z.max_le.


End Example1.





















