From Coq Require Import ssreflect ssrfun ssrbool.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Inductive Nat: Set:=
| O : Nat
| S : Nat -> Nat.

Check O.
Check S.

Ltac forward_gen H tac :=
  match type of H with
  | ?X -> _ => let H' := fresh in assert (H':X) ; [tac|specialize (H H'); clear H']
  end.
  
Tactic Notation "forward" constr(H) "by" tactic(tac) := forward_gen H tac.

(* assert (H := ...). works like have H := ... *)

(*
remember (iter_pos _ _ _) as shr_p0m1.
How it works?

Goal:
  ... match (emax - e0 + 1)%Z with ...

remember (emax - e0 + 1)%Z as z'.

Creates this hyp:
Heqz' : z' = (emax - e0 + 1)%Z
*)




Section ex.
Variables a b: nat.
Goal a * a - b * b = (a - b) * (a + b).
Proof.
replace ((a - b) * (a + b)) with ((a - b) * a + (a - b) * b).
{
  assert (H : forall x y z, (x - y) * z = (x * z - y * z)). admit.
  Check H a b a.
  rewrite (H a b a) (H a b b).
  assert (H2 : forall x y z, x - y = x - z -> y = z). admit.
  Check H2 (a * a) (b * b) (b * a + (a * b - b * b)).
  admit.
}
Abort.
End ex.

Ltac refl := match goal with 
| _ => f_equal; reflexivity
| [ |- ?T1 = ?T2 ] => fail "refl tactic failed: maybe"T1"not equal to"T2"by definition"
end.

Ltac do_nat n tac :=
  match n with
    | O => idtac
    | S ?n' => tac ; do_nat n' tac
  end.


Ltac rw H := rewrite H.
(* Ltac have_tac name prf := assert (name: _) by exact prf. *)

(* Tactic Notation "have" ident(name) ":" constr(prf) :=
have_tac name prf.
 *)


Fixpoint add (m n: Nat) {struct n}: Nat :=
match n with
| O   => m
| S n => S (add m n)
end.

Check Some 0.
Check None : option Nat.

Definition prev (n : Nat) : option Nat :=
match n with
| O    => None
| S n  => Some n
end.

(* Example of syntax:
Notation "x < y" := (lt x y) (at level 70). *)
Local Notation "0" := (O).
Local Notation "1" := (S 0).
Local Notation "2" := (S 1).
Local Notation "3" := (S 2).
Local Notation "4" := (S 3).
Local Notation "5" := (S 4).
Local Notation "6" := (S 5).
Local Notation "7" := (S 6).
Local Notation "8" := (S 7).
Local Notation "9" := (S 8).
(* Local Notation "a +++ b" := (add a b) (at level 50). *)

Check 1= 1.
(* Check 1 =1. Fails with message: Syntax error: [constr:operconstr] expected after '=1' (in [constr:operconstr]).
Try add to notation of "1" `(at level 70)` don't helps.
*)

Section Prev.

Lemma prev_zero: prev O = None.
Proof eq_refl.

Lemma prev_succ n: prev (S n) = Some n.
Proof eq_refl.

Definition S' (nopt: option Nat): option Nat :=
match nopt with
| Some n => Some (S n)
| None   => None
end.

(* Lemma succ_prev n: S' (prev (Some n)) = . *)

Print S'.
Lemma S'_some n: Some (S n) = S' (Some n).
Proof eq_refl.

End Prev.


Section BasicAdd.

(* Just for filling that definition is right *)
Example add_two_two : add 2 2 = 4 := eq_refl.

(* Lemmas from definition of add *)
Lemma add_zero m: add m 0 = m.
Proof ltac:(exact eq_refl).

Lemma add_succ_right m n: add m (S n) = S (add m n).
Proof eq_refl.

End BasicAdd.

Lemma zero_add n: add 0 n = n.
Proof.
  simple induction 0.
  (* exact (add_zero 0). without ssreflect we need brackets *)
  exact: add_zero 0.
  intros k ih.
  rw add_succ_right.
  rw ih.
  refl.
Qed.

Lemma n_neq_succ: forall n, n <> (S n).
Fail discriminate.
Admitted.

Lemma zero_neq_one: 0 <> 1.
Proof ltac:(discriminate).

Lemma zero_neq_one': 0 <> 1.
Proof @n_neq_succ 0. (* Why we need @? *)


Lemma add_assoc m n k: add (add m n) k = add m (add n k).
Proof.
simple induction 0.
+ now rw add_zero.
+ intros N ih.
  do 3 rw add_succ_right.
  f_equal.
  exact ih.
Qed.

(* Base case for add_succ_sym *)
Lemma one_add n: add 1 n = S n.
Proof.
(* Try to prove without induction, i.e. proof must use zero_add: add 0 n = n  *)
 (* try add (S n) 0 fails *)
(* replace (S n) with (add 0 (S n)).
+ rw add_succ_right. ALSO FAILS*)
(* isomorphism detected: backward step add_succ_right can be applied to left part of equaltion like it's applied to a right part of equation *)
(*
But isomorphism not helps because:
add 1 S(n) = ...
S (add 1 n) = ...
add 1 n = ...
can't help
*)
(* If we draw 2d graph of function add we can see that exist only two ways to prove: 
1. Prove add_succ_left
2. Prove this by induction
3. Maybe using `add_assoc` because assoc helps in proving `add_succ_sym`

How to prove that 3ed way not exist formally (without drawing graph)?
*)

(** Check 1st way: *)
have H : forall m k, add (S m) k = S (add m k) by admit.
Check H 0 n : add 1 n = S (add 0 n).
rw (H 0 n).
f_equal.
exact: zero_add n.
(* Done *)
Undo. Undo. Undo. Undo.

(** Check 2nd way: *)
induction n; trivial.
rw add_succ_right; f_equal.
exact IHn.
Qed.

(* Intermediate step for add_succ_left proof by induction *)
Lemma add_succ_sym m n: add (S m) n = add m (S n).
Proof.
induction m.
+
  rw zero_add; apply one_add.
+
  rw add_succ_right.
  rw IHm.
  (* Proof using add_succ_left *)
  have H_add_succ_left : forall m n, add (S m) n = S (add m n) by admit.
  rw H_add_succ_left.
  f_equal.
  exact IHm. (* Done *)
  Undo. Undo. Undo. Undo. Undo. Undo.
  
(* Proof without add_succ_left *)
rw add_succ_right.
(* replace (add (S (S m)) n) with (add (S (add 1 m)) n). (* Branch FAILS  *) *)
replace (add (S (S m)) n) with (add (add 1 (S m)) n).
rw add_assoc.
rw one_add.
refl.
f_equal.
now rw one_add.
Qed.

Lemma add_succ_left m n: add (S m) n = S (add m n).
Proof.
induction n.
now do 2 rw add_zero.
rw add_succ_right.
f_equal.
  exact: add_succ_sym m n.
  Undo.
rw IHn.
now rw add_succ_right.
Qed.

Theorem add_comm m n: add m n = add n m.
Proof.
induction m.
(* Base case *)
Check zero_add n : add 0 n = n.
now rw zero_add.
(* Induction case/step *)
Check add_succ_right m n : add m (S n) = S (_).
rw add_succ_right.
rewrite <- IHm. (* IHm is Induction hypothesis *)
exact: add_succ_left m n. (* No more subgoals. *)
Undo. Undo.
(* Possible demo of interfaces: first we create interface(API). and after Lemma add_m_Sn  *)
have H: add (S m) n = S (add m n) := add_succ_left m n.
rewrite H.
  congruence. (* No more subgoals. *)
Undo.
  f_equal.
  exact IHm.
Qed.

Section Mul_operation.

Fixpoint mul (m n: Nat) {struct n}: Nat :=
match n with
| O   => O (* zero *)
| S n => add m (mul m n)
end.

Goal mul 2 3 = 6. Proof eq_refl.
Goal mul 3 2 = 6. Proof eq_refl.
Goal mul 0 0 = 0. Proof eq_refl.

Section Basic.
Lemma mul_zero m: mul m 0 = 0.
Proof eq_refl.
Lemma mul_m_Sn m n: mul m (S n) = add m (mul m n).
Proof eq_refl.
End Basic.

Lemma zero_mul n: mul 0 n = 0.
Proof.
induction n; trivial.
rw mul_m_Sn.
now rw IHn.
Qed.

Lemma one_mul n: mul 1 n = n. Admitted.

Lemma mul_Sm_n m n: mul (S m) n = add n (mul m n).
Proof.
induction m.
+ 
  rw zero_mul.
  rw add_zero.
  apply one_mul.
+
  rw IHm.

Abort.

Lemma mul_Sm_n_sym m n: mul (S m) n = mul m (S n).
induction n.
rw mul_zero.



Fixpoint pow (m n: Nat) {struct n}: Nat :=
match n with
| O   => 1 (* one *)
| S n => mul m (pow m n)
end.

Definition forty_two := mul 6 7.

Goal pow 1 forty_two = 1. Proof eq_refl.
Goal pow 2 3 = 8. Proof eq_refl.
Goal pow 8 2 = mul 8 8. Proof eq_refl.
Goal pow forty_two 0 = 1. Proof eq_refl.

(**
hyper_pow(2, n + 1) = hyper_pow(2, n) ^ 2
hyper_pow(2, n - 1) = sqrt(hyper_pow(2, n))

hyper_pow(2, 0) = 2^(1/2) = sqrt(2)
hyper_pow(2, 1) = 2 = 2
hyper_pow(2, 2) = 2^2 = 4
hyper_pow(2, 3) = (2^2) ^ 2 = 16
hyper_pow(2, 4) = ((2^2) ^ 2) ^ 2 = 256
*)
(* Fixpoint hyper_pow (m n: Nat) {struct n}: option Nat :=
match n with
| O   => None (* ??? = m^(1/m) *)
| S O => Some 1
| S n => Some (pow (hyper_pow m n) m)
end. *)

(**
hyper_pow_right(2, n + 1) = 2 ^ hyper_pow_right(2, n)
hyper_pow_right(2, n + 2) = 2 ^ (2 ^ hyper_pow_right(2, n)) =
  = 2 ^ (hyper_pow_right(2, n) * hyper_pow_right(2, n))
  = 2 ^ hyper_pow_right(2, n) * 2 ^ hyper_pow_right(2, n)
  = (2 ^ hyper_pow_right(2, n)) ^ 2
  
hyper_pow_right(2, 1) = 2 ^ hyper_pow_right(2, 0)
if hyper_pow_right(2, 0) = 0 then
  hyper_pow_right(2, 1) = 2 ^ 0 = 1
  hyper_pow_right(2, 2) = 2 ^ hyper_pow_right(2, 1) = 2 ^ 1 = 2
*)
Check I.

(*hp = hyper_pow_right*)
Fixpoint hp (m n: Nat) {struct n}: Nat :=
match n with
| O   => O
| S n => pow m (hp m n)
end.

Goal hp 2 1 = 1. Proof eq_refl.
Goal hp 2 2 = 2. Proof eq_refl.
Goal hp 2 3 = 4. Proof eq_refl.
Goal hp 2 4 = pow 4 2. Proof eq_refl.
(* 16 -> pow 4 2 interesting thing: what if hex, bin, all stuff is wrong (not simplest way to represent numbers)?
From this idea we can derive:
Iteration 1: Some lemma is just a "view" of some instance of type.
Iteration 2: Some lemma is just a "bridge" between instances of type.
Iteration 3: Some lemma is just a "bridge system" between instances of type.
Iteration 4: Some lemma is just a "graph?" between instances of type.
Iteration 5: Some lemma is just a "link between views" of instance(s) of type.
...

Iteration N: Some lemma is just a OBJECT(N) of instance(s) of type. 
  where forall K: Nat | K < N :: !EQUIV(OBJECT(N), OBJECT(K))
        EQUIV("view", "bridge") = False
        EQUIV("view", "bridge system") = False
        EQUIV("bridge", "bridge system") = False
        ...
 *)
Goal hp 2 4 = add 8 8. Proof eq_refl.
Goal hp 3 0 = 0. Proof eq_refl.
Goal hp 3 1 = 1. Proof eq_refl.
Goal hp 3 2 = 3. Proof eq_refl.
Goal hp 3 3 = pow 3 3. Proof eq_refl.


Lemma hp_right_zero m: hp m 0 = 0.
Proof eq_refl.

Lemma hp_right_one m: hp m 1 = 1.
Proof eq_refl.

Lemma hp_right_two m: hp m 2 = m.
Proof eq_refl.

Lemma hp_right_three m: hp m 3 = pow m m.
Proof eq_refl.

Lemma hp_right_four m: hp m 4 = pow m (pow m m).
Proof eq_refl.

Lemma hp_right_five m: hp m 5 = pow m (pow m (pow m m)).
Proof eq_refl.

Goal hp 1 0 = 0. Proof hp_right_zero 1.
Goal hp 1 1 = 1. Proof hp_right_one 1.
Goal hp 1 2 = 1. Proof hp_right_two 1.
Goal hp 1 3 = 1. Proof hp_right_three 1.
Goal hp 1 4 = 1. Proof hp_right_four 1.
Goal hp 1 5 = 1. Proof hp_right_five 1.

Lemma hp_left_one n: hp 1 (S n) = 1.
Proof.
induction n.
exact: hp_right_one 1.
simpl.
simpl in IHn.
rw IHn.
refl.
Qed.

Goal hp 2 0 = 0. Proof hp_right_zero 2.
Goal hp 2 1 = 1. Proof hp_right_one 2.
Goal hp 2 2 = 2. Proof hp_right_two 2.
Goal hp 2 3 = pow 2 2. Proof hp_right_three 2.
Goal hp 2 4 = pow 2 (pow 2 2). Proof hp_right_four 2.

Lemma hp_left_two n: hp 2 (S n) = pow (hp 2 n) 2. Admitted.
(* hp 1 1) not equal to (pow 1 0)
 *)
Goal hp 1 1 = pow 1 0. Proof eq_refl.
 
Goal hp 0 0 = 0. Proof eq_refl.
Goal let N := hp 1 1 in N = 1 /\ N = pow 1 0.
Proof ltac:(split; reflexivity).

(* this pattern:
Goal let N := hp 2 2 in N = 2 /\ 2 = pow 2 (pow 2 0).
is bad because if we copy it we can create an error
Goal let N := hp 2 2 in N = 2 /\ 3 = pow 3 (pow 3 (pow 3 0)).  2 != 3 => Error
*)
Goal let N := hp 2 2 in N = 2 /\ N = pow 2 (pow 2 0).
Proof ltac:(split; reflexivity).

Goal let N := hp 3 3 in N = pow 3 3 /\ N = pow 3 (pow 3 (pow 3 0)).
Proof ltac:(split; reflexivity).

(* Goal hp 4 4 = pow 4 (pow 4 (pow 4 (pow 4 0))). Proof hp_right_four 4. *)

Check hp_right_five 5. (* hp 5 5 = pow 5 (pow 5 5) *)
(* Why this Check perfrom some redundant comutation: 
Check hp_right_five 5 : hp 5 5 = pow 5 (pow 5 5).
This can be optimized if we just check that input of `Check hp_right_five 5.`:string is the same
`hp 5 5 = pow 5 (pow 5 5)`:string
or this two strings can be expressions... no matter.
*)

Inductive Le : Nat -> Nat -> Prop :=
| n n     : True
| m (S n) : Le m n.

Lemma mul 


End Mul_operation.

Goal add 0 0 = 0.
cbv delta.
cbv fix.
cbv beta.
cbv match.
refl.
Qed.

Goal add 3 1 = 4.
cbv.
Undo.
cbv delta.
cbv fix.
cbv beta.
cbv match.
cbv beta.
cbv fix.
cbv beta.
cbv match.
guard .



Definition f : Nat -> Nat :=
| 0 := 0
| _ := 1
.


Definition mul : Nat -> Nat -> Nat :=
| m  0   => m
| m  S n => add (mul m n) n
.



Goal add 0 1 = 1.
Proof.
(*   have h1 : (conj (add_step 0 0) (add_zero 0) : add 0 1 = S (add 0 0) /\ add 0 0 = 0). *)
  have h1 : 1 = 1.
  destruct h1.
  revert H.
  rw H0.
  exact id.
  Undo. Undo. Undo. Undo.
  rw h1.1.
  rw h1.2.
  refl.
Qed.

Check (n_neq_succ O).
have H : (n_neq_succ).

Example test_refl k: add k O = add (add k O) 1.
Proof.
  Fail refl.
Abort.

Example f_eq k: add k O = add (add k O) O.
Proof.
  have H : add_zero.
  generalize (add k 0).
  refl.
Qed.

(* Ideas about automating rewrite *)

Goal add 1 1 = 2.
Proof.
have ->: _ := add_step.
refl.
Qed.

Goal add 1 1 = 2.
Proof.
(* have h1 : _ := add_step 0 0. *)
(* List of possible actions {*)
have H1 : add 0 1 = 1 := zero_add 1.
have H2 : add 1 0 = 1 := add_zero 1.
(* } *)
(* Try to eliminate some subset of actions in prev state by
adding rules in current state *)
(** apply all set of lemmas (by rw) to H1 and H2 in all combinations **)

have Hr : add 0 1 = add 1 0 by admit.

idtac.
have H2' : add 1 0 = 1 by (erewrite Hr in H1).
Undo.
Fail have H2' : add 1 0 = _ by (erewrite Hr in H1).
Fail have H2' : add 1 _ = 1 by (erewrite Hr in H1).
Fail have H2' : add _ 0 = 1 by (erewrite Hr in H1).
Fail have H2' : add _ _ = 1 by (erewrite Hr in H1).
Fail have H2' : add _ _ = _ by (erewrite Hr in H1).
Fail have H2' : _ = _ by (erewrite Hr in H1).
(** How to write similar tactic to rewrite but which not fails in previous cases? 
+ Idea if syntax for example:
Fail have H2' : add _ 0 = 1 by ([erewrite || rewrite] Hr in H1).
Proves that erewrite and rewrite fails in that case.
Logically : use erewrite || use rewrite ==> False
**)

clear Hr.
have H3 := congr1 S H1.
have H4 : add 1 1 = 2 by erewrite add_step in H3.
exact H4.
Abort.




























