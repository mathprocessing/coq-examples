Require Import ZArith Psatz.
Open Scope Z_scope.

Lemma zz : forall z, ~z < z.
Proof ltac:(lia).

Lemma e2 a b: a < b -> a = b -> False.
Proof.
intros hl he.
rewrite he in hl.
eapply zz.
exact hl.
Show Proof.
Qed.

Goal forall a b:Z, 2 * a * b = 30 -> 
(a + b) ^ 2 = a ^ 2 + b ^ 2 + 30.
intros a b H.
ring [H].
Qed.

Goal forall a b:Z, 
(a = 0 \/ a = 1) /\ b >= 0 ->
(a + b) ^ 2 >= b ^ 2.
intros a b H.
destruct H.
lia.
rewrite H.
assert (Hz:forall z, z >= 0 \/ z < 0) by lia.
case (Hz b); intro h.
lia.
contradiction.
Qed.

Goal forall b:Z, b >= 0 -> b < 0 -> False.
intros b habove hbelow.
contradiction.
Show Proof.

(* h : b < 0
______________________________________(1/1)
(1 + b) ^ 2 >= b ^ 2 *)

Goal forall (b:Z) (h : b < 0), 
(1 + b) ^ 2 < b ^ 2.
lia.
Qed.


Lemma ex1 x: -x^2 >= 0 -> x - 1 >= 0 -> False.
Proof.
ring.
Qed.
