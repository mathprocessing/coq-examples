From Coq Require Import ssreflect ssrfun ssrbool.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


(*https://plato.stanford.edu/entries/logic-substructural/
1.2 Commutativity

implication commutativity proof:

|- (p -> q) -> p -> q
--------------------
intro hpq
p -> q |- p -> q
--------------------
intro hp
p -> q, p |-  q
--------------------
swap hyps (or do nothing in Coq)
p, p -> q |-  q
--------------------
revert hpq
p |- (p -> q) -> q
--------------------
revert hp
|- p -> (p -> q) -> q
--------------------
*)

Example imp_comm (P Q: Prop):
(P -> Q) -> (P -> Q).
intro hpq.
intro hp.
(* swap hyps <=> idtac.*)
revert hpq.
revert hp. 
(* |- P -> (P -> Q) -> Q *)
Abort.

Section ExProp.
Variables P Q R : Prop.

Goal P -> (P -> Q) -> Q.
Proof.
intros hp hpq.
apply hpq.
exact hp.
Qed.

Goal ~Q -> (P -> Q) -> ~P.
Proof.
intros hnq hpq.
intro hp.
apply hnq.
assert (hq := hpq hp).
exact hq.
Qed.

Goal (P \/ Q) -> ((P -> R) -> (Q -> R) -> R).
Proof.
case; intros h hpr hqr.
1: apply hpr.
2: apply hqr.
all: exact h.
Qed.

Example modus P Q: P -> (P -> Q) -> Q :=
fun a b => b a.

Example imp_trans : (P -> Q) -> (Q -> R) -> P -> R :=
fun a b c => b (a c).

Example p_not_2 : (P -> Q) -> (P -> (Q -> False)) -> (P -> False) :=
fun a b c => (b c) (a c).

Example p_p_not_false : P -> (P -> False) -> False :=
fun a b => b a.

Example p_p_not_false' : P -> (P -> False) -> False :=
@modus P False.

Example p_not_p_false : (P -> False) -> P -> False := id.

Example false_x: False -> P := False_ind P.
Example x_true: P -> True := fun _ => I.

End ExProp.
Lemma decidable (P : Prop): P \/ ~P. Admitted. 

Example ex1 (P : Prop): ~(~P) \/ P -> P.
Fail tauto.
Abort.

Example ex2 (P : Prop) : ~(~P) \/ P -> P.
case (decidable P); intro H; tauto.
Qed.

Example ex3 (P : Prop) :  ~(~P) \/ P -> P.
case (decidable P); intro H.
intro; assumption.
intro Hnnp_or_p.
case Hnnp_or_p.
2: exact id.
change (~(P -> False) -> P).
intro Hnnp.
exfalso.
apply Hnnp.
exact H.
Qed.

Check eq_add_S.
Print eq_add_S.
Print not_eq_S.

Goal forall n m : nat, S n = S m -> n = m.
intros n m.
Abort.

Example x_ff_is_nx (P: Prop): (P -> False) <-> ~P.
Proof.
apply iff_refl.
Undo.
constructor; exact id.
Qed.

Example tt_x_is_x (P: Prop): (True -> P) <-> P.
Proof.
constructor; trivial.
apply.
exact I.
Qed.

Goal forall P : Prop, (P -> False) -> (P -> False).
exact (fun _ => id). (* Of course we can prove it, but main idea is about rewriting propositions
in context of human "obviousness" *)
Undo.
intro P.
Check iff_to_and.
have H := x_ff_is_nx P.
Fail rewrite H. (* Annoing thing, I'm can't do obvious thing *)
Fail subst H.
Abort.

Example p_nnp (P : Prop) : P -> ~(~P).
Proof. intros hp hnp. apply (hnp hp). Qed.

(* Can be easily proved by flip function lambda: a b => b a *)
Example p_nnp_easily (P : Prop) : P -> ~(~P) := fun a b => b a.

(*But ~(~P) -> P can't be proved without assumption of decidability of P: P \/ ~P *)
Example nnp_p (P : Prop) : ~(~P) -> P.
Proof.
intros hnnp.
change ((P -> False) -> False) in hnnp.
have HP_decidable : P \/ ~P := decidable P.
case HP_decidable.
exact: id.
intro hnp.
exfalso.
apply hnnp.
exact hnp.
Qed.

Lemma decidable_3 (P: Prop): P \/ ~P \/ ~(~P). Admitted.

Example nnp_p_2 (P: Prop): ~(~P) -> P.
Proof.
intros hnnp.
change ((P -> False) -> False) in hnnp.
have H_dec3 := decidable_3 P.
case H_dec3.
exact: id.
case.
+
  intro hnp.
  exfalso.
  apply hnnp.
  exact hnp.
+
  (* Attempt to recursive using exact npp_p_2. *)
  admit.
Abort.



(*

H: P -> Q

|- Q => H P

|- P 
  case P = true
  |- true => trivial 
  case P = false
  H: false -> Q
  
  |- false
  
  H: true (do nothing)
  H2: Q = false
  |- false


*)

Example lt_zero_case a: 0 = a \/ 0 < a. Admitted.
Example lt_absurd : 
forall m n, m < n /\ n < m + 1 -> False.
Admitted.

Example rem_ex a b:
  a + b < 1 -> a + b = 0.
Proof.
remember (a + b) as z.
assert (H := lt_zero_case z).
case H; intro h.
now rewrite h.
intro h2.
exfalso.
exact: (@lt_absurd 0 z).
Qed.
