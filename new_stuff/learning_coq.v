Require Import add_comm.
(* https://github.com/ImperialCollegeLondon/real-number-game *)

(* Coq trick exploit: https://github.com/tchajed/coq-tricks/issues/18 *)

(* Using Next Obligation:
https://github.com/Yu-zh/rbgs/blob/e46df4ae20194b8e4438259b820b15062f79b11c/examples/Bigstep.v#L537

Interesting idea: we can disable default behaviour of Obligation:
`Local Obligation Tactic := idtac.`
*)



(* Goal selectors
all : tactic  Apply to all goals
! : tactic    Before we have only one goal
par : tactic  Parallel execution
*)
Example goal_selectors (P Q : Prop): P -> 1 = 1 /\ 2 = 2.
  intro hp.
  split. all : refl.
  Undo. Undo.
  split; refl.
  Undo.
  split.
  refl.
  ! : refl.
Qed.

Goal 1 = 1 /\ 2 = 2 /\ 3 = 3.
ltac:(match goal with
| |- ?a /\ ?b => idtac "left:"a"rest:" b
end).
ltac:(match goal with
| |- ?a /\ ?b => cut a
end).
2: refl.
intro h.
split.
exact h.
split.
all : congruence.
Undo.
apply congr1.
exact h.
repeat apply congr1.
refl.
Qed.

Goal not (4 = 5).
discriminate.
Qed.

Goal not (1 = 2).
discriminate.





Lemma gen_ex: add O (S n) = (S n).
  generalize (S n). intro k.
  reflexivity.
  
Goal True.
match constr:(fun x => (x + 1) * 3) with
| fun z => ?y * 3 => idtac "y =" y; pose (fun z: nat => y * 5)
end.
Abort.

Goal 3 = 5 /\ 2 = 2.
let n := numgoals in guard n = 1.
split.
match goal with
| |- ?a = ?a => refl
| |- O = S _ => fail "can't be proved"
| |- ?a = ?b => repeat apply congr1
end.
Abort.

(* You wrap no error tactic with solve[t]*)
Goal 1 = 0.
Proof.
  Fail solve [f_equal].
Abort.

Goal 1 = 1.
Proof.
  solve [f_equal].
Qed.

Lemma card_nat :
  ~ exists x y : nat, forall z:nat, x = z \/ y = z.
Proof.
  intros (x & y & Hz).
  destruct (Hz (Nat.zero)), (Hz (Nat.succ Nat.zero)), (Hz (Nat.succ (Nat.succ Nat.zero))).
  all: match goal with
       | _ : ?a = ?b, _ : ?a = ?c |- _ => assert (b = c) by now transitivity a
       end.
  all: discriminate.
Qed.

Lemma some (P Q : Prop) : ~P /\ Q -> P \/ Q.
Proof.
  intro Hpq.
  elim Hpq; do 2 intro.
  right; exact H0.
Qed.

(* Good prrof view*)
Lemma prime_mult :
  forall p:Z, prime p -> forall a b:Z, (p | a * b) -> (p | a) \/ (p | b).
Proof.
  intro p.
  simple induction 1. intros.
  case (Zdivide_dec p a). tauto.
  right.
  Check Gauss : forall a b c : Z,
       (a | b * c) -> rel_prime a b -> (a | c).
  eapply Gauss with a.
  exact (H2 : (p | a * b)).
  Check H1 a : 1 <= a < p -> rel_prime a p.
  Check prime_rel_prime : forall p : Z, prime p -> forall a : Z, ~ (p | a) -> rel_prime p a.
  (* Check can replace `assert (H : _) by ...` and make something like "interface of proof to outer world"
  And user incrementaly build that interface by million ways simultatniously in all "directions"
  System can reduce action space by proving some branches are equal_to/isomorphic_to/contains others.
  
   *)
  Check conj H n : prime p /\ ~ (p | a).
  apply (prime_rel_prime p H a n).
   (*
   Original really not easy to understand:
  intro p; simple induction 1; intros.
  case (Zdivide_dec p a); intuition.
  right; apply Gauss with a; auto with zarith.
  *)
Qed.

Lemma square_spec n : square n = n * n.
Proof.
(* When we proving (saving complete proof in library) we need to be sure that any action of deleting element in sequence
  tac1; tac2; tac3; ... tacn. -> delete seq[2] -> tac1; tac3; ... tacn.
  can't produce right proof sequence of tactics.
  
  For example you can try to delete `simpl` and every tactic in sequence below:
  *)
 destruct n; trivial; simpl; f_equal; apply Pos.square_spec.
Qed.

Lemma sqrt_neg n : n<0 -> sqrt n = 0.
Proof.
 destruct n; try reflexivity.
(*  goal : pos p < 0 -> sqrt (pos p) = 0 *)
Qed.


(* TACTIC EXAMPLES ------------------*)
(* see Coq/lib/coq/theories/ZArith/BinInt.v *)
Ltac swap_greater := rewrite ?gt_lt_iff in *; rewrite ?ge_le_iff in *.
(*  ------------------*)

Theorem quotrem_eq a b : let (q,r) := quotrem a b in a = q * b + r.
Proof.
 destruct a as [|a|a].
 now simpl.
 (* We can add function F that maps between case POS_A and case NEG_A, then we debug this by trying prove two goals simultanionly
 : i.e. by doing one action at for example goal POS_A, and F automatically fixes (changes or some props being revealed) during proving/debugging *)

 destruct a as [|a|a], b as [|b|b].
 
 simpl; trivial;

 destruct a as [|a|a], b as [|b|b]; simpl; trivial;
 generalize (N.pos_div_eucl_spec a (N.pos b)); case N.pos_div_eucl; trivial;
  intros q r;
  try change (neg a) with (-pos a);
  change (pos a) with (of_N (N.pos a)); intros ->; now destruct q, r.
Qed.

Example ex1 : True := I.

Check true.
Check false.
Check andb true true.

Lemma subnK : forall m n, n <= m -> m - n + n = m.
Proof.
move => m n le_n_m.

move: m le_n_m. 
(* 
move => ... change state (like intros)
move :  ... set to state new value
 *)
Abort.


Ltac mytac H := rewrite H.

Lemma test x y (H1 : x = y) (H2 : y = 3) : x + y = 6.
Proof.
do [mytac H2] in H1 *.
Abort.