Require Import PeanoNat.
Require Export Bool.
Require Import Setoid.
Require Import Lia.
From Coq Require Import ssreflect ssrfun ssrbool.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Parameter f : nat -> nat.

Lemma le_SS m n k: m + k <= n -> m + S k <= S n.
Proof. lia. Qed.

Section s1.

Lemma add_pred m n: S m = S n -> m = n.
Proof ltac:(lia).

Lemma le_one_cases a: a <= 1 -> a = 0 \/ a = 1.
Proof ltac:(lia).

Lemma add_cases a b: a + b = 1 -> a = 1 /\ b = 0 \/ a = 0 /\ b = 1.
Proof.
move=> Hsum.
have le_one : a <= 1 /\ b <= 1 by lia.
case (le_one_cases le_one.1); intro H;
rewrite H.
right.
split; trivial.
rewrite H in Hsum.
exact: Hsum.
left.
split; trivial.
rewrite H in Hsum.
simpl in Hsum.
apply: add_pred Hsum.
Qed.

Hypothesis f_step: forall n, f n + f (S n) = 1.

Lemma f_le_one a: f a <= 1.
Proof.
have [H|H] := add_cases (f_step a);
rewrite H.1.
exact: le_n.
apply: le_S (le_n _).
Qed.

Lemma sum_3 n: f n + f (S n) + f (S (S n)) <= 2.
Proof.
have h0 := f_step n.
have h1 := f_step (S n).
rewrite h0.
case: (add_cases h1); intro H; rewrite H.2.
all: by [ apply: le_S | apply: le_n ].
Qed.

Lemma mul_f_2 n: f n * f (S n) = 0.
Proof.
have H := conj (f_le_one n) (f_le_one (S n)).
have H2 := f_step n.
(* Search (?a * 0 = 0). *)
Check Nat.mul_0_l.
Check Nat.mul_0_r.
have H3 := Nat.mul_0_l.
Fail lia.
have H4 : f n = 0 \/ f n = 1 by lia.
lia.

(* All minimal proofs have this property:
tac_n-1.
Fail lia.
tac_n.
lia.

i.e. main tactic (lia) must fails one step before
previous tactic prev_tac and succeeds at end of whole proof.

Additional:
Lemma: prev_tac != id_tac
*)
Qed.

Lemma mul_f_3 n: f n * f (S n) * f (S (S n)) = 0.
Proof.
rewrite mul_f_2.
apply: Nat.mul_0_l.
Qed.

(* Cannot guess decreasing argument of fix. *)
(* Fixpoint mul_range (g: nat -> nat) (lo hi: nat) :=
if Nat.eqb lo hi then
    1
  else
    (g lo) * @mul_range g (S lo) hi.
 *)

Fixpoint product (g: nat -> nat) (n: nat) :=
match n with
| 0   => 1
| S m => g (S m) * product g m
end.

Lemma product_def_zero : product f 0 = 1.
Proof eq_refl.

Lemma product_def_step n: product f (S n) = f (S n) * product f n.
Proof eq_refl.

Lemma product_f_one: product f 1 <= 1.
Proof.
simpl.
rewrite Nat.mul_1_r.
exact: f_le_one 1.
(*
What if (we make a mistake and) product f 1 <= 1 can simplified in general to 
`product f 1 = 1` or `product f 1 = 0`?

How to prove that we can't make that Lemma more
stronger?
i.e.
product f 1 = 1 is unprovable
and
product f 1 = 0 is also unprovable
*)
Qed.

Lemma product_f_two : product f 2 = 0.
Proof.
simpl.
rewrite Nat.mul_1_r Nat.mul_comm.
apply mul_f_2.
Qed.

Lemma product_f_three : product f 3 = 0.
Proof.
simpl.
rewrite Nat.mul_1_r.
rewrite Nat.mul_comm.
rewrite (@Nat.mul_comm (f 2) (f 1)).
by rewrite mul_f_2 Nat.mul_0_l.
Qed.

Lemma product_f_all' n: product f (S (S n)) = 0.
Proof.
elim n; first exact product_f_two.
move => m ih.
rewrite product_def_step.
rewrite ih.
now rewrite Nat.mul_0_r.
Qed.

Lemma product_f_all n: 2 <= n -> product f n = 0.
Proof.
elim n. 
+ (* Base case *)
  lia.
+ (* Induction case *) 
  move => m ih.
  simpl.
  have H: 2 <= m \/ m <= 1 by lia.
  case: H => hm.
  - (* Simple case (2 <= n) induction works ok *)
  apply ih in hm. clear ih.
  rewrite hm.
  rewrite Nat.mul_0_r.
  by move => _.
  - (* base case inside induction step (n <= 1)*)
  move => h1lem.
  have H_one_le_m : 1 <= m by lia. clear h1lem.
  have H_m_is_one: m = 1 := Nat.le_antisymm m 1 hm H_one_le_m.
  clear hm H_one_le_m ih.
  (* Now we must prove n = 1 => product f (S n) = 0 *)

  (* have H_product_zero : product f m <= k by lia. What is minimal k? *)
  (* Maybe product f m <= 1 is true? from forall n, f n <= 1. *)

  rewrite H_m_is_one.
  simpl.
  rewrite Nat.mul_1_r.
  rewrite Nat.mul_comm.
  exact (mul_f_2 1).
Qed.

Lemma product_f_le_one n: product f n <= 1.
Proof.
have H := f_le_one n.
have H_all := @product_f_all n.
have h_n_case : n <= 1 \/ 2 <= n by lia.
case: h_n_case => h.
+ clear H_all.
  move: H h.
  case: n. move => H h.
  rewrite product_def_zero.
  exact: le_n 1.
  move => n H h.
  apply le_S_n in h.
  apply Le.le_n_0_eq in h.
  apply eq_sym in h.
  rewrite h.
  exact product_f_one.
+ rewrite (H_all h). apply: le_S (le_n 0).
Qed.

End s1.

Lemma without_loss a b c (hnotzero : a > 0 /\ b > 0 /\ c > 0):
 a <= b <= c ->
a * b * c <= 2 -> a + b + c <= 4.
Proof.
move=> Hle_abc Hmul_abc.

have Hc_right_case : c > 2 -> False by lia.

clear Hc_right_case. (* we don't need this, just for
understanding that this branch can help to prove,
because `c` is a right bound of `a` and `b`. *)

have Hc: c = 1 \/ c = 2 by lia.

case: Hc => hc; rewrite hc.

rewrite hc in Hle_abc.
rewrite hc in Hmul_abc.
rewrite hc in hnotzero.
clear hc.

all: repeat ltac:(apply le_SS + rewrite Nat.add_0_r).
+ (* c = 1 |- a + b <= 3*)

   have H : b = 1.
   {
   (* We can clear part of context to ensure that 
    lia do not used hacks (other hyps, than user don't assumed when
    he do proof on paper)
    I think it's really almost all that user needs!
    Just a guarantee that some task can be solved using
    only some user-expected actions!
    
    i.e. proof-level-2 that some set actions enough to achieve
    task (create another proof-level-1).
    
    Also we can use more simpler idea:
    
    just calculate that minimal set of actions S
    (using Python for example) and after this prove
    (in Coq for example) that S is really minimal.
    
    Just crazy ideas:
    * Delegate is a group of actions (like `have`, `lia`, `clear`).
    * Between delegates can be some morphisms (or arrows).
    * We can prove things about pairs of delegates for example.
    *)
    clear Hmul_abc.
    lia.
   }
   (*
   have with clear and lia in body {} it's like lia with args where args is
   a set of possible lemmas.
   i.e. Like in simp[h1, h2, h3, ...] in Lean or Isabelle
   *)
   rewrite H.
   repeat ltac:(apply le_SS + rewrite Nat.add_0_r).
   Check Hle_abc.1 : a <= b.
   apply: Nat.le_trans.
   apply: Hle_abc.1.
   by [rewrite H; apply: le_S].
+ (* c = 2 |- a + b <= 2*)
   have H : a = 2 -> 2 <= b.
   {
    clear hc Hmul_abc hnotzero.
    have [Hab _] := Hle_abc. clear Hle_abc.
    lia. (* Main idea that lia is enough to prove that set of hyps is enough to
    achieve goal *)
    Undo. (* But we can play around with other tactics, also 
    we achieve 2 `rabbits` simultaniously:
    1. demonstrate that lia is works on particular goal
    2. create short proof (because lia usually create unreadable lambda proofs)
    Hint: You can use `Print lemma_name.` to see that kind of proof.
     *)
    move => hatwo.
    rewrite <- hatwo.
    exact Hab.
   }
   have H2 : a = 2 -> a + b <= 4 by lia. (* from H : a = 2 -> 2 <= b *)
   (* now we use another way of `grabbing` context not by hyps_names, but by
   their contents. Why i do this? Because it would be a great feature if system can
   spot this and convert to appropriate standard automatically
   (for example standard that uses only hyp names) *)
   have H3 : 2 <= b /\ b <= c /\ c = 2 -> b = 2 by lia.
   (* now user can use some `simp.` tactic to simplify H3 *)
   have H3_simplified : 2 <= b -> b = 2 by lia.
   apply H3_simplified in H.
   rewrite hc in Hmul_abc.
   have Hmul_2 : a * b <= 1.
   {
    clear H H2 H3 H3_simplified Hle_abc hnotzero hc.
    (* if we clear too much lia must fails: *)
    clear Hmul_abc. Fail lia.
    Undo. Undo.
    lia.
   }
   clear Hmul_abc.
   lia.
   (* Goal unprovable, because we make errors earlier *)
   admit.
Admitted.

Lemma mul_le_right lowx x n: lowx <= x -> lowx * n <= x * n.
Proof.
move => hx.
elim n; lia.
Qed.


Lemma le_mul_low lowx lowy x y:
lowx <= x -> lowy <= y -> lowx * lowy <= x * y.
Proof.
move=> hx hy.
(* Check @mul_le_right lowx x lowy _. *)
have H : lowx * lowy <= x * lowy := @mul_le_right lowx x lowy hx.
apply: Nat.le_trans.
exact H.
rewrite {1}Nat.mul_comm.
Fail rewrite {2}Nat.mul_comm. (* why? *)
rewrite (@Nat.mul_comm x y).
now apply mul_le_right.
Qed.

Lemma without_loss_2 a b c:
a <= b <= c ->
1 <= a * b * c <= 2 -> a + b + c <= 4.
Proof.
move => H_le_abc H_mul_abc.
(* from H_mul_abc we have a,b,c > 0*)
have H_gt_zero: 1 <= a /\ 1 <= b /\ 1 <= c.
{
  clear H_le_abc.
  lia.
}
(* 
* IF WE DON'T USE wlog (imaginary case): 
  if for any of two a or b or c is `x1 = x2 = 2` then a * b * c >= 4
  and we have contradiction with H_mul_abc : 1 <= a * b * c <= 2
* IF WE USE wlog (real case):
  remember that we also need a >= 1
  let b >= 2 then c >= 2 then a * b * c >= 4 ==> contradiction with H_mul_abc  
  *)
  have Hbletwo: b < 2. (* from H_le_abc.2 : b <= c and H_mul_abc *)
  {
    (* To make experience of using Undo. less frustrating.
    One (developer of Coq) can add labels:
    `Label_h_start.`
    and after we can call
    `Undo to Label_h_start.`
    *)
    clear H_gt_zero.
    have H2 :=  H_le_abc.2.
    clear H_le_abc.
    Fail lia. (* lia fails and help to user remember a > 0 hyp *)
    
    Undo.     Undo.     Undo.    Undo.
    have H_a_gt_zero := H_gt_zero.1.
    clear H_gt_zero.
    have H2 :=  H_le_abc.2.
    clear H_le_abc.
    Fail lia. (* why? *)
    (* Let's prove by contradiction *)
    have Hb_cases : b < 2 \/ 2 <= b by lia.
    case Hb_cases; try exact id.
    move=>H_two_le_b. exfalso. clear Hb_cases.
    (**
    H_mul_abc : 1 <= a * b * c <= 2
    H_a_gt_zero : 1 <= a
    H2 : b <= c
    H_two_le_b : 2 <= b

    2 <= b <= c  ---> H: 2 <= c

    4 <= b * c by (blow = clow = 2) blow <= b /\ clow <= c ==> blow * clow <= b * c

    from 4 <= b * c /\ 1 <= a ==> 1 * 4 <= a * (b * c)
    *)
    have Hbc: 4 <= b * c.
    {
      Fail lia.
      have Hc : 2 <= c by lia. 
      have Hlem := le_mul_low H_two_le_b Hc.
      exact Hlem.
    }
    lia.
  }
have Hb_one: b = 1. {
  have H_b_gt_zero := H_gt_zero.2.1. clear H_gt_zero.
  clear H_le_abc H_mul_abc.
  lia.
}
clear Hbletwo.
(* Now we know that Hb_one : b = 1 *)
rewrite Hb_one in H_le_abc.
rewrite Hb_one in H_mul_abc.
rewrite Hb_one.
rewrite -Nat.add_assoc.
rewrite (@Nat.add_comm 1 c).
rewrite Nat.add_assoc.
repeat ltac:(apply le_SS + rewrite Nat.add_0_r).

(* from H_le_abc.1 : a <= 1 and H_gt_zero.1 : 1 <= a *)
have Ha_one: a = 1. {
  have h1 := H_le_abc.1.
  have h2 := H_gt_zero.1.
  clear H_gt_zero Hb_one H_le_abc H_mul_abc.
  lia.
}
rewrite Ha_one.
repeat ltac:(apply le_SS + rewrite Nat.add_0_r + rewrite Nat.add_comm).
rewrite Ha_one in H_mul_abc.
do 2 rewrite Nat.mul_1_l in H_mul_abc.
exact H_mul_abc.2.
Qed.


(* Without loss of generality*)
(* We can replace ` a * b * c <= 2 -> a + b + c <= 4`
 with some P(a, b, c) that have commutativity on
 all arguments: 
 P(a, b, c) = P(a, c, b) = ... = P(c, b, a)
and do not stop!

How to generalize this?
Maybe:
forall P (H: commutative_arg P) (arg0, arg1, ...,): nat,
we can add arg0 <= arg1 <= arg2 <= ... <= argn as
premise.

Or arglist is just a list of nats?
forall (P: list nat -> Prop) (H: commutative_arg P) args: list nat,
we can add arg0 <= arg1 <= arg2 <= ... <= argn
as premise to goal (P arglist).
*)
Lemma main_lemma (a b c: nat):
 1 <= a * b * c <= 2 -> a + b + c <= 4.
Proof.
(**
we can assume in symmteric goals
(goals where all permutations of variables
creates same goals)
that exists some order a <= b <= c without
loss of generality.
*)
wlog: a b c / a <= b <= c.
move => J.
have perm1 := J a b c.
have perm2 := J a c b.
have perm3 := J b a c.
have perm4 := J b c a.
have perm5 := J c a b.
have perm6 := J c b a.
have p1p2eq : a * b * c = a * c * b /\ a + b + c = a + c + b by lia.
rewrite -(@p1p2eq.1) in perm2.
rewrite -(@p1p2eq.2) in perm2.
clear p1p2eq.
(* now check works: *)
Check or_ind (perm1) (perm2).
have Hor_p1p2 := or_ind perm1 perm2.

(* just imagine that we already have 6 permutation and after
apply Hor_p1p2.
goal becomes: |- a <= b <= c \/ a <= c <= b
it's all right... but how to simplify all 5=6-1 (possible 2*5=10) rewrites?

Maybe we can go forward (in our imagination) and assume that we already solved this:
*)

have Hor_all_orders: 
  a <= b <= c \/ a <= c <= b \/
  b <= a <= c \/ b <= c <= a \/
  c <= a <= b \/ c <= b <= a by lia.

(* to prove Hor_all_statements we must
use properties of multiplication [mul_assoc, mul_comm] and addition [add_assoc, add_comm], and `lia` can
 use it *)
have Hor_all_statements: 
  (a <= b <= c \/ a <= c <= b \/
  b <= a <= c \/ b <= c <= a \/
  c <= a <= b \/ c <= b <= a) -> 1 <= a * b * c <= 2 -> a + b + c <= 4.
  {
  (* if defined only perm1, perm2 lia fails, but
  now defined perm1, perm2, ... , perm6 and lia
  succeeds, because `lia` knows 
  [mul_assoc, mul_comm, add_assoc, add_comm] *)
    lia.
  }

have Hgoal := Hor_all_statements Hor_all_orders.
exact Hgoal.

apply: without_loss_2.
Qed.

(* now we must show equivalence of 6 
permutations of [a,b,c], [a,c,b],... [c,b,a]?
*)

(* apply Hor_p1p2.
have
apply J. *)

(* move => J.
have H := J a b c.
apply H. *)



