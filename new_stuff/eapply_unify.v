

Parameter A : Type.
Parameter R : A -> A -> Prop.

Axiom test : forall a b c d, R a b -> R c d.

Goal forall x y p q, R p q -> R x y.
Proof.
intros x y p q hr.
eapply test with p _.
exact hr.
Qed.

Goal forall x y p q, R p q -> R x y.
Proof.
intros x y p q hr.
eapply test.
exact hr.
Qed.

Section RecExample.

Record N: Prop :=
{eq_zero: forall a, a = 0}.

Variable (c : nat).
Hypothesis (h : c = 0).

Definition res (x : N) := 0.

Parameter (k : N).

Check eq_zero k 9.


End RecExample.

Section RatExample.

Record Rat : Set := mkRat
{ sign: bool
; top: nat
; bottom: nat
; Rat_bottom_cond : 0 <> bottom
; Rat_irred_cond :
    forall x y z:nat, (x * y) = top /\
    (x * z) = bottom -> x = 1
}.

Lemma one_two_irred : forall x y z:nat,
x * y = 1 /\ x * z = 2 -> x = 1.
Admitted.


Definition half := mkRat true 1 2 (O_S 1) one_two_irred.

Check half.

Eval compute in (
  match half with
  | {| sign := true; top := n; bottom:=m |} => (n, m)
  | _ => (0, 0)
  end).

Eval compute in (top half, bottom half).

Eval compute in (half.(top), half.(bottom)).

End RatExample.

Section Coinduction.

CoInductive Point {A : Set} : Set :=
| none : Point
| p : A -> A -> Point.


Inductive Point' {A : Set} : Set :=
| none' : Point'
| p' : A -> A -> Point'.

Check Point'_rect nat.
(* Point is defined
Point_rect is defined
Point_ind is defined
Point_rec is defined
Point_sind is defined *)

Check p 4 5.

Check Point


End Coinduction.




































