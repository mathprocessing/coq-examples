Require Export Bool.
Require Import Setoid.
Require Import Lia.
Require Import BinInt BinNat PeanoNat.
Require Import ssreflect ssrbool.
Require Import Hammer.Tactics.Reflect.

Goal forall x y z, (x <= y) /\ (y <= z) -> (x <= z).
blia.
Qed.


Goal forall a b, a || b -> a && b -> a && b.
move=>a b hor hand.
brefl in hor.
brefl in hand.
Fail exact: hand.
breif in hor.
breif in hand.
exact: hand.
Qed.

(* use coertion is_true 
Coercion is_true : bool >-> Sortclass.
defined in Hammer.Tactics.Reflect
*)
Goal forall a b, is_true a -> is_true b -> a && b.
move=> a b ha hb.
by bsplit.
Qed.

Check andb.
Check orb.
Check implb.
Check negb.
Check falseE.
Check trueE.

(* Tactic Notation "breflect" :=
  try rewrite_strat topdown hints brefl. *)

Check andb_true_r.
Check andb_true_l.


Example eqb_true_l x: eqb true x <-> (x = true).
Fail solve [bsimpl].
Fail by breflect.
by case: x.
Qed.

Example eqb_true_r x: eqb x true <-> (x = true).
Proof. by case: x. Qed.

Example eqb_false_l x: eqb false x <-> (x = false).
Proof. by case: x. Qed.

Example eqb_false_r x: eqb x false <-> (x = false).
Proof. by case: x. Qed.

Global Hint Rewrite -> eqb_true_l : bsimpl.
Global Hint Rewrite -> eqb_true_r : bsimpl.
Global Hint Rewrite -> eqb_false_l : bsimpl.
Global Hint Rewrite -> eqb_false_r : bsimpl.

Goal forall a b, (negb a -> negb b) = (b -> a).
move=> [] [].
Undo.
move => a b.
have Ha : (eqb a true) -> ((b -> a) <-> a).
move=>hat.
  breflect in hat.
  destruct hat as [h1 h2].
  Undo. Undo.
Fail bdestr hat.
(* I think bdestr must be equiv to 
breflect in H
destruct H as [H1 H2]
"maybe undo reflection"
*)
case a in hat.
Undo.
bsimpl in hat. (* Now it works! *)
rewrite hat.
  Fail breif.
(* because it don't looking at depth of goal expresion *)
rewrite <- implE.
rewrite trueE.
constructor.
by move=> _.
  move=> _.
  (* bsimpl.  not works *)
Fail by [].
now brefl.
bdestr a. (* use bdestr instead of case: a*)
bsimpl in Ha.
have Hpre : true = true by trivial.
apply Ha in Hpre.
(**
(b -> true) <-> true
I want using this as rewrite hyp.
or make coersion 
(b -> true) = true
then uses as rewrite
*)


(* We must prove that some tactic 
covers some set of proof states(goals, expression goals, types, etc.)
expr_set can't bound from up atoms_length < 3
ot subexpr_lists < 3. Or something like that.
To ensure that simple things must be solvable.

Another way of things in "same reaction" to "same states"

For example in this states:
|- a <-> b
|- a = b
|- eqb a b
our reaction must be "same" but not identical
*)
Abort.








Goal forall x y z, (x <= y) /\ (y <= z) -> (x <= z).
blia.
Qed.


Lemma andE : forall a b, a && b <-> a /\ b.
Proof.
split.
move /andP. done.
move /andP. done.
Qed.



