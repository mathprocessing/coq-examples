Definition square := fun (n : nat) => n * n.


Variable A : Type.

(** Definition relation (A : Type) := A -> A -> Prop. *)
Definition relation := A -> A -> Prop.

Variable R : relation.

Section General_Properties_of_Relations.

  Definition reflexive : Prop := forall x : A,
    R x x.
  Definition transitive : Prop := forall x y z : A,
    R x y -> R y z -> R x z.
  Definition symmetric : Prop := forall x y : A,
    R x y -> R y x.
  Definition antisymmetric : Prop := forall x y : A,
    R x y -> R y x -> x = y.

End General_Properties_of_Relations.



Lemma sym_iff (x y : A) :
  symmetric -> (R x y <-> R y x).
Proof.
  intro hs.
  apply conj. (** constructor. *)
  Check conj. (** forall A B, A -> B -> A /\ B *)
  + exact (hs x y).
  + exact (hs y x).
Qed.

Print sym_iff.


Definition sym_iff' (x y : A) : 
  symmetric -> (R x y <-> R y x) :=
    fun hs => conj (hs x y) (hs y x).



Definition flip' {A B C} (f : A -> B -> C) x y := f y x.
Definition test_flip {x y} : (flip' R) x y = R y x := eq_refl.

Require Import List.
Import ListNotations.

Section Permutation.

Variable A : Type.

Inductive Permutation : list A -> list A -> Prop :=
| perm_nil : Permutation [] []
| perm_skip x l l' : Permutation l l' -> Permutation (x::l) (x::l')
| perm_swap x y l : Permutation (y::x::l) (x::y::l)
| perm_trans l l' l'' :
  Permutation l l' -> Permutation l' l'' -> Permutation l l''.

https://coq.inria.fr/distrib/V8.10.0/stdlib/Coq.Sorting.Permutation.html

Theorem Permutation_nil : forall (l : list A),
  Permutation [] l -> l = [].
Proof.
  intros l h.
  

Qed.




End Permutation.






























