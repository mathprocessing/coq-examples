Module Short.

Ltac refl := reflexivity.

Definition f x : nat := x + 1.

Goal 1 = 1.
refl.
Abort.

End Short.


(** Require Export Short.*)

Check Short.f.
(** Check Short.refl. not works *)
Import Short.
Check f.

(** 
How to write new module?
*)