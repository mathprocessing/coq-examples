Record Rat : Set := mkRat
{ sign : bool
; top : nat
; bottom : nat
; Rat_bottom_cond : 0 <> bottom
; Rat_irred_cond : forall x y z:nat,
  (x * y) = top /\ (x * z) = bottom -> x = 1
}.

Definition T1 (x : Rat) : sign x = sign x := eq_refl.

Definition T2 (x : Rat) : bottom x = 0 -> False :=
  (fun b_ne_zero => 
    let H := Rat_bottom_cond x in
    H (eq_sym b_ne_zero)
  ).

Print T2.

(**
  fun (x : Rat) ... => ... : forall x : Rat, bottom x = 0 -> False
  [apply eta-redution]
  ... => ... : bottom x = 0 -> Fals
*)