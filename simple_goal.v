Goal forall X Y Z : Prop,
  (X -> Y) -> (Y -> Z) -> (X -> Z).
Proof. 
  exact (fun X Y Z A B C => B (A C)).
Qed.

Goal forall X Y Z : Prop, 
  (X -> Y) -> (Y -> Z) -> (X -> Z).
Proof.
  refine (fun _ _ _ _ _ C => _ (_ C)).
  exact y0. (* default variable names *)
  exact y.
Qed.

(* assert <=> have in lean prover *)
(* enough <=> suffices in lean prover *)
Goal forall X Y Z : Prop,
  (X -> Y) -> (Y -> Z) -> (X -> Z).
Proof.
  intros X Y Z A B C.
  Show Proof.
  assert (H : (X -> Z)).
  Check A C. (* Y *)
  Check B (A C). (* Z *)
  rewrite (B (A C)).
  
  
Qed.

(*
(fun (X Y Z : Prop) (A : X -> Y)
   (B : Y -> Z) (C : X) => ?Goal)
*)