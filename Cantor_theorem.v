Set Implicit Arguments.
Unset Strict Implicit.
Require Import Omega.
Require Import List.
Import ListNotations.

Definition surjective (X Y : Type) (f : X -> Y) : Prop :=
  forall y, exists x, f x = y.


Goal forall (A B : Prop), A -> B -> A.
Proof.
  intros A B ha hb.
  exact ha.
Qed.

Goal forall (A B : Prop), A -> not A -> False.
Proof.
  intros A B ha hb.
  contradiction.
  Show Proof.
Qed.

Goal forall (A : Prop), A -> not A -> False.
Proof.
exact (fun (A : Prop) (ha : A) (hb : ~ A) => (hb ha)).
Qed.

Goal forall (A : Prop), A -> ~ A -> False.
Proof.
intros A hA hNotA.
Check False_ind.
Check False_ind False.
Check hNotA hA. (** hNotA : A -> False *)
exact (hNotA hA).
(* exact (False_ind False (hb ha)). *)

Qed.


Check iff_trans.
Check iff_stepl.
Print iff_stepl.

Lemma helper_for_helper (A : Prop) : (A -> ~ A) -> ~ A.
Proof.
  change ((A -> (A -> False)) -> A -> False).
 (**  assert (h3 : (A -> A) <-> True ) by tauto. *)
  intros h_A_notA h_A.
  (** Check h_A_notA h_A.
  pose (h_notA := h_A_notA h_A). *)
  exact ((h_A_notA h_A) h_A).
  
  (** (fun (A : Prop) (H : A -> ~ A) (H0 : A) =>
         let H1 : ~ A := H H0 in let H2 : False := H1 H0 in False_ind False H2)
  *)
Qed.

Lemma helper (A : Prop) : (A <-> ~ A) -> False.
Proof.
  (** tauto. *)
  apply and_ind.
  intros H0 H1.
  assert (H3 : _) by exact (helper_for_helper H0).
  assert (H2 : A) by exact (H1 H3).
  change (A -> False) in H3.
  exact (H3 H2).
  (* contradiction. *)
Qed.


(**
(fun (A : Prop) (H : A <-> ~ A) =>
 and_ind
   (fun (H0 : A -> ~ A)
      (H1 : ~ A -> A) =>
    let H2 : A :=
      let H2 : ~ A :=
        fun H2 : A =>
        let H3 : ~ A := H0 H2 in
        let H4 : False := H3 H2 in
        False_ind False H4 in
      H1 H2 in
    (fun H3 : A =>
     let H4 : ~ A := H0 H3 in
     let H5 : False := H4 H3 in
     False_ind False H5) H2) H)
 *)

Theorem Cantor X : 
  ~ exists f : X -> X -> Prop, surjective f.
Proof.
  intros [f A].
  Show Proof.
  pose (g := fun x => ~f x x).
  (** empty diagonal of function f. *)
  Show Proof.
  destruct (A g) as [x B].
  Show Proof.
  assert (C : g x <-> f x x).
  { rewrite B. reflexivity. }
  unfold g in C.
  Show Proof.
  exact (helper (A := f x x) (iff_sym C)).
  Show Proof.
  (**
  refine (and_ind _ C).
  destruct C as [C0 C1].
  
  (~ f x x -> f x x) -> (f x x -> ~ f x x) -> False
  True -> (True -> False)
  True -> False   [interesting by two ways we have same result!
                  1) True -> expr = expr   2) True -> False = False]
  False   => goal failed... this way not works.
  *)
Qed.

(**
and_ind
           (fun (H0 : ~ f x x -> f x x) (H1 : f x x -> ~ f x x) =>
            let H2 : f x x :=
              let H2 : ~ f x x :=
                fun H2 : f x x =>
                let H3 : ~ f x x := H1 H2 in
                let H4 : False := H3 H2 in False_ind False H4 in
              H0 H2 in
            (fun H3 : f x x =>
             let H4 : ~ f x x := H1 H3 in
             let H5 : False := H4 H3 in False_ind False H5) H2) C


 *)





(**  pose (square := fun x : nat => x * x). *)
