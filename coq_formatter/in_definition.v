Definition some (x y : nat) :=
match x, y with
| 0, 0 := 100
| (S n), n := n
end.