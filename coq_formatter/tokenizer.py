"""
States:
read_index = 0 -> State = full_set
read_index = 1 -> State = union(token1, token2, ...)
...

-------------------------Examples-----------------------------
CheckB.
Syntax error: illegal begin of vernac.
Check B. (* OK *)

Check B5x.
The reference B5x was not found in the current environment.

Check B 5x. <=> Check B 5 x.
--------------------------------------------------------------

# Grammar

Assumptions:
Variable := letter + seq(letter or digit)
Number := digit + [Number or ""]


Implications:
`5x` is not a variable (from Variable_def)
single digit is a number (from number_def) (this example contraints to force `Number or ""` instead of just `Number`)


Task #1: Tokenize simple sequence of hexadecimal numbers, spaces and decimal numbers.
"0xAB 42 0x7    CGHJ5 65536" -> [(HEX, '0xAB'), (DEC, '42'), (HEX, '0x7'), (UNKNOWN, 'CGHJ5'), (DEC, '65536')]

Task #2: Tokenize arithmetic expressions.
"x + 0 <= y + x" -> [(VAR, 'x'), (FUNC, '+'), (CONST, '0'), (FUNC, '<='), (VAR, 'y'), (FUNC, '+'), (VAR, 'x')]
"abc - h x = -123 * (5 + -7)" -> [(VAR, 'abc'), (FUNC, '-'), (VAR, 'h'), (FUNC, MUL_SPACE), (VAR, 'x'), (FUNC, '='), (FUNC, '-'), (CONST, '123'), (FUNC, '*'), (OPEN, '('), (CONST, '5'), (FUNC, '+'), (FUNC, '-'), (CONST, 7), (CLOSE, ')')]
                              -> [(VAR, 'abc'), (FUNC, '-'), (VAR, 'h'), (VAR, 'x'), ...]
                              -> [                                                                 ..., (FUNC, '='), (UNARY_MINUS, '-'), (CONST, '123')]
                              -> [                                                                 ..., (FUNC, '='), (CONST, '-123')]

Task #3: Tokenize turbo pascal like code only with variable declarations (only integers) and if statements:
var x : integer;
if x = 5 then
  x := 7
end;

if sin(x) = 1 then write(123); if sin(x) = -1 then write(321) end; end;


"""
from pytest import raises
import string

class TokenizeError(Exception):
  pass


class Representable:
  def __repr__(self):
    return str(self)


class TokenType(Representable):
  def __init__(self, token_type_id):
    self.token_type_id = token_type_id

  def __str__(self):
    return token_type_repr[self.token_type_id]

  def __eq__(self, other):
    if isinstance(other, list):
      return False
    return self.token_type_id == other.token_type_id

  def __lt__(self, other):
    return self.token_type_id < other.token_type_id

  def __le__(self, other):
    return self.token_type_id <= other.token_type_id

  def __hash__(self):
    return self.token_type_id


def get_full_set():
  return map(TokenType, range(len(token_type_repr)))


token_type_repr = ["HEX", "DEC", "SPACE", "L_BRACKET", "R_BRACKET", "VAR"]
HEX, DEC, SPACE, L_BRACKET, R_BRACKET, VAR = tuple(get_full_set())

"""
        if (ch not in '0123456789') and (prev_ch in ' ()'):
          return {VAR, SPACE, L_BRACKET, R_BRACKET, DEC, HEX} # ~{HEX, DEC}
        else:
          return {VAR, HEX} # {VAR} -> False, {HEX} -> False -> need more if branches
"""


"""
set({}) API:
'add', 'clear', 'copy', 'difference', 'difference_update', 'discard', 'intersection', 'intersection_update', 'isdisjoint', 'issubset', 'issuperset', 'pop', 'remove', 'symmetric_difference', 'symmetric_difference_update', 'union', 'update']
"""

def char_to_token_subset(prev_ch: str, ch: str):
  hex_letters = 'xABCDEFabcdef'
  if ch == ' ':
    if prev_ch in "-+":
      raise TokenizeError("Unknown symbol")
    else:
      return {SPACE}
  elif ch.upper() in string.ascii_uppercase:
    if ch not in hex_letters:
      return {VAR}
    else: # in that branch `ch in hex_letters`
      if ch == 'x' and prev_ch == 'x':
        # branch created after: test A and test B
        return {VAR}
      else:
        if prev_ch == ' ': # splitting by condition prev_ch == ' ' {VAR, HEX} to {VAR} and {VAR, HEX} by test D
          return {VAR}
        else:
          return {VAR, HEX} # {VAR} -> False, {HEX} -> False -> need more if branches
  elif ch in '0123456789':
    if prev_ch in hex_letters:
      # {HEX, DEC} also valid on tests -> means we have valid mutation -> it's not good -> our program needs more test cases
      return {HEX, VAR} # add VAR to {HEX} by `test C`
    else:
      if prev_ch in ' +-':
        if ch == '0':
          return {DEC, HEX} # {DEC} -> False, {HEX} -> False -> need more if branches
        else:
          return {DEC}
      else:
        return {DEC, HEX} # {DEC} -> False, {HEX} -> False -> need more if branches
  elif ch == '(':
    return {L_BRACKET}
  elif ch == ')':
    return {R_BRACKET}
  elif prev_ch in ' ()' and ch in '-+':
    return {DEC}
  else:
    raise TokenizeError("Unknown symbol")


class Tokenizer(Representable):
  def __init__(self):
    self.full_token_set = set(get_full_set())
    self.state = self.full_token_set.copy()
    self.tokens = []
    self.temp_token = ""
    self.prev_ch = " "


  def tokenize(self, content: str):
    for ch in (content + " "):
      self.eat(ch)
    return self.tokens


  def eat(self, ch: str):
    # `state` can be changed or not changed after this operation
    tss = char_to_token_subset(self.prev_ch, ch)
    prev_state = self.state.copy()
    self.state = self.state.intersection(tss)
    if len(self.state) == 0:
      # reset state
      self.state = self.full_token_set.copy()
      # check that we have only one hypothesis
      # assert len(prev_state) == 1
      if len(prev_state) == 1:
        token_type = prev_state.pop()
      else:
        token_type = list(prev_state)
      self.tokens.append((token_type, self.temp_token))
      self.temp_token = ""
      self.eat(ch)
    else:
      self.temp_token += ch
    self.prev_ch = ch

  def __str__(self):
    return f'state: {set(map(lambda token_type_id: TokenType(token_type_id), self.state))}, temp_token: "{self.temp_token}", tokens: {self.tokens}'


def tokenize(content: str):
  return Tokenizer().tokenize(content)


def print_list(lst):
  for elem in lst:
    print(elem)


def test_long():
  assert tokenize("0x 9xAB (-12)  29 ())(0x78) +5 ") == [(HEX, '0x'), (SPACE, ' '), (DEC, '9'), ([HEX, VAR], 'xAB'), (SPACE, ' '), (L_BRACKET, '('), (DEC, '-12'), (R_BRACKET, ')'), (SPACE, '  '), (DEC, '29'), (SPACE, ' '), (L_BRACKET, '('), (R_BRACKET, '))'), (L_BRACKET, '('), (HEX, '0x78'), (R_BRACKET, ')'), (SPACE, ' '), (DEC, '+5')]
  assert tokenize("0xAB 0x123 9xAB 9x12 0x12 ") == [(HEX, '0xAB'), (SPACE, ' '), (HEX, '0x123'), (SPACE, ' '), (DEC, '9'), ([HEX, VAR], 'xAB'), (SPACE, ' '), (DEC, '9'), (HEX, 'x12'), (SPACE, ' '), (HEX, '0x12')]


def test_shorts():  
  assert tokenize("-100") == [(DEC, '-100')]
  assert tokenize("0x12") == [(HEX, '0x12')]
  assert tokenize("-0x12") == [(DEC, '-0'), (HEX, 'x12')]
  assert tokenize("9x12 0x12") ==[(DEC, '9'), (HEX, 'x12'), (SPACE, ' '), (HEX, '0x12')]
  with raises(TokenizeError):
    tokenize("10+200")
  assert tokenize("10 +200") == [(DEC, '10'), (SPACE, ' '), (DEC, '+200')]
  assert tokenize("Hello 9xAB") == [(VAR, 'Hello'), (SPACE, ' '), (DEC, '9'), ([HEX, VAR], 'xAB')]
  assert tokenize("Hello 0xAB") == [(VAR, 'Hello'), (SPACE, ' '), (HEX, '0xAB')]
  with raises(TokenizeError):
    tokenize("++")
  with raises(TokenizeError):
    tokenize("-+100")
  #-----------------------------------------------------------------------------------
  # See in the function `char_to_token_subset` which branch is impacted by these tests
  assert tokenize("xx") == [(VAR, 'xx')] # test A
  assert tokenize("xx0xFF") == [(VAR, 'xx0xFF')] # test B
  #-----------------------------------------------------------------------------------
  assert tokenize("d6wa") == [(VAR, 'd6wa')] # test C
  assert tokenize("d") == [(VAR, 'd')] # test D

test_long()
test_shorts()
print_list(tokenize("7 8 278 28472 82 742 -7478284 +2 482 K j jdaw dhaw d6wa dwjahd 272 d x wajda w"))