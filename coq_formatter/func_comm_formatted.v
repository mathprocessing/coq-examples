Require Import Unicode.Utf8.
Axiom sorry : forall {A : Type}, A.

Parameter f : nat -> nat -> nat.
Axiom A : ∀ y, f     0 y = y.
Axiom B : ∀     x y, f x (S y) = f (      S x) y.

Section Section1.
Variables x y : nat.

Check B     x 0.
Check B 0     y.
Check A 0.

Check B 0 y.   (** f 0 (S y) = f 1 y *)
Check A (S y). (** f 0 (S y) = S y   *)

Check eq_trans.

Check eq_trans (eq_sym (A (S y))) (B 0 y).

Definition some (x y : nat) :=
    match x, y with
| 0,    0 := 100
| (S n), n := n
end
.

Lemma L1 : S y = f 1 y.
Proof.
eapply eq_trans.
exact (eq_sym (A (S y))).
eapply B.
Qed.

Create HintDb my.
Hint Resolve A B eq_trans eq_sym: my.

Lemma L1_2 : S y = f 1 y.
Proof.
eauto with my.
Qed.

Hint Resolve L1 : my.

Lemma L2 : S (S y) = f 2 y.
Proof.
  eapply eq_trans.
     exact (eq_sym (A (S (S y)))). eapply eq_trans.
  exact      (B 0   (S y     )).
  exact (B 1   y).
Qed.
(**
eq_trans (eq_sym (A (S (S y))))
  (eq_trans (B 0 (S y)) (B 1 y))
     : S (S y) = f 2 y
*)

Print L2.

Example comm : ∀ x y, x + y =       y + x := sorry.
Example add_zero_r : ∀x, x     + 0 = x := sorry.

Check add_zero_r y.

Lemma right_zero: f x 0 = x.
Proof.
induction x.
exact (A 0).
assert (H : _) by exact (A n).

(**
A :   ∀ y, f(0, y) = y
B : ∀ x y, f(x, y + 1) = f(x + 1, y)
C : ∀ x y, f(x, y + 1) = f(x, y) + 1

IH : f(n, 0) = n
h : f(0, n) = n
Goal : f(n + 1, 0) = n + 1

 f(n + 1, 0) = n + 1





*)





Qed.

End Section1.
