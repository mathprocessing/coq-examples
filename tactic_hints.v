Require Import List.
Import ListNotations.
Goal forall A : Prop, A -> (A -> A).
Proof.
Info 1 auto.
Qed.

Goal forall x: nat, x + 1 = x + 1.
Proof.
Info 1 simpl.
Abort.

Hint Extern 5 ((?X1 = ?X2) + (?X1 <> ?X2)) =>
  generalize X1, X2; decide equality : eqdec.

Goal forall a b:list (nat * nat), 
  {a = b} + {a <> b}.
Proof.
Info 1 auto with eqdec.
Abort.

Search (?a + ?b = ?b + ?a).

Require Import PeanoNat.
Import Nat.
Require Import Setoid.


Hint Extern 4 (?a + ?b = ?b + ?a) => apply add_comm : core.


Goal forall x : nat, (x + 2 = 2 + x).
Proof.
intros x.
Print HintDb core.
Print Firstorder Solver.
auto.
Show Proof.
Qed.

(** f_equal tactic test *)

Goal forall (f : Prop -> Prop -> Prop) (A B : Prop),
  f (True -> A) (B -> B) = f A True.
Proof.
intros.
f_equal.

Qed.




Definition my_if :=
fun P Q R =>
P /\ Q \/ ~ P /\ R.


Goal forall A B : Prop, my_if True A B = A.
intros.
unfold my_if.
assert (H : forall P : Prop, True /\ P = P) by tauto.
(* rewrite (H A). *)
replace (True /\ A) with A.
rewrite (H A).

(*

Tactic failure: setoid rewrite failed: Unable to satisfy the following constraints:
UNDEFINED EVARS:
 ?X40==[A B H |- relation Prop]
         (internal placeholder) {?r}
 ?X41==[A B H
         (do_subrelation:=Morphisms.do_subrelation)
         |- Morphisms.Proper
              (and ==> ?r) not]
         (internal placeholder) {?p}
 ?X42==[A B H |- relation Prop]
         (internal placeholder) {?r0}
 ?X43==[A B H |- relation Prop]
         (internal placeholder) {?r1}
 ?X44==[A B H
         (do_subrelation:=Morphisms.do_subrelation)
         |- Morphisms.Proper
              (?r ==> ?r1 ==> ?r0)
              and]
         (internal placeholder) {?p0}
 ?X45==[A B H
         |- Morphisms.ProperProxy
              ?r1 B]
         (internal placeholder) {?p1}
 ?X46==[A B H |- relation Prop]
         (internal placeholder) {?r2}
 ?X47==[A B H
         (do_subrelation:=Morphisms.do_subrelation)
         |- Morphisms.Proper
              (?r0 ==> ?r2) 
              (or A)]
         (internal placeholder) {?p2}
 ?X48==[A B H |- relation Prop]
         (internal placeholder) {?r3}
 ?X49==[A B H
         (do_subrelation:=Morphisms.do_subrelation)
         |- Morphisms.Proper
              (?r2 ==>
               ?r3 ==>
               Basics.flip
                 Basics.impl)
              Logic.eq]
         (internal placeholder) {?p3}
 ?X50==[A B H
         |- Morphisms.ProperProxy
              ?r3 A]
         (internal placeholder) {?p4}
TYPECLASSES:?X40 ?X41 ?X42 ?X43
?X44 ?X45 ?X46 ?X47 ?X48 ?X49
?X50
.

 *)