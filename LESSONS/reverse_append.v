(**


 *)
Require Import Unicode.Utf8.
Require Import List.
Import ListNotations.
Axiom sorry : forall {A : Type}, A.

Section Section1.
Variables (T : Type).
Parameter reverse : list T -> list T.
Parameter append : list T -> T -> list T.

Example r_base : reverse nil = nil := sorry.
Example r_step (x : T) (xs : list T) :
  reverse (x :: xs) = append (reverse xs) x := sorry.
  
Example a_base (x : T) : append nil x = [x] := sorry.
Example a_step (x y : T) (xs : list T) : 
  append (x :: xs) y = x :: append xs y := sorry.

(* Theorem factorization (rewrite factorization):
  a_base_step := rewrite t1. rewrite t2. Task: Find t1 and t2.
*)
Lemma a_base_step (x y : T) :
  append (append [] y) x = y :: append [] x.
Proof. rewrite a_base. apply a_step. Qed.

Example b := a_base.
Example s := a_step.

Lemma reverse_single (x : T) : reverse [x] = [x].
Proof.
rewrite r_step, r_base.
rewrite b. (** all solutions: b, length = 1, proof_length = 1 *)
exact eq_refl.
Qed.

Lemma reverse_two (x y : T) : reverse [x; y] = [y; x].
Proof.
rewrite 2r_step, r_base.
rewrite b,s,b. (** all solutions: bsb, length = 1, proof_length = 3 = 1 + 2 *)
exact eq_refl.
Qed.

Lemma reverse_three (x y z : T) : reverse [x; y; z] = [z; y; x].
Proof.
rewrite 3r_step, r_base.
rewrite b, s, s, b, s, b.
(**
bsbssb
bssbsb

all solutions: b s(b _) s b, length = 2 = 2!
proof_length = 6 = 1 + 2 + 3

*)
exact eq_refl.
Qed.

Lemma reverse_four (x y z w : T) : reverse [x; y; z; w] = [w; z; y; x].
Proof.
rewrite 4r_step, r_base.
rewrite b,s,b,s,s,b.
(** 
bsbsssbssb (X)
bsbssssbsb (palindrom)
bssbssbssb (every 3rd)
bssbsssbsb (mirror X)
bsssbsbssb (Y)
bsssbssbsb


all solutions: b s(b _ _)s(b _)s b, length = 3 * 2 = 3!
proof_length = 10 = 1 + 2 + 3 + 4

(b _ _) <=> means only one b allowed

*)
exact eq_refl.
Qed.

Lemma reverse_five (x y z w u : T) : reverse [x; y; z; w; u] = [u; w; z; y; x].
Proof.
rewrite 5r_step, r_base.
rewrite b,s,s,s,s,b,s,s,s,b,s,s,b,s,b.
(** 
bssb..
bsss..

bsbssssbsssbssb
bsbssssbssssbsb (palindrom)
bsbsssssbssbssb
bsbsssssbsssbsb
bsbssssssbsbssb
bsbssssssbssbsb

Process of search:
if b and s:
  copy branch [bss..] to [bssb.., bsss..]
if only b or only s:
  append symbol to solution with lower lexicographical order
if no variants:
  make solution closed (bss.. to bss)
  
Idea of Improving search: (move all symbol maximally right or maximally left)
Just check bounds of 'b' symbol positions:
left bound:  bsbssb......ssb
right bound: bss......bssbsb

Hypothesis1: left_bound = mirror(right_bound)
Counterexample solution(bsssbssbsb) => not solulution(bsbssb...)
                        X                             mirror X

fast solution for all reverse problems:
  b
  b s b
  b s s b s b
  b s s s b s s b s b
  b s s s s b s s s b s s b
  b s_n b s_n-1 b ... b s s b s b
From this scheme we can conclude that proof_length = 1 + 2 + 3 ... + n
*)
exact eq_refl.
Qed.


Theorem concat



End Section1.




