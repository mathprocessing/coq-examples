(**
I think proof is a like path in graph, that have nodes and edges.
This can be better viewed if you think about forward proof and backward proof.
Forward proof is like answer to question:
What can I have now? What can I easy get by 1 step of (replacing, rewriting or ...)?
Forward proof - just one step from current state of knowledge.
Example:
Prove that: 1 <= 3
1) I know that 1 <= 1 from (le_refl 1)
2) also I can understand substitution, so
I have 1 <= 2 from le_step : forall n m. n <= m ==> n <= m + 1 and le_refl : 1 <= 1

Backward proof:
Backward proof usually we get if we use tactic apply.
Backward proof is like answer to question:
What step must finishing this whole proof?
What we must apply to finish this proof? (1 step before)
*)

Require Import Arith.
Require Import Arith.PeanoNat.

(* Define sorry keyword like in Lean Theorem Prover *)
Axiom sorry : forall {A : Type}, A.

Check nat.

Inductive Ternary :=
| Yes : Ternary
| No : Ternary
| Unknown: Ternary. (* We don't know exactly that thing equals Yes or No *)

Check Yes. (* `Yes : Ternary` means lambda expression `Yes` has type `Ternary`*)

Definition flip (x : Ternary) : Ternary :=
match x with
| Yes => No
| No => Yes
| _ => Unknown
end.

Definition t_and (x y : Ternary) : Ternary :=
match x, y with
| Yes, y => y
| No, _ => No
| _, No => No
| Unknown, Yes => Unknown
| Unknown, Unknown => Unknown
end.

(* Let's use more lambdas: *)
(* You can type `lambda symbol` just typing `fun` *)
Check (fun x => x + 1). (* one argument *)
Check (fun x => fun y => x + y). (* two arguments *)
(* Apply carrying to lambda expression results to: *)
Check (fun x y => x + y). (* is equivalent to expression above *)

Definition If {T: Type} (condition : bool) (first second : T) :=
match condition with true => first | false => second end.

Example test_If_1 : If true 1 2 = (1 : nat) := eq_refl.
Example test_If_2 : If false 1 2 = (2 : nat) := eq_refl.
Example test_If_3 : If true false true = (false : bool) := eq_refl.
Example test_If_4 : @If bool true false true = (false : bool) := eq_refl.

(* `Parameter` is like `constant` in Lean Theorem Prover *)
Parameter is_prime : nat -> Prop.
Axiom two_is_prime : is_prime 2.

(* slightly more complicated example of lambda expression *)
Check fun (x : nat) (h1 : is_prime x) (h2 : 0 < x /\ x < 3) => x = 2.


Eval compute in (fun x => fun y => x + y) 2 3. (* 5 *)
Eval compute in (fun x y => x + y) 2 3. (* 5 *)
Eval compute in (fun (x : nat) (h1 : is_prime x) (h2 : 0 < x /\ x < 3) => x = 2) 2 (two_is_prime : is_prime 2).

(* 
0. lambda expression:       two_is_prime     we can think about this as `name of hypothesis`
   type of that expression: is_prime 2       ...                        `statement of hypothesis`

1. fun (h1 : is_prime 2) (h2 : 1 <= 2 /\ 3 <= 3) => 2 = 2 : is_prime 2 -> 0 < 2 < 3 -> Prop
  lambda expression:       `fun (h1 : is_prime 2) (h2 : 1 <= 2 /\ 3 <= 3) => 2 = 2`
  type of that expression: `is_prime 2 -> 0 < 2 < 3 -> Prop`

2. fun (h1 : is_prime 2) (h2 : True /\ True) => True : is_prime 2 -> 0 < 2 < 3 -> Prop
 
 
 
 *)
 
 
Check 2.

(* You can use `Eval compute in [expression]` to evalute expression, but it's a bad practice to have it in code *)

Eval compute in flip Yes. (* No : Ternary *)
Eval compute in t_and Yes Unknown. (* Unknown : Ternary *)

(** Or you can check it by writing tests: (Yes! You can write tests in Coq! And no need to use `Eval` in code) *)

Example test_flip_1 : flip Yes = No := eq_refl.
Example test_flip_2 : flip No = Yes := eq_refl.
Example test_flip_3 : flip Unknown = Unknown := eq_refl.
Example test_flip_4 : flip (flip Yes) = Yes := eq_refl.

Example test_t_and_1 (x : Ternary) : t_and x x = x.
Proof.
  case x; exact eq_refl.
  Show Proof.
Qed.

Example test_t_and_1_using_match (x : Ternary) : t_and x x = x :=
match x with
| Yes => eq_refl 
| No => eq_refl
| Unknown => eq_refl
end.


(*
match x as t return (t_and t t = t) with
| Yes => eq_refl 
| No => eq_refl
| Unknown => eq_refl
end.
*)

Definition is_unknown : Ternary -> Prop :=
fun x => match x with
| Unknown => True
| _       => False
end.

Example test_all_1 (x y : Ternary) : is_unknown x -> y = flip x -> t_and y y = x.
Proof.
  intros h1 h2.
  assert (h3 : is_unknown x -> x = Unknown).
  case x.
  + intro h4.
    exfalso.
    exact h4.
  + intro h4.
    exfalso.
    exact h4.
  + intro h4.
    exact eq_refl.
  + assert (h_xy : x = y) by exact sorry.
    rewrite h_xy.
    apply test_t_and_1.
Qed.

(* Let's write more complicated proofs than eq_refl *)

(* We free to add your own thorems if you don't have enough time to search it. It's totally fine.
But you must verify it manually (in paper, or using `Check`, or writing tests like we did earlier) *)


Lemma le_step_explicit (n m : nat) : n <= m -> n <= S m.
Proof. Admitted.

Check le_step_explicit 0 1.

(* angle brackets make explicit arguments implicit *)
Lemma le_step {n m : nat} : n <= m -> n <= S m.
Proof. Admitted.

(* We must use prefix `@` to make our arguments of function explicit *)
Check @le_step 1 1.

(* Let's construct a proof manually*)
Check le_refl 1.
Check le_trans 1 2 3. (* OK that's can help, we have 1 <= 3 as right part if implication *)
Check @le_step 1 1.

(* Let's prove our first thorem *)
Theorem test_le_long : 1 <= 3.
Proof.
(* remember add brackets `()` if some stuff in interactive tactic mode did't work
   not works: apply le_trans 1 2 3.
   works:     apply (le_trans 1 2 3). *)
eapply le_trans.
apply (le_step : 1 <= 1 -> 1 <= 2). (* We can add type to our lambda expression to help youself *)
(* You can think about type as a:
1. difference between goal1 and goal2
2. Statement of theorem
3. State or Node in proving graph, where Actions or Edges are theorems.
*)
exact (le_refl _).
(* Now we must prove 2 <= 3 bacause end of proof 1 <= 3 already determined
1 <= 2 => 2 <= 3 => 1 <= 3 equivalent to 1 <= 2 and 2 <= 3 => 1 <= 3.
This means we have two subgoals: 1 <= 2 and 2 <= 3, first subgoal 1 <= 2 is already solved. *)

(* Make that construction to help yourself: exact (le_step 2 2 (_)). *)
exact (le_step 2 2 (le_refl _)).
(* exact (le_step _ _ (le_refl _)). also valid proof *)
Qed.

(* Let's turn that proof to more shortest (in one line)
   I recommend to use wildcard `_`:
   Example test_le_short : 1 <= 3 := _.
   And see error message: Cannot infer this placeholder of type "1 <= 3"
   That means we have goal "1 <= 3". 
*)

(* Note: if you put single quote ("") (but single) to comments, surprisingly it might give error: Syntax Error: Lexer: Unterminated string *)

(* Just steps:
1. Example test_le_short : 1 <= 3 := (le_trans _ _).

   You can see `Nat.le_trans ?n ?m ?p and ?n <= ?m -> ?m <= ?p -> ?n <= ?p`
   
   We want that "?n <= ?p" to be equal to "1 <= 3" that means ?n = 1, ?p = 3
   Nat.le_trans 1 ?m 3, we can leave ?m as wildcard `_` like in Lean Thorem Prover, but in Coq this trick sometimes not helps.
   
   finally we got: Nat.le_trans 1 _ 3, let's substitite it to our oneline proof.

2. Example test_le_short : 1 <= 3 := (le_trans 1 100 3 (_ : 42 = 42) (_)).
   You can see error message:
   `The term "?e : 42 = 42 " has type "42 = 42" while it is expected to have type "1 <= 100".`
   We just put it to TYPE of lambda expression in our proof.
   
   After this you can see message:
   `Cannot infer this placeholder of type "1 <= 100".`
   That means OK! Coq accepted that type as valid.
   
   
   
   
*)

Example test_le_short : 1 <= 3 := (le_trans 1 2 3 (le_step : 1 <= 2) (_)).


Lemma flip_twice (x : Ternary) : flip (flip x) = x.
Proof.

exact (
match x with
Yes => eq_refl
_ => _
end
).


Qed.




