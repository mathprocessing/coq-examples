Require Import List.
Import ListNotations.
Require Import Permutation.
Check Permutation_ind.

Print Permutation_sym.

(* Goal 1 <> 2.
intro h.
apply succ. *)


Print Permutation_nil.

Lemma nil_cons (A : Type) (l : list A) (x : A) :
  ~ Permutation nil (x::l).
Proof.
apply (list_ind 
  (fun xs => ~ Permutation [] (x :: xs))
).
  intro h.
  (**
  TARGET STATE: Permutation [] l /\ l <> [].
  Possible actions:
    perm_nil            : Permutation [] []
    perm_skip x l l'    : Permutation l l' -> Permutation (x::l) (x::l')
    perm_swap x y l     : Permutation (y::x::l) (x::y::l)
    perm_trans l l' l'' :
      Permutation l l' -> Permutation l' l'' -> Permutation l l''.

  let previous_action = perm_nil => False from l <> [] in Permutation [] l
  let previous_action = perm_skip => False from
    we have : Permutation (x::l) (x::l') `not intersected with` Permutation [] l
    from : (x::l) <> []
  let previous_action = perm_swap => False from [] <> (y::x::_)
  let previous_action = perm_trans => False from
    Permutation [] l' -> Permutation l' l -> Permutation [] l.
    => we must have this property: l' = []
    Permutation [] [] -> Permutation [] l -> Permutation [] l.
    by perm_nil
    Permutation [] l -> Permutation [] l.
    => previous_state must be target_state => that's impossible
   *)
  
  
Qed.


Lemma sym (A : Type) (l l' : list A) :
  Permutation l l' -> Permutation l' l.
Proof.
intro Hperm.
apply (Permutation_ind (fun l l' => Permutation l' l)). (** inductive predicate *)
+ exact (perm_nil A).
+ intros x l0 l'0 _ IHHperm.
  exact (perm_skip _ IHHperm).
+ intros x y l0.
  exact (perm_swap y x l0).
+ intros l1 l2 l3 _ h2 _ h4.
  transitivity l2.
  - exact h4.
  - exact h2.
+ exact Hperm. (** initial proposition *)
Qed.

Goal forall (x y z : nat) (H : x = y) (C : Prop),
C -> (y = z -> C).
Proof.
  intros ? ? ? x_eq_y.
  intros C hc.
  intros ->.
  exact hc.
(*   intros y_eq_z.
  pose (x_eq_z := eq_trans x_eq_y y_eq_z). *)
Qed.



Print Permutation_refl.
Lemma Permutation_refl' (A : Type) 
  (l : list A) : Permutation l l.
Proof.
  induction l using list_ind.
  + exact (perm_nil A).
  + apply perm_skip.
    exact IHl.
    Show Proof.
Qed.

Definition Permutation_refl''
  (A : Type) (l : list A) : Permutation l l :=
  (
    list_ind _ _ _ l
  ).


(** 
Proof of: Permutation_refl
fun (A : Type) (l : list A) =>
list_ind
  (fun l0 : list A => Permutation l0 l0)
  (perm_nil A)
  (fun (a : A) (l0 : list A)
     (IHl : Permutation l0 l0) =>
        perm_skip a IHl)
   l
*)


Lemma Permutation_nil' : forall (A : Type) (l : list A),
  Permutation nil l -> l = nil.
Proof.
  induction l.
  exact (fun _ => eq_refl).
  intro hp.
  exfalso.
  exact (Permutation_nil_cons hp).
(**
  assert (Permutation [] l -> Permutation [a] (a :: l)).
  exact (fun hpnil : Permutation [] l => perm_skip a hpnil).
  ------------------------------------------
  intros A l HF.
  set (m := nil).
  assert (Heqm : m = nil) by exact eq_refl.
  apply Permutation_ind.
  + exact eq_refl.
  + intros x l0 l' hp leq.
    Check eq_ind 
            (x :: l0) 
            (fun e : list A => match e with nil => False | (_ :: _) => True end)
            I nil.
    rewrite leq.
    exact eq_refl.
  + intros.
    admit.
  + intros.
    exact (eq_trans H0 H2).
  + rewrite Heqm.
    apply Permutation_sym.
    exact HF.
*)
Qed.