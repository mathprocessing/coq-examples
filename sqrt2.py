# p/q == sqrt(2)
# (p*p)/(q*q) == 2
# p*p == 2*q*q
# obviously p > q

p = 1
while True:
  for q in range(1, p+1):
    if p*p == 2*q*q:
      print("FOUND %d/%d" % (p,q))
      exit(0)
    else:
      print("miss %d/%d" % (p,q))
  p += 1
