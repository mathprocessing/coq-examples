Require Import ZArith.
(* need for rapply tactic *)
Require Import Coq.Program.Tactics.

Locate "++".
(* "x ++ y" := app x y : list_scope *)
Locate "_ = _".
(* "x = y" := eq x y : type_scope *)
Locate "a = b = c".
(* Unknown notation *)
Local Notation "a = b = c" := (a = b /\ b = c).

Lemma transitive {T : Type} (a b c : T) :
  a = b /\ b = c -> a = c.
Proof.
  intro h.
  destruct h as [h1 h2].
  transitivity b. exact h1. exact h2.
Qed.

Lemma transitive' {T : Type} {a b c : T} :
  a = b /\ b = c -> a = c.
Proof.
  intro h.
  exact (
    match h with
    | conj h1 h2 => eq_trans h1 h2
    end).
Qed.

Lemma update_notation {T : Type} (a b c : T) :
  a = b /\ b = c -> a = b /\ b = c /\ a = c.
Proof.
  intro h.
  destruct h as [h1 h2].
  apply (conj h1).
  
  Fail rapply (conj h2 _).
  Fail rapply (conj h2).
  eapply (conj h2 _).
  Unshelve.
  (* refine (transitive a b c (conj _ _)). *)
  (* apply (@transitive' _ a b). *)
  apply (transitive a b).
  apply (conj h1).
  apply h2.
  all : assumption.
Qed.

Lemma update_notation' {T : Type} (a b c : T) :
  a = b /\ b = c -> a = b /\ b = c /\ a = c.
Proof.
  intro h.
  destruct h as [h1 h2].
  eauto using transitive.
Qed.

Lemma update_notation'' {T : Type} (a b c : T) :
  a = b /\ b = c -> a = b /\ b = c /\ a = c.
Proof.
  intro h.
  destruct h as [h1 h2].
  intuition. (* now goal is "a = c" *)
  exact (transitive a b c (conj h1 h2)).
Qed.

Lemma test2 : forall a : Z,
  0%Z = (a - a)%Z.
Abort.

Locate "-".
(* "x - y" := Z.sub x y : Z_scope *)

(* open scope for integers *)
Local Open Scope Z_scope.

Lemma test2 : forall a : Z,
  0 = (a - a). (* Now we don't need %Z *)
Abort.

Lemma test : forall a b c : Z,
  (a = b = c) -> a + b - c = a.
Proof.
  intros.
  destruct H as [h1 h2].
  pose (h3 := transitive a b c (conj h1 h2)).
  subst a b.
(*   interesting thing happens:
  h3 := transitive c c c
        (conj eq_refl eq_refl) *)
  ring.
Qed.

Lemma test3 : forall a b c : Z,
  a * c - c = (a - 1) * c.
Proof.
  eauto with zarith.
  Show Proof.
(* (fun a _ c : Z =>
 legacy_pe_subproof a c) *)
(** what is legacy_pe_subproof? *)
Qed.























