Theorem succ_eq_implies_eq : forall (x y : nat),
  S x = S y -> x = y.
Proof.
  intros x y [= succ_eq]. assumption.
Qed.

Theorem succ_eq_implies_eq' : forall (x y : nat),
  S x = S y -> x = y.
Proof.
  intros x y succ_eq. inversion succ_eq. reflexivity.
Qed.