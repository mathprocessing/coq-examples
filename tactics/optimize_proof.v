Require Import Arith.

Lemma lt_to_gt : forall a b, 
  a < b -> b > a.
Proof.
  intros a b h.
  exact h.
Qed.

(* Nat.lt_stepr
x < y -> y = z -> x < z
0 < 1 -> 1 = n -> 0 < n *)

Lemma eq_one_to_gt_zero : forall n, n = 1 -> n > 0.
Proof.
  intros n h.
  refine ((Nat.lt_stepr _ _ _) _ _).
  exact Nat.lt_0_1.
  exact (eq_sym h).
Qed.

Lemma test (A B: Prop) : forall x : nat,
  (x = 1) \/ B -> B \/ (x > 0).
Proof.
  intro x.
  intuition eauto using eq_one_to_gt_zero.
  Show Proof.
  Optimize Proof. (* change nothing *)
  Show Proof.
Qed.


Lemma testOptimize (A B : Prop) :
  A -> (B \/ A) -> A.
Proof.
  intros ha hb.
  destruct hb.
  exact ha.
  exact ha.
  Optimize Proof.
(** (fun (A B : Prop) (ha : A) (hb : B \/ A) =>
 match hb with
 | or_introl _ | _ => ha
 end) *)
Qed.

Lemma testOptimize' (A B : Prop) :
  A -> (B \/ A) -> A.
Proof.
  intros ha hb.
  exact ha.
  Show Proof.
(**  (fun (A B : Prop) (ha : A) (_ : B \/ A) => ha) *)
Qed.






Print foil.



