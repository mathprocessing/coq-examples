Require Import Bool.
Require Import Arith.

Print "-".
(* Search (le ?a ?b -> _). *)
Search Nat.sub.

Fixpoint ble (n m : nat) : bool :=
match n, m with
| _, 0     => false
| 0, _     => true
| S x, S y => ble x y
end.

Example test_ble1 : ble 0 0 = false := eq_refl.
Example test_ble2 : ble 0 1 = true := eq_refl.
Example test_ble3 : ble 1 1 = false := eq_refl.
Example test_ble4 : ble 2 1 = false := eq_refl.

Require Import Coq.Program.Tactics.
Require Import Program.Wf.

(* Fixpoint divn_my (n m : nat) : nat.
exact (match n, m with
| _, 0 => 0
| x, 1 => x
| x, y => 
  if ble x y
  then 0
  else divn_my (x - y) y + 1
end).
Admitted. *)


Program Fixpoint divn_my (n m : nat) {measure (n)} : nat :=
match n, m with
| x, 0 => 0
| x, 1 => x
| 0, x => 0
| x, y => 
  if ble x y
  then 0
  else divn_my (x - y) y + 1
end.
Next Obligation.
assert (h : _) by exact (H0 m).
assert (h2 : ~ (n = 0)).
  intro H2. apply h. split; [exact (eq_sym H2)| exact eq_refl].
assert (h3 : n - m <= n /\ n - m <> n -> n - m < n) by admit.
apply h3.
split. apply Nat.le_sub_l.
intro h4.
assert (h5 : m = 0 <-> n - m = n) by admit.
apply h5 in h4.
assert (h6 : _) by exact (H1 n).
apply h6.
split.
reflexivity.
exact (eq_sym h4).
Admitted.


Check divn_my.



Example test_divn1 : divn_my 0 0 = 0 := eq_refl.
Example test_divn2 : divn_my 1 0 = 0 := eq_refl.
Example test_divn3 : divn_my 0 3 = 0 := eq_refl.
Example test_divn4 : divn_my 1 3 = 0 := eq_refl.
Example test_divn5 : divn_my 2 3 = 0 := eq_refl.
Example test_divn6 : divn_my 3 3 = 1 := eq_refl.
Example test_divn7 : divn_my 8 3 = 2 := eq_refl.
Example test_divn8 : divn_my 42 1 = 42 := eq_refl.
Example test_divn9 : divn_my 42 2 = 21 := eq_refl.
Example test_divn10 : divn_my 42 3 = 14 := eq_refl.
Example test_divn11 : divn_my 42 4 = 10 := eq_refl.



Fixpoint divn_my (n m : nat) {struct m} : nat :=
match n, m with
| _, 0 => 0
| x, 1 => x
| x, y => 
  match (ble x y) with
  | true  => 0
  | false => divn_my (x - y) y + 1
  end
end.