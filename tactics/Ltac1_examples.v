Ltac reduce_and_try_to_solve :=
  simpl; intros; auto.
 
Ltac destruct_bool_and_rewrite b H1 H2 :=
  destruct b; [rewrite H1; eauto | rewrite H2; eauto ].

(** Proof that the natural numbers have at least two elements *)
Lemma catd_nat : ~ exists x y : nat,
  forall z : nat, x = z \/ y = z.
Proof.
intros (x & y & Hz).
destruct (Hz 0), (Hz 1), (Hz 2).
            (* xxx *)
congruence. (* xxy *)
congruence. (* xyx *)
congruence. (* xyy *)
congruence. (* yxx *)
congruence. (* yxy *)
congruence. (* yyx *)
congruence. (* yyy *)
congruence. (* goal solved *)
Abort. (*we don't want to save lemma*)

Lemma catd_nat : ~ exists x y : nat,
  forall z : nat, x = z \/ y = z.
Proof.
intros (x & y & Hz).
destruct (Hz 0), (Hz 1), (Hz 2).
all: match goal with
     | _ : ?a = ?b,
       _ : ?a = ?c |- _ =>
       assert (b = c) by now transitivity a
     end.
all: discriminate.
Qed.

(** Proving that a list is a permutation of a second list *)
Section Sort.
  Variable A : Set.

  Inductive perm : list A -> list A -> Prop :=
  | perm_refl : forall l, perm l l
  | perm_cons : forall a l0 l1,
      perm l0 l1 -> perm (a :: l0) (a :: l1)
  | perm_append : forall a l,
      perm (a :: l) (l ++ a :: nil)
  | perm_trans : forall l0 l1 l2,
      perm l0 l1 -> perm l1 l2 -> perm l0 l2.

End Sort.

Require Import List.
Import ListNotations.

Check perm_refl nat [1;2;3].

Example test: rev [1;2;3] = [3;2;1] := eq_refl.


Ltac perm_aux n :=
  match goal with
  | |- (perm _ ?l ?l) => apply perm_refl
  | |- (perm _ (?a :: ?l1) (?a :: ?l2)) =>
    let newn := eval compute in (length l1) in
        (apply perm_cons; perm_aux newn)
  | |- (perm ?A (?a :: ?l1) ?l2) =>
    match eval compute in n with
    | 1 => fail
    | _ =>
        let l1' := constr:(l1 ++ a :: nil) in
        (apply (perm_trans A (a :: l1) l1' l2);
        [ apply perm_append | compute;
        perm_aux (pred n) ])
    end
  end.
  
(* "| |-" ~ all set of hypothsis *)
Ltac solve_perm :=
  match goal with
  | |- (perm _ ?l1 ?l2) =>
    match eval compute in (length l1 = length l2) with
    | (?n = ?n) => perm_aux n
    end
  end.

(* Require Import Coq.Program.Tactics.
 *)
Goal 1 = 1.
Proof.
  match goal with
  | |- (?a = ?a) => idtac " hello "; reflexivity
  end.
Qed.

Ltac contradict' H :=
  let save tac H := let x:=fresh in intro x; tac H; rename x into H
  in
  let negpos H := case H; clear H
  in
  let negneg H := save negpos H
  in
  let pospos H :=
    let A := type of H in (exfalso; revert H; try fold (~A))
  in
  let posneg H := save pospos H
  in
  let neg H := match goal with
   | |- (~_)       => idtac "negneg1: " H; negneg H
   | |- (_->False) => idtac "negneg2: " H; negneg H
   | |- _          => idtac "negpos: "  H; negpos H
  end in
  let pos H := match goal with
   | |- (~_)       => idtac "pospneg: "        H; posneg H
   | |- (_->False) => idtac "pospneg(false): " H; posneg H
   | |- _          => idtac "pospos: "         H; pospos H
  end in
  match type of H with
   | (~_)       => neg H
   | (_->False) => neg H
   | _          => (elim H;fail) || pos H
  end.


Goal forall A, ~ A -> A -> False.
Proof.
intros A nota a.
assert (b : forall P : Prop, P -> P).
Check (nota _).
change_no_check False.
refine (nota _).
contradict' a.
(* exact (nota a). *)
Abort.

Goal forall A B : Prop, ~ A -> ~B.
Proof.
intros.
contradict' H.
(* exact (nota a). *)
Abort.

Goal forall n, n = 0 -> 0 = n.
Proof.
intro n.
case n.
exact id.
discriminate.
Show Proof.
Qed.

Ltac destruct_all' t :=
 match goal with
  | x : t |- _ => destruct x; destruct_all' t
  | _          => idtac "do nothing"
 end.

Notation anyplus := (_ + _).

Ltac split' :=
  match goal with
  | |- context[anyplus] => idtac "anyplus"
  | |- (?f ?a ?b) => idtac "split:"f a b; apply conj
  | _              => idtac "general case"
  end.
  
Goal forall A B : Prop, (A -> A) /\ (A -> B).
Proof.
intros.
split'.
cut (A -> A).
all: try exact id.
Abort.

(** work with hypothisis: get type *)
Ltac get_type H :=
  match type of H with
    | ?typeOfH => idtac "type(H): " typeOfH
  end.


Goal forall A B, A \/ B -> B \/ A.
Proof.
intros.
get_type H.
assert (B \/ A).
destruct_all' (A \/ B).


Lemma perm_reverse (A : Set): forall l : list A,
  perm _ l (rev l).
Proof.
  intro l.
  Check perm_ind.
  refine (perm_ind
    (forall l, perm A l (rev l))
    _
    _
    _
    _
    _
    _
    _
  ).
  
  
  
  exact (perm_refl A).
  intros.
  apply (perm_cons _ _ _ _ H).
  apply perm_append.
  intros.
  eapply (perm_trans A l0 l1 l2 H H1).
  
  
Qed.






























