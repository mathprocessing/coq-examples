(* https://coq.discourse.group/t/hint-databases-as-argument/966/5 *)

(* DOCS: https://coq.inria.fr/refman/proof-engine/tactics.html#automation *)
(* From Ltac2 Require Import Ltac2 Ident. this line disable debug for auto tactic *)

(* Require Import Coq.Program.Tactics. *)

Create HintDb base.
Create HintDb zero_rewriters.
Create HintDb zero_rewriters_min.
Create HintDb one_rewriters.
Create HintDb plus.

Axiom add_n_Sm : forall n m, n + S m = S (n + m).
Axiom add_comm : forall n m, n + m = m + n.

Hint Resolve add_n_Sm : plus.
Hint Resolve add_comm : plus.
(* Print HintDb plus. *)

Hint Resolve eq_refl : base.
Hint Resolve f_equal_nat : base.
Hint Resolve f_equal2_nat : base.

Hint Resolve mult_n_O : zero_rewriters_min.
Hint Resolve plus_O_n : zero_rewriters_min.

Hint Resolve mult_n_O : zero_rewriters.
Hint Resolve plus_n_O : zero_rewriters.
Hint Resolve plus_O_n : zero_rewriters.


Axiom mult_n_1 : forall n, n = n * 1.
Axiom mult_1_n : forall n, n = 1 * n.

Hint Resolve mult_n_1 : one_rewriters.
Hint Resolve mult_1_n : one_rewriters.

Goal forall n (f : nat -> nat), f n = f (n + 0).
Proof.
(*   solve[ auto with nocore base zero_rewriters ]. *)
   debug auto with nocore base zero_rewriters.
Qed.


Goal forall n (f : nat -> nat), f n = f (n + 0).
Proof.
   debug auto with nocore base zero_rewriters_min plus. (* change nothing *)
   intros.
   rewrite add_comm.
   exact eq_refl.
Qed.

Ltac pattern_matching_rewriter :=
  match goal with
  |[ |- context[0] ] => solve[auto with nocore zero_rewriters]
  |[ |- context[_ + _]] => solve[auto with nocore plus]
  | _ => first[solve[timeout 5 auto] | idtac "Finding a proof takes too long. Try taking a smaller step."]
  end.

Goal 1 + 2 = 2 + 1.
Proof.
match goal with 
| |- 1 + 2 = ?right => idtac "right_side:"right
end.

Fail match goal with 
| |- context[_ * _] => apply add_comm
end.


(* match goal with 
| |- context[_ + _] => apply add_comm
end. *)

pattern_matching_rewriter.
Qed.

Goal forall A B C : Prop, A /\ B -> C.
Proof.
intros.
match type of H with
  | ?type_of_H => idtac "type(H): " type_of_H
end.


match goal with
  | hname : ?htype |- ?goal => 
        idtac "1) goal:" goal
     || idtac "hname:" hname", htype:" htype || fail
  | hname : ?htype |- ?goal => 
        idtac "2) goal:" goal
     || idtac "hname:" hname", htype:" htype
end.



Qed.



let H := 
  match goal with H : n = n
   |- _ => H
  end











