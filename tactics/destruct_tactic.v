Inductive bool: Set :=
  | true
  | false.

Definition not (b: bool) : bool :=
  match b with
    | true => false
    | false => true
  end.

Lemma not_not_x_equals_x:
  forall b, not (not b) = b.
Proof.
  intro.
  destruct b.
  - reflexivity.
  - reflexivity.
Qed.

Definition and (x y : bool) : bool :=
  match x with true => y | _  => false end.
  
Definition and' (x y : bool) : bool :=
  match y with true => x | _  => false end.
  
Lemma f_eq {T : Type} :
  forall f g : T -> T, (forall x, f x = g x) -> f = g.
Proof. Admitted.

Lemma f_eq_two_arg {T : Type} : 
  forall f g : T -> T -> T, (forall x y, f x y = g x y) -> f = g.
Proof. Admitted.

Lemma and_def_eq: and' = and.
Proof.
  eapply f_eq_two_arg.
  intros; destruct x, y; reflexivity.
Qed.

Ltac rewrite_and_simpl H :=
  (rewrite -> H || rewrite <- H ); simpl; try reflexivity.

Lemma and_comm:
  forall a b, and a b = and b a.
Proof.
  intros.
  destruct a.
  all: rewrite_and_simpl and_def_eq.
  all: rewrite_and_simpl and_def_eq.
  Show Proof.
Qed.



