(* We duplicate the goal for comparing different proofs. *)

(* use dup n. *)

(* unshelve epose proof _ as H2.
https://stackoverflow.com/questions/55883274/duplicate-subgoals-in-coq *)

Ltac distinct_subgoals_tactical tac :=
  let H := fresh in
  unshelve epose proof _ as H;
  [ |
  | tac;
    [ (* first split apart the hypothesis containing the conjunction of all previous subgoals *)
      repeat match type of H with prod _ _ => destruct H as [? H] end;
      (* see if any of them solve the goal *)
      try assumption;
      (* otherwise, add another conjunct to the hypothesis and use the first part of it to solve the current goal *)
      refine (fst H) .. ] ];
    [ exact True (* fill the final conjunct with True *)
    | repeat apply pair; try exact I .. ].
    
    
(** TODO: write example for demonstrate behaviour of this tactic. *)


Check and_ind.
Example wrong_and_ind : forall A B P : Prop, (A -> B -> P) -> A /\ (B -> P).
Proof.
intros.
split.
all: swap 1 2.
apply H.
(** we have duplicated goals, solution:
1. how we eliminate one of two goals, or squash it. (1: A, 2: A) => (1: A).
2. we have fact: if we can proof this one -> we proof another one. 
In this case we can't proof A. *)
Abort.