Require Import Lia.

(* lia proves arithmetic goals with equalities and inequalities, but it does not support multiplication. *)

(* ring proves arithmetic goals with multiplications, but does not support inequalities. *)

(* https://softwarefoundations.cis.upenn.edu/plf-current/UseAuto.html *)
(* lia demos *)

Lemma lia_demo_1 : forall (x y : nat),
  (y <= 4) ->
  (x + x + 1 <= y) ->
  (x <> 0) ->
  (x = 1).
Proof. intros. lia. Qed.

Lemma lia_demo_2 : forall (x y z : nat),
  (x + y = z + z) ->
  (x - y <= 4) ->
  (x - z <= 2).
Proof. intros. lia. Qed.

Lemma lia_demo_3 : forall (x y : nat),
  (x + 5 <= y) ->
  (y - x < 3) ->
  False.
Proof. intros. lia. Qed.


Print lia_demo_1.