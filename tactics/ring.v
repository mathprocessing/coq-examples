Require Import Arith.
Import Nat.

Require Import Reals.
Open Scope R_scope.

Lemma eq1 : forall a b,
  (a = b) -> (a + b) * (a + b) =
  4 * a * a.
Proof.
  intros.
  intuition (subst; ring).
Qed.

Print eq1.


(* 
a / (a + b) + b / (a - b) =
a (a - b) / (a + b) (a - b) +
+ b (a + b) / (a - b) (a + b) =

(a (a - b) + b (a + b)) / (a - b) (a + b) =
(a * a + b * b) / (a - b) (a + b) =

(a^2 + b^2) / (a^2 - b^2) =
1 + (2 * b^2) / (a^2 - b^2) *)

Lemma eq2 : forall a : R,
  a^10 = a*a*a*a*a*a*a*a*a*a.
Proof.
  intros.
  unfold "^".
  ring.
Qed.


Search (Rabs ?a = _).

Lemma helper1 (a b: R): 
  Rabs a <> Rabs b ->
  a <> b /\ a <> -b.
Proof.
  intros h.
  split; intro h2; subst a.
  + contradiction.
  + destruct h.
    exact (Rabs_Ropp b).
Qed.

Lemma helper2 (x y : R) :
  (x * y = 0) -> x = 0 \/ y = 0.
Proof.
(* write tactic to prove this *)
Admitted.


Lemma helper3 (a b : R) :
  a - b <> 0 <-> a <> b.
Proof.
(* write tactic to prove this *)
Admitted.

Lemma helper4 (a b : R) :
  a + b <> 0 <-> a <> -b.
Proof.
(* write tactic to prove this *)
Admitted.


Lemma add_number {a b : R}(c : R) :
  a = b -> a + c = b + c.
Proof.
  intro h.
  subst a.
  reflexivity.
Qed.

Lemma eq3 : forall a b : R,
  Rabs a <> Rabs b ->
  (a / (a + b) + b / (a - b)) = 
  1 + (2 * b^2) / (a^2 - b^2).
Proof.
  intros.
  field.
  apply helper1 in H.
  destruct H as [h1 h2].
  repeat split.
  ** intro h3.
     replace (a^2 - b^2) with ((a + b)* (a - b)) in h3 by ring.
     apply helper2 in h3.
     apply helper4 in h2.
     destruct h3.
     + contradiction.
     + apply helper3 in h1.
       contradiction.
  ** Check (conj (helper3 a b) h1).
     pose (
       H := let H1 : a <> b -> a - b <> 0 :=
       match helper3 a b with
         | conj _ x0 => x0
       end in H1 h1).
      (*  previous line ~ apply (helper3 _ _). *)
     exact H.
  ** intro H.
     pose (h3 := add_number (-b) H).
     (*  field_simplify in h3.  Why this poop not works!!!*)
     field_simplify in h3.
     replace (a + b + - b) with a in h3 by ring.
     field_simplify in h3.
     contradiction.
(*     subst a.
    intro h4.
  - intuition ring.
  - intuition ring.
(*   field_simplify. reflexivity.
  pose (contr_h2 := H0 H).
  contradiction.
  destruct h1.
  destruct h2. *)
  destruct h1.
  replace b with (0 + b) by ring.
  replace a with (a - b + b) by ring.
  apply (add_number b H).
  apply helper3.
  all:  swap 1 2.
  apply helper4.
  all:  swap 1 2.
  all: apply helper1 in H; intuition. *)
Qed.