
(** default goal names that we can see
  in exact proof is Goal, Goal0, Goal1,..
  is terrible *)
Ltac name_goal name := refine ?[name].

Goal forall n, n + 0 = n.
Proof.
  induction n; 
    [ name_goal base | name_goal step ].
  [base] : { reflexivity. }
  [step] : {
    simpl. (** simpl do nothing in proof
    works like "change" tactic *)
    f_equal.
    exact IHn.
  }
Qed.


Goal forall A B : Prop, A -> B -> A /\ B.
Proof.
intros A B ha hb.
split; [name_goal first | name_goal second].
Show Proof.
(* (fun (A B : Prop) (ha : A) (hb : B) =>
 conj ?name ?second) *)
(* look at "?name" != "?first" *)
all: assumption.
Qed.
Fail Check first.

Goal forall A B : Prop, A -> B -> A /\ B.
Proof.
intros A B ha hb.
split; [name_goal first_ | name_goal second_].
Show Proof.
(* (fun (A B : Prop) (ha : A) (hb : B) =>
 conj ?first_ ?second_) *)
(** Problem fixed? Or maybe not by good way? *)
all: assumption.
Qed.

Goal exists n, n = 0.
Proof.
  eexists ?[subgoal_name].
  Show subgoal_name.
  Fail Check subgoal_name.
  Check eq_refl.
  exact (eq_refl 0).
Qed.


Show Match nat.
(**

match # with
 | O =>
 | S x =>
 end

*)

Require Import List.
Show Match list.
(**

match # with
 | nil =>
 | cons x x0 =>
 end

 *)


Lemma test1 A B : A /\ B -> A.
Proof.
  intros [ha hb]. exact ha.
Qed.

Lemma test2 A B : A /\ B -> A.
Proof.
  intros (ha & hb). exact ha.
Qed.

Lemma test3 A B : A /\ B -> A.
Proof.
  intros (ha, hb). exact ha.
Qed.

Goal forall A B, A -> B -> B.
Proof.
intros A B.
info_auto 1.
Qed.

(* info auto:
intro.
intro.
assumption. *)

Create HintDb my.
Hint Resolve ex_intro : my.

Goal forall A B, A \/ B -> B.
Proof.
debug eauto.
Abort.
(* debug eauto:
1 depth=5 
1.1 depth=5 intro
1.1.1 depth=5 intro
1.1.1.1 depth=5 intro *)
































