(* https://softwarefoundations.cis.upenn.edu/plf-current/UseAuto.html *)


Lemma search_depth_0 :
  True /\ True /\ True /\ True /\ True /\ True.
Proof.
  debug auto.
  (** how to save progrees that make auto tactic? *)
Abort.


Lemma search_depth_1 : forall (P : nat->Prop),
  P 0 ->
  (P 0 -> P 1) ->
  (P 1 -> P 2) ->
  (P 2).
Proof.
  debug auto 0. (* does not find the proof *)
  debug auto 1. (* does not find the proof *)
  debug auto 2. (* does not find the proof *)
  debug auto 3. (* finds the proof *)
          (* more generally, auto n solves the goal if n ≥ 3 *)
Qed.