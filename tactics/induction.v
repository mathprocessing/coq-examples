Theorem n_plus_n : forall (n : nat),
  n + n = n * 2.
Proof.
  induction n as [| x IH].
  - reflexivity.
  - simpl. rewrite <- IH.
    simple apply f_equal_nat.
    simple apply eq_sym.
    simple apply plus_n_Sm.
(*   used: info_auto. *)
Qed.