(* https://coq.inria.fr/refman/proof-engine/ltac.html#coq:tacn.solve *)

Goal forall A B : Prop, A -> B -> (A->B).
https://coq.inria.fr/refman/proof-engine/ltac.html#ltac-examples