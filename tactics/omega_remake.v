Require Import ZArith.
(* or Require Import Omega. *)

(* https://softwarefoundations.cis.upenn.edu/plf-current/UseAuto.html *)
From Coq Require Import Lia.

Lemma odds_arent_even:
forall a b: nat, 2*a + 1 <> 2*b.
Proof.
  intros.
  omega.
  Show Proof.
Qed.

Example ex1: Z.of_nat 3 = 3%Z := eq_refl.
Example ex2: 1 = 1 := eq_refl.

Check Nat2Z.inj_add.

(** Nat2Z.inj_add : forall n m : nat, (n + m) = (n + m)%Z *)



(**
Lemma omega4: fun (x : Z) (Omega4 : a = x /\ (0 <= x * 1 + 0)%Z)
1. this help to establish fact that a * 1 + 0 >= 0
2. prove: 2 * a + 1 + - (2 * b))%Z = 0%Z -> False

*)

Check fast_Zmult_comm. 
(** forall (x y : Z) (P : Z -> Prop), 
      P (y * x)%Z -> P (x * y)%Z *)
Check fast_Zopp_mult_distr_r.
(** forall (x y : Z) (P : Z -> Prop),
       P (x * - y)%Z -> P (- (x * y))%Z *)
       
Check fast_OMEGA11.
(* forall (v1 c1 l1 l2 k1 : Z) (P : Z -> Prop),
       P (v1 * (c1 * k1) + (l1 * k1 + l2))%Z ->
       P ((v1 * c1 + l1) * k1 + l2)%Z *)

Check Zegal_left. (* forall n m : Z, n = m -> (n + - m)%Z = 0%Z *)


Check inj_eq. (* forall x y : nat, x = y -> Z.of_nat x = Z.of_nat y *)

(**
(fun (a b : nat) (H : 2 * a + 1 = 2 * b) =>

 (fun (P : Z -> Prop) (H0 : P ((2 * a) + 1)%Z)
  => eq_ind_r P H0 (Nat2Z.inj_add (2 * a) 1))
   
   
   (fun x : Z => x = 2 * b -> False)
   
   (
     (fun (P : Z -> Prop) (H0 : P (2 * a)%Z) => eq_ind_r P H0 (Nat2Z.inj_mul 2 a))
   
     (fun x : Z => (x + 1)%Z = (2 * b) -> False)
      ((fun (P : Z -> Prop) (H0 : P (2 * b)%Z) => eq_ind_r P H0 (Nat2Z.inj_mul 2 b))
         (fun x : Z => (2 * a + 1)%Z = x -> False)
         (fun H0 : (2 * a + 1)%Z = (2 * b)%Z =>
          ex_ind
          
            (fun (Zvar0 : Z) (Omega4 : a = Zvar0 /\ (0 <= Zvar0 * 1 + 0)%Z) =>
             and_ind
               (fun (Omega0 : a = Zvar0) (_ : (0 <= Zvar0 * 1 + 0)%Z) =>
                ex_ind
                  (fun (Zvar1 : Z) (Omega3 : b = Zvar1 /\ (0 <= Zvar1 * 1 + 0)%Z) =>
                   and_ind
                     (fun (Omega1 : b = Zvar1) (_ : (0 <= Zvar1 * 1 + 0)%Z) =>
                      (fun (P : Z -> Prop) (H1 : P Zvar0) => eq_ind_r P H1 Omega0)
                      
                        (fun x : Z => (2 * x + 1 + - (2 * b))%Z = 0%Z -> False)
                        (fast_Zmult_comm 2 Zvar0 (fun x : Z => (x + 1 + - (2 * b))%Z = 0%Z -> False)
                           ((fun (P : Z -> Prop) (H1 : P Zvar1) => eq_ind_r P H1 Omega1)
                              (fun x : Z => (Zvar0 * 2 + 1 + - (2 * x))%Z = 0%Z -> False)
                              (fast_Zmult_comm 2 Zvar1 (fun x : Z => (Zvar0 * 2 + 1 + - x)%Z = 0%Z -> False)
                                 (fast_Zopp_mult_distr_r Zvar1 2 (fun x : Z => (Zvar0 * 2 + 1 + x)%Z = 0%Z -> False)
                                    (fast_Zplus_comm (Zvar0 * 2 + 1) (Zvar1 * -2) (fun x : Z => x = 0%Z -> False)
                                       (fun Omega2 : (Zvar1 * -2 + (Zvar0 * 2 + 1))%Z = 0%Z =>
                                        let H1 : (1 > 0)%Z := eq_refl in
                                        (let H2 : (2 > 1)%Z := eq_refl in
                                         (fun (auxiliary_2 : (2 > 1)%Z) (auxiliary_1 : (1 > 0)%Z) =>
                                          (fun auxiliary : ... -> False => auxiliary (fast_OMEGA11 Zvar1 ... ... 1 2 ... ...))
                                            (OMEGA4 1 2 (Zvar1 * -1 + (...)) auxiliary_1 auxiliary_2)) H2) H1))))))
                        (Zegal_left (2 * a + 1) (2 * b) H0)) Omega3) (intro_Z b)) Omega4) (intro_Z a))))
   (inj_eq (2 * a + 1) (2 * b) H))
*)


Local Open Scope Z_scope.

Lemma odds_arent_even_for_integers:
forall a b: Z, 2*a + 1 <> 2*b.
Proof.
  intros.
  Check fast_OMEGA11 b (-1) (a * 1 + 0) 1 2.
  omega.
  Show Proof. (* proof for integers much simplier then for natural numbers*)
Qed.

(**
(fun (a b : Z) (H : 2 * a + 1 = 2 * b) =>


 fast_Zmult_comm 2 a (fun x : Z => x + 1 + - (2 * b) = 0 -> False)
   (fast_Zmult_comm 2 b (fun x : Z => a * 2 + 1 + - x = 0 -> False)
      (fast_Zopp_mult_distr_r b 2 (fun x : Z => a * 2 + 1 + x = 0 -> False)
         (fast_Zplus_comm (a * 2 + 1) (b * -2) (fun x : Z => x = 0 -> False)
            (fun Omega0 : b * -2 + (a * 2 + 1) = 0 =>
             let H0 : 1 > 0 := eq_refl in
             (let H1 : 2 > 1 := eq_refl in
              (fun (auxiliary_2 : 2 > 1) (auxiliary_1 : 1 > 0) =>
               (fun auxiliary : (b * -1 + (a * 1 + 0)) * 2 + 1 = 0 -> False =>
                auxiliary
                  (fast_OMEGA11 b (-1) (a * 1 + 0) 1 2 
                     (fun x : Z => x = 0)
                     (fast_OMEGA11 a 1 0 1 2 (fun x : Z => b * (-1 * 2) + x = 0)
                        Omega0)))
                 (OMEGA4 1 2 (b * -1 + (a * 1 + 0)) auxiliary_1 auxiliary_2)) H1)
               H0)))) (Zegal_left (2 * a + 1) (2 * b) H))
 *)

Lemma odds_arent_even_for_integers_solved_by_lia:
  forall a b: Z, 2*a + 1 <> 2*b.
Proof.
  intros.
  lia.
  Show Proof.
Qed.
(**
(fun a b : Z =>
 let __arith : forall __x2 __x1 : Z, 2 * __x1 + 1 = 2 * __x2 -> False :=
   fun __x2 __x1 : Z =>
   let __wit :=
     (ZMicromega.CutProof
        (RingMicromega.PsatzMulC (EnvRing.Pc (-1)) (RingMicromega.PsatzIn Z 0))
        (ZMicromega.CutProof (RingMicromega.PsatzIn Z 0) ZMicromega.DoneProof)
      :: nil)%list in
   let __varmap := VarMap.Branch (VarMap.Elt __x2) __x1 VarMap.Empty in
   let __ff :=
     Tauto.N
       (Tauto.A
          {|
          RingMicromega.Flhs := EnvRing.PEadd
                                  (EnvRing.PEmul (EnvRing.PEc 2) (EnvRing.PEX 1))
                                  (EnvRing.PEc 1);
          RingMicromega.Fop := RingMicromega.OpEq;
          RingMicromega.Frhs := EnvRing.PEmul (EnvRing.PEc 2) (EnvRing.PEX 2) |}
          tt) in
   ZMicromega.ZTautoChecker_sound __ff __wit
     (eq_refl <: ZMicromega.ZTautoChecker __ff __wit = true)
     (VarMap.find 0 __varmap) in
 __arith b a) *)
