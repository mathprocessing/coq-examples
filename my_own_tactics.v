Section Props.

Lemma test {A B C : Prop} :
  A /\ B /\ C -> C /\ B /\ A.
Proof.
intros h.
destruct h as [h2 h].
destruct h as [h3 h].
repeat split.
exact h. exact h3. exact h2.
Qed.

Variable T : Type.
Variable f : T -> Prop.

Definition test2 (x : T):
  (f x) /\ False /\ True ->
  True /\ False /\ (f x) := test.

Print test2.

End Props.

Ltac refl := reflexivity.

Lemma test3 : forall x y : nat,
  (x + y) * (x + y) = (x * x + 2 * x * y + y * y).
Proof.
intros x y.
pose (z := x + y).
replace ((x + y) * z) with (x * z + y * z).
(* replace not works, let's write my own tactic proper_replace, that can do it thing *)
Abort.
Require Import PeanoNat.
Import Nat.


Lemma test4 : forall x y : nat,
  (x + y) * (x + y) = (x * x + 2 * x * y + y * y).
Proof.
intros x y.
pose (z := x + y).
replace ((x + y) * (x + y)) 
  with (x * (x + y) + y * (x + y)).
replace (x * (x + y)) with
  (x * x + x * y).
replace (y * (x + y)) with
  (y * x + y * y).
replace (x * x + x * y + (y * x + y * y)) with
  (x * x + x * y + y * x + y * y).
replace (y * x) with (x * y).
replace (x * x + x * y + x * y + y * y) with 
  (x * x + (x * y + x * y) + y * y).
replace (x * y + x * y) with (2 * x * y).
refl.
all: swap 1 3.
apply mul_comm.
rewrite add_assoc. refl.
Show Proof.
rewrite mul_succ_l, 
  mul_1_l, mul_add_distr_r. refl.
rewrite add_assoc. refl.
(** define distributive set of actions *)
rewrite mul_add_distr_l. refl.
rewrite mul_add_distr_l. refl.
(** end of distributive set *)
Show Proof.
Check eq_ind.
Check eq_ind_r.



Admitted.

Require Import Logic.

Definition f x y := 
match (x, y) with
| (0, 0) => 0
| (_, _) => 1
end.

Definition f2 x y := 
match x + 1, y as p return nat with
| 0, 0 => 1
| m, n => m + n
end.

Print f2.

Definition add (p : option nat * option nat) :=
match p with
| (Some x, Some y) => Some (x + y)
| (_     , _     ) => None
end.

Definition optpair (x y : nat) :=
  (Some x, Some y).
  
Eval compute in add (optpair 5 6).
Eval compute in add (Some 6, None).

Definition test' : 1 = 1 := eq_refl.

Definition add_none :
  add (Some 1, None) = None := eq_refl.

Definition G {T : Type}(first second : T) : T :=
second.

(*function that return first argument *)
Definition F {T : Type}{first : T} : T -> T.
intro second.
apply G.
(* no matters what value in this line *)
exact second.
exact first.
Defined.

Variable sometype : Type.
Variable x y : sometype.

Eval compute in F y. (* ?first : sometype *) 
Check @F nat. (* nat -> nat -> nat *)
Eval compute in @F nat 1 2. (* 1 : nat *)

Print F.








