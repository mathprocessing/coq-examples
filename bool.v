Inductive bool : Type :=
| true : bool
| false : bool.


Goal forall A B : Prop, (A -> B) -> ~ B -> ~ A.
Proof.
  intros A B h b.
  contradict b. (* works like contrapose in lean prover *)
  exact (h b).
Qed.


Definition prop_cases (A : Prop) : or A (~ A).
Proof.
  Check and A A.
Admitted.


Definition bool_cases (x : bool) : x = true \/ x = false.
Proof.

Check or_ind.

  
Admitted.

Let example := bool_cases true.
Check example.

Definition example_introl : true = true \/ 1 = 1 := or_introl eq_refl.

Definition bool_cases_prop (x : bool) (P : Prop) : 
  P -> (x = true /\ P) \/ (x = false /\ P).
Proof.
  intros hp.
  assert (H : forall A B, A \/ B -> A /\ P \/ B /\ P) by tauto.
  exact (  H (x = true) (x = false) (bool_cases x)  ).
Qed.






(** List of bools *)
Inductive listOfBool : Type :=
| empty : listOfBool
| cons : bool -> listOfBool -> listOfBool.

Check cons true empty.
Check cons false (cons true empty).

Fixpoint length(list : listOfBool) : nat :=
match list with
| empty     => 0 : nat
| cons _ xs => S (length xs)
end.


Definition length' : listOfBool -> nat :=
(fun list : listOfBool =>
   match list with
   | empty     => 0 : nat
   | cons _ xs => S (length xs)
   end).


Definition empty_eq : empty = empty := eq_refl.
Definition empty_zero : length empty = 0 := eq_refl.
Definition empty_zero' : length' empty = 0 := eq_refl.


Definition one_element' (value : bool) : (fun b : bool => length (cons b empty)) value = 1 := eq_refl.
Definition one_element(value : bool) : length (cons value empty) = 1 := eq_refl.


Parameter xs : listOfBool.
Lemma T1 : length xs = 0 -> xs = empty.
Proof.
  exact (
    fun h => _
  ).
Qed.







