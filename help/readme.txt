# How to define a function inductively on two arguments in Coq?
https://cstheory.stackexchange.com/questions/1807/how-to-define-a-function-inductively-on-two-arguments-in-coq

Answer:
Fixpoint pair l := fix pair1 (r : Tree) :=
  match l with
    | Tip => match r with
              | Tip => TipTip
              | Bin rl rr => TipBin rl rr
            end
    | Bin ll lr => match r with
                    | Tip => BinTip ll lr
                    | Bin rl rr => BinBin (pair1 rl) (pair lr r)
                   end
  end.