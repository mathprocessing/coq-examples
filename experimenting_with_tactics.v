Require Import Reals.
Goal forall x y: R,
(x * y > 0)%R ->
(x * (1 / x + x / (x + y)))%R =
((-1 / y) * y * (-x * (x / (x + y)) - 1))%R.
Proof.
intros.
field.
Abort.

Variable P : nat -> Prop.
Goal P 1 /\ P 2 /\ P 3 /\ P 4.
Proof.
repeat split.
all: cycle -1.
all: swap 1 2.
Abort.


Goal P 1 /\ P 2 /\ P 3 /\ P 4.
Proof.
repeat split.
all: revgoals.
Abort.

Goal exists n, n = 0.
Proof.
refine (ex_intro _ _ _).
unshelve auto.
Qed.

(* Wrong example *)
Goal 1 = 1.
Proof.
change_no_check True.
exact I.
Abort.

(* Right example *)
Goal 2 = 2.
Proof.
change_no_check ((fun X => X + 1) 1 = 2).
reflexivity.
Qed.


(** --------------------- *)

Goal True -> False.
Proof.
intro H.
change_no_check False in H.
exact H.
Qed.














