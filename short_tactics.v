Module Short.

Ltac refl := reflexivity.

Goal 1 = 1.
refl.
Abort.

End Short.

Export Short.

(** 
How to write new module?
*)