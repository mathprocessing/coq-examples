(* Preloaded.v

   The key definitions and notations presented in this file are
   adapted from the Software Foundations series, an excellent
   resource for learning Coq:
   https://softwarefoundations.cis.upenn.edu/current/index.html

   The copyright notice of the series is reproduced below as
   follows:

   Copyright (c) 2019

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE. *)

(* Volume 3: Verified Functional Algorithms *)

(* Basic Techniques for Permutations and Ordering (Perm) *)

From Coq Require Import Lists.List Arith.Arith.
Import ListNotations.
Require Import Permutation.

(* Insertion Sort (Sort) *)

Inductive sorted: list nat -> Prop := 
| sorted_nil:
    sorted nil
| sorted_1: forall x,
    sorted (x::nil)
| sorted_cons: forall x y l,
   x <= y -> sorted (y::l) -> sorted (x::y::l).

Definition is_a_sorting_algorithm (f: list nat -> list nat) :=
  forall al, Permutation al (f al) /\ sorted (f al).

(* Additional definitions used in this Kata *)

Fixpoint merge (gas : nat) (l1 l2 : list nat) : list nat :=
  match gas with
  | O => []
  | S gas' => match l1, l2 with
    | [], _ => l2
    | _, [] => l1
    | x1 :: xs1, x2 :: xs2 => if x1 <=? x2
        then x1 :: merge gas' xs1 l2
        else x2 :: merge gas' l1 xs2
    end
  end.

Fixpoint mergesort_aux (gas : nat) (l : list nat)
  : list nat :=
  match l with
  | _ :: _ :: _ => match gas with
    | O => l
    | S gas' => let size := length l in
        let n := Nat.div2 size in
        merge size (mergesort_aux gas' (firstn n l))
          (mergesort_aux gas' (skipn n l))
    end
  | _ => l
  end.

Definition mergesort l := mergesort_aux (length l) l.