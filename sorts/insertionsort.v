(* https://softwarefoundations.cis.upenn.edu/vfa-current/Sort.html *)
Require Import Unicode.Utf8.
From Coq Require Import Lists.List Arith.Arith.
Import ListNotations.
Require Import Permutation.

(* insert i l inserts i into its sorted place in list l.
   Precondition: l is sorted. *)
Fixpoint insert (i : nat) (l : list nat) :=
  match l with
  | [] => [i]
  | h :: t => if i <=? h then i :: h :: t else h :: insert i t
  end.
Fixpoint sort (l : list nat) : list nat :=
  match l with
  | [] => []
  | h :: t => insert h (sort t)
  end.
Example sort_pi :
  sort [3;1;4;1;5;9;2;6;5;3;5]
  = [1;1;2;3;3;4;5;5;5;6;9].
Proof. simpl. reflexivity. Qed.

Inductive sorted : list nat → Prop :=
| sorted_nil :
    sorted []
| sorted_1 : ∀ x,
    sorted [x]
| sorted_cons : ∀ x y l,
    x ≤ y → sorted (y :: l) → sorted (x :: y :: l).
    
Check nth : ∀ A : Type, nat → list A → A → A.
Check nth_error : ∀ A : Type, list A → nat → option A.

Definition sorted'' (al : list nat) := ∀ i j,
    i < j < length al →
    nth i al 0 ≤ nth j al 0.

Definition sorted' (al : list nat) := ∀ i j iv jv,
    i < j →
    nth_error al i = Some iv →
    nth_error al j = Some jv →
    iv ≤ jv.

Definition is_a_sorting_algorithm (f: list nat → list nat) := ∀ al,
    Permutation al (f al) ∧ sorted (f al).

(** Proof of Correctness *)

Lemma insert_perm: ∀ x l,
    Permutation (x :: l) (insert x l).
Proof. Admitted.

(** Prove that sort is a permutation, using insert_perm. *)
Theorem sort_perm: ∀ l, Permutation l (sort l).
Proof. Admitted.

Theorem insertion_sort_correct:
    is_a_sorting_algorithm sort.
Proof. Admitted.

(** Validating the Specification (Advanced) *)

Lemma sorted_sorted': ∀ al, sorted al → sorted' al.
Proof. Admitted.

Lemma sorted'_sorted : ∀ al, sorted' al → sorted al.
Proof. Admitted.

(** Proving Correctness from the Alternative Spec *)

Lemma nth_error_insert : ∀ l a i iv,
    nth_error (insert a l) i = Some iv →
    a = iv ∨ ∃ i', nth_error l i' = Some iv.
Proof. Admitted.

Lemma insert_sorted':
  ∀ a l, sorted' l → sorted' (insert a l).
Proof. Admitted.

Theorem sort_sorted': ∀ l, sorted' (sort l).
Proof.
  induction l.
  - unfold sorted'. 
    intros. 
    destruct i; 
    inversion H0.
  - simpl. 
    apply insert_sorted'.
    assumption.
Qed.







