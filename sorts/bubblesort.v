(* Preloaded.v

   The key definitions and notations presented in this file are
   adapted from the Software Foundations series, an excellent
   resource for learning Coq:
   https://softwarefoundations.cis.upenn.edu/current/index.html

   The copyright notice of the series is reproduced below as
   follows:

   Copyright (c) 2019

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE. *)

(* Volume 3: Verified Functional Algorithms *)

(* Basic Techniques for Permutations and Ordering (Perm) *)

From Coq Require Import Lists.List.
Import ListNotations.
Require Import Permutation.

Notation "a >? b" := (Nat.ltb b a)
  (at level 70, only parsing) : nat_scope.

(* Insertion Sort (Sort) *)

Inductive sorted: list nat -> Prop := 
| sorted_nil:
    sorted nil
| sorted_1: forall x,
    sorted (x::nil)
| sorted_cons: forall x y l,
   x <= y -> sorted (y::l) -> sorted (x::y::l).

Definition is_a_sorting_algorithm (f: list nat -> list nat) :=
  forall al, Permutation al (f al) /\ sorted (f al).

(* Additional definitions used in this Kata *)

Fixpoint bubblesort_pass (l : list nat) : list nat * bool :=
  match l with
  | [] =>
      ([], false)
  | x :: xs =>
      match bubblesort_pass xs with
      | ([], b) => ([x], b)
      | (x' :: xs', b) =>
          if x >? x'
            then (x' :: x :: xs', true)
            else (x :: x' :: xs', b)
      end
  end.

Fixpoint bubblesort_aux (gas : nat) (l : list nat) : list nat :=
  match gas with
  | O => l
  | S gas' => match bubblesort_pass l with
    | (x :: xs, true) => x :: bubblesort_aux gas' xs
    | _ => l
    end
  end.

Definition bubblesort (l : list nat) :=
  bubblesort_aux (pred (length l)) l.