(* https://softwarefoundations.cis.upenn.edu/vfa-current/BagPerm.html *)
From Coq Require Import Strings.String. (* for manual grading *)
From Coq Require Import Setoid Morphisms.
(* From VFA Require Import Perm.
From VFA Require Import Sort. *)
Require Import Unicode.Utf8.
From Coq Require Import Lists.List Arith.Arith.
Import ListNotations.
Require Import Permutation.

(** imports begin *)
Fixpoint insert (i : nat) (l : list nat) :=
  match l with
  | [] => [i]
  | h :: t => if i <=? h then i :: h :: t else h :: insert i t
  end.
Fixpoint sort (l : list nat) : list nat :=
  match l with
  | [] => []
  | h :: t => insert h (sort t)
  end.
(** imports end *)


Definition bag := list nat.
Fixpoint count (v:nat) (s:bag) : nat :=
  match s with
  | nil => 0
  | h :: t =>
      (if h =? v then 1 else 0) + count v t
  end.

Definition bag_eqv (b1 b2: bag) : Prop :=
  ∀ n, count n b1 = count n b2.

(* It is easy to prove bag_eqv is an equivalence relation. *)

Lemma bag_eqv_refl : ∀ b, bag_eqv b b.
Proof. Admitted.

Lemma bag_eqv_sym: ∀ b1 b2, bag_eqv b1 b2 → bag_eqv b2 b1.
Proof. Admitted.

Lemma bag_eqv_trans: ∀ b1 b2 b3, bag_eqv b1 b2 → bag_eqv b2 b3 → bag_eqv b1 b3.
Proof. Admitted.
(* The following little lemma is handy in a couple of places. *)
Lemma bag_eqv_cons : ∀ x b1 b2, bag_eqv b1 b2 → bag_eqv (x::b1) (x::b2).
Proof. Admitted.

(** Correctness *)

Inductive sorted : list nat → Prop :=
| sorted_nil :
    sorted []
| sorted_1 : ∀ x,
    sorted [x]
| sorted_cons : ∀ x y l,
    x ≤ y → sorted (y :: l) → sorted (x :: y :: l).

Definition is_a_sorting_algorithm' (f: list nat → list nat) :=
  ∀ al, bag_eqv al (f al) ∧ sorted (f al).

Lemma insert_bag: ∀ x l, bag_eqv (x::l) (insert x l).
Proof. Admitted.

Theorem sort_bag: ∀ l, bag_eqv l (sort l).
Proof. Admitted.

(* This theorem is already proved in inserttionsort.v *)
Theorem sort_sorted: ∀ l, sorted (sort l).
Proof. Admitted.

Theorem insertion_sort_correct:
  is_a_sorting_algorithm' sort.
Proof.
split. apply sort_bag. apply sort_sorted.
Qed.














