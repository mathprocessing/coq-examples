(** George Hotz | Programming | The Coq Files: sqrt(2) is irrational
https://www.youtube.com/watch?v=bTLc_9buWLQ *)
(* p/q != sqrt(2)
(p*p)/(q*q) != 2
p*p != 2*q*q *)
Import Nat.
Require Import Omega.
Require Import Arith.Even.
Require Import PeanoNat.
Require Import Reals.

(** Define sorry keyword like in Lean Theorem Prover *)
Axiom sorry : forall {A : Type}, A.

(* Search plus. *)
(**
add_succ_l: forall n m : nat, S n + m = S (n + m)
add_succ_r: forall n m : nat, n + S m = S (n + m)
*)
Example add_0_r : forall n : nat, n + 0 = n := sorry.

Lemma x_times_2_is_even : forall x : nat,
  Nat.even (2*x) = true.
Proof.
  simpl.
  intros.
  induction x.
  - reflexivity.
  - rewrite add_0_r.
    rewrite add_0_r in IHx.
    replace (S x + S x) with (S (S (x + x))).
    simpl.
    exact IHx.
    + simpl. rewrite (Nat.add_comm x (S x)). simpl.
      (** rewrite add_succ_l add_succ_r. *)
      reflexivity.
Qed.

Example even_add_mul_2 :  

(* Search even.*)
Lemma x_times_2_is_even' : forall x : nat,
  Nat.even (2*x) = true.
Proof.
  intro x.
  Check even_add_mul_2 0.
  assert (h : even (0 + 2 * x) = even 0) by exact (even_add_mul_2 0 x).
  exact h.
Qed.

Definition x_times_2_is_even'' : forall x : nat,
  Nat.even (2*x) = true := (fun x => even_add_mul_2 0 x).

(* Search (_ < _ -> _ <> _).  (* found lt_neq  *)
Search (_ <> _ -> _ <> _). (* found neq_sym *)
Search (_ ^ _ = _).        (* found pow_2_r *) *)

Lemma square_of_even_is_even' : forall x: nat,
  Nat.even (x*x) = true -> Nat.even x = true.
Proof.
  intro x.
  Check even_pow x 2.
  assert (h : even (x ^ 2) = even x).
  - apply even_pow.
    set (h2 := (lt_neq 0 2) lt_0_2).
    exact (neq_sym 0 2 h2).
  - replace (x * x) with (x ^ 2).
    rewrite h.
    exact id.
    + exact (pow_2_r x).
Qed.

Lemma even_exists : forall x : nat,
  Nat.even x = true -> exists k, 2 * k = x.
Proof.
  intros.
  apply even_spec in H.
  unfold Even in H.
  destruct H.
  exists x0.
  symmetry.
  apply H.
Qed.

(* Search (even (_ + _ * _) = _).
Search xorb. *)
(* my try before i see George try *)
Lemma even_sum_to_eqb : forall m n: nat,
  even (m + n) = Bool.eqb (even m) (even n).
Proof.
  intros m n.
  
Admitted.
(** 
  even +   even =   even    f(true, true)   -> true
  even + ~ even = ~ even    f(true, false)  -> false
~ even +   even = ~ even    f(false, true)  -> false
~ even + ~ even =   even    f(false, false) -> true
=> f(x, y) = xor(x, y, true) = eq(x, y)
*)

Lemma even_is_xor_GEORGE : forall x y : nat,
  Nat.even (x+y) = negb (xorb (Nat.even x) (Nat.even y)).
Proof.
  intros.
  
Qed.

Lemma square_of_even_is_even : forall x: nat,
  Nat.even (x*x) = Nat.even x.
Proof.
  intros.
  induction x.
  - simpl. reflexivity.
  - (* (x + 1) * (x + 1) = x * x + 2 * x + 1 *)
    replace (S x * S x) with ((1+x)*(1+x)) by reflexivity.
    rewrite mul_add_distr_l.
    rewrite mul_add_distr_r.
    rewrite mul_add_distr_r.
    rewrite add_assoc.
    replace (1 * 1 + x * 1 + 1 * x) with (1 + 2 * x).
    2 : {
      simpl.
      rewrite mul_1_r, add_0_r.
      reflexivity.
    }
    rewrite even_sum_to_eqb.
    rewrite even_sum_to_eqb.
    rewrite IHx.
    rewrite even_1.
    rewrite x_times_2_is_even.
    unfold Bool.eqb.
    
    
    
Qed.


Lemma square_of_even_is_even'' : forall x: nat,
  Nat.even (x*x) = true -> Nat.even x = true.
Proof.
  intros.
  induction x.
  - simpl.
    reflexivity.
  - (* 35:56 George: This is never true? you see the problem? *)
    (* George: This is isn't even, because we have successor in a goal. *)
    (**
    even (S x * S x) = even (x ^ 2 + 2 * x + 1) = ((x ^ 2) % 2 + (2 * x) % 2 + 1 % 2) is 0 =
    = (x % 2 + 0 % 2 + 1 % 2) is 0 = (x + 1) % 2 is 0 = even (x + 1)
    =. H : even (S x) = true *)
    Check even_add_mul_even.
    assert (H2 : even (S x) = true) by admit.
    exact H2.
    
    
  apply even_exists in H.
  
  destruct H.
  
Qed.

Theorem sqrt2_is_irrational : forall p q : nat,
  p*p <> 2*q*q.
Proof.
  intros.
  unfold not.
  intros eq.
  (** proof by contradiction *)
  assert (Nat.even (p * p) = true).
  {
    rewrite -> eq.
    rewrite <- mul_assoc.
    apply x_times_2_is_even.
  }
Qed.



(* Check even 0.

Check Nat.even_0.

Goal Nat.even 0 = true.
Proof.
  rewrite Nat.even_spec.
  unfold Nat.Even.
  exact (ex_intro (fun m => 0 = 2 * m) 0 eq_refl).
Qed. *)