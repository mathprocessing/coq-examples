Require Import Setoid.

Parameter X Y : Set.
Parameter (R : X -> Y -> Prop) (S : Y -> X -> Prop).
Hypothesis R_S_inv : forall x y, R x y <-> S y x.
(** also can use: Axiom, Conjecture *)

Goal forall x y, R x y <-> R x y.
Proof.
  intros x y.
  apply iff_refl.
Qed.

Goal forall x y, S y x -> R x y.
Proof.
  intros x y.
  rewrite R_S_inv.
  (** exact id. *)
  exact (fun thing : S y x => thing).
Qed.

Definition f : X -> X := id.
Definition g : forall X : Prop,
  X -> ~X -> False := fun X A B => B A.
  
Theorem test : forall X : Prop,
  X -> ~X -> False.
Proof.
  exact g.
  (** intros X.
      exact (g X). *)
Qed.

Definition three_eq_three : 3 = 3 := eq_refl 3.
Definition three_eq_three' : id 3 = 3 := eq_refl 3.
Definition three_eq_three'' : id 3 = id 3 := eq_refl 3.
Definition three_eq_three''' : 1 + 2 = 3 := eq_refl 3.

Inductive bool : Type := true : bool | false : bool.
(**
bool is defined
bool_rect is defined
bool_ind is defined
bool_rec is defined
*)
Print bool.
Print bool_rect.
Print bool_ind.
Print bool_rec.

Parameter u : bool -> Prop.
Definition F : true = true.
Check bool_ind u.
(** u true -> u false -> forall b : bool, u b
<=> 
if we have (u true) and (u false) then
we have all bool values in set {true, false}.
*)

(** bool_rect *)
forall P : bool -> Type,
       P true ->
       P false ->
       forall b : bool, P b

(** bool_ind *)
forall P : bool -> Prop,
       P true ->
       P false ->
       forall b : bool, P b

(** bool_rec *)
forall P : bool -> Set,
       P true ->
       P false ->
       forall b : bool, P b
